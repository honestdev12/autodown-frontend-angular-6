"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var token_storage_service_1 = require("./token-storage.service");
var utils_service_1 = require("../services/utils.service");
var api_1 = require("../../config/api");
var current_user_service_1 = require("../services/current-user.service");
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http, tokenStorage, util, currentUser) {
        this.http = http;
        this.tokenStorage = tokenStorage;
        this.util = util;
        this.currentUser = currentUser;
        this.API_ENDPOINT_LOGIN = 'auth/login';
        this.API_ENDPOINT_REFRESH = '';
        this.API_ENDPOINT_REGISTER = 'auth/registration';
        this.onCredentialUpdated$ = new rxjs_1.Subject();
        this.API_URL = api_1.AppConstants.baseURL;
    }
    /**
     * Check, if user already authorized.
     * @description Should return Observable with true or false values
     * @returns {boolean}
     * @memberOf AuthService
     */
    AuthenticationService.prototype.isAuthorized = function () {
        return this.tokenStorage.getAccessToken().pipe(operators_1.map(function (token) { return !!token; }));
    };
    /**
     * Get access token
     * @description Should return access token in Observable from e.g. localStorage
     * @returns {Observable<string>}
     */
    AuthenticationService.prototype.getAccessToken = function () {
        return this.tokenStorage.getAccessToken();
    };
    /**
     * Get user roles
     * @returns {Observable<any>}
     */
    AuthenticationService.prototype.getUserRoles = function () {
        return this.tokenStorage.getUserRoles();
    };
    /**
     * Function, that should perform refresh token verifyTokenRequest
     * @description Should be successfully completed so interceptor
     * can execute pending requests or retry original one
     * @returns {Observable<any>}
     */
    AuthenticationService.prototype.refreshToken = function () {
        var _this = this;
        return this.tokenStorage.getRefreshToken().pipe(operators_1.switchMap(function (refreshToken) {
            return _this.http.get(_this.API_URL + _this.API_ENDPOINT_REFRESH + '?' + _this.util.urlParam(refreshToken));
        }), operators_1.tap(this.saveAccessData.bind(this)), operators_1.catchError(function (err) {
            _this.logout();
            return rxjs_1.throwError(err);
        }));
    };
    /**
     * Function, checks response of failed request to determine,
     * whether token be refreshed or not.
     * @description Essentialy checks status
     * @param {Response} response
     * @returns {boolean}
     */
    AuthenticationService.prototype.refreshShouldHappen = function (response) {
        return response.status === 401;
    };
    /**
     * Verify that outgoing request is refresh-token,
     * so interceptor won't intercept this request
     * @param {string} url
     * @returns {boolean}
     */
    AuthenticationService.prototype.verifyTokenRequest = function (url) {
        return url.endsWith(this.API_ENDPOINT_REFRESH);
    };
    /**
     * Submit login request
     * @param {Credential} credential
     * @returns {Observable<any>}
     */
    AuthenticationService.prototype.login = function (credential) {
        var httpOptions = {
            headers: new http_1.HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        return this.http.post(this.API_URL + this.API_ENDPOINT_LOGIN + '/', JSON.stringify(credential), httpOptions).pipe(operators_1.map(function (result) {
            if (result instanceof Array) {
                return result.pop();
            }
            return result;
        }), operators_1.tap(this.saveAccessData.bind(this)));
    };
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    AuthenticationService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // Let the app keep running by returning an empty result.
            return rxjs_1.from(result);
        };
    };
    /**
     * Logout
     */
    AuthenticationService.prototype.logout = function (refresh) {
        this.tokenStorage.clear();
        if (refresh) {
            location.reload(true);
        }
    };
    /**
     * Save access data in the storage
     * @private
     * @param {AccessData} data
     */
    AuthenticationService.prototype.saveAccessData = function (accessData) {
        if (typeof accessData !== 'undefined') {
            this.tokenStorage
                .setAccessToken(accessData.token)
                .setUserRoles(accessData.user.roles);
            this.currentUser.set(accessData.user);
            this.onCredentialUpdated$.next(accessData);
        }
    };
    /**
     * Submit registration request
     * @param {Credential} credential
     * @returns {Observable<any>}
     */
    AuthenticationService.prototype.register = function (credential) {
        var httpOptions = {
            headers: new http_1.HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        return this.http.post(this.API_URL + this.API_ENDPOINT_REGISTER + '/', JSON.stringify(credential), httpOptions)
            .pipe(operators_1.catchError(this.handleError('register', [])));
    };
    /**
     * Submit forgot password request
     * @param {Credential} credential
     * @returns {Observable<any>}
     */
    AuthenticationService.prototype.requestPassword = function (credential) {
        return this.http.get(this.API_URL + this.API_ENDPOINT_LOGIN + '?' + this.util.urlParam(credential))
            .pipe(operators_1.catchError(this.handleError('forgot-password', [])));
    };
    AuthenticationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            token_storage_service_1.TokenStorage,
            utils_service_1.UtilsService,
            current_user_service_1.CurrentUser])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map