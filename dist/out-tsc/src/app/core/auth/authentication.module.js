"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ngx_auth_1 = require("ngx-auth");
var token_storage_service_1 = require("./token-storage.service");
var authentication_service_1 = require("./authentication.service");
function factory(authenticationService) {
    return authenticationService;
}
exports.factory = factory;
var AuthenticationModule = /** @class */ (function () {
    function AuthenticationModule() {
    }
    AuthenticationModule = __decorate([
        core_1.NgModule({
            imports: [ngx_auth_1.AuthModule],
            providers: [
                token_storage_service_1.TokenStorage,
                authentication_service_1.AuthenticationService,
                { provide: ngx_auth_1.PROTECTED_FALLBACK_PAGE_URI, useValue: '/' },
                { provide: ngx_auth_1.PUBLIC_FALLBACK_PAGE_URI, useValue: '/login' },
                {
                    provide: ngx_auth_1.AUTH_SERVICE,
                    deps: [authentication_service_1.AuthenticationService],
                    useFactory: factory
                }
            ]
        })
    ], AuthenticationModule);
    return AuthenticationModule;
}());
exports.AuthenticationModule = AuthenticationModule;
//# sourceMappingURL=authentication.module.js.map