"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var menu_aside_directive_1 = require("./directives/menu-aside.directive");
var menu_aside_offcanvas_directive_1 = require("./directives/menu-aside-offcanvas.directive");
var menu_horizontal_offcanvas_directive_1 = require("./directives/menu-horizontal-offcanvas.directive");
var menu_horizontal_directive_1 = require("./directives/menu-horizontal.directive");
var clipboard_directive_1 = require("./directives/clipboard.directive");
var scroll_top_directive_1 = require("./directives/scroll-top.directive");
var header_directive_1 = require("./directives/header.directive");
var menu_aside_toggle_directive_1 = require("./directives/menu-aside-toggle.directive");
var quick_sidebar_offcanvas_directive_1 = require("./directives/quick-sidebar-offcanvas.directive");
var first_letter_pipe_1 = require("./pipes/first-letter.pipe");
var time_elapsed_pipe_1 = require("./pipes/time-elapsed.pipe");
var quick_search_directive_1 = require("./directives/quick-search.directive");
var join_pipe_1 = require("./pipes/join.pipe");
var get_object_pipe_1 = require("./pipes/get-object.pipe");
var console_log_pipe_1 = require("./pipes/console-log.pipe");
var safe_pipe_1 = require("./pipes/safe.pipe");
var portlet_directive_1 = require("./directives/portlet.directive");
var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule],
            declarations: [
                // directives
                menu_aside_directive_1.MenuAsideDirective,
                menu_aside_offcanvas_directive_1.MenuAsideOffcanvasDirective,
                menu_horizontal_offcanvas_directive_1.MenuHorizontalOffcanvasDirective,
                menu_horizontal_directive_1.MenuHorizontalDirective,
                scroll_top_directive_1.ScrollTopDirective,
                header_directive_1.HeaderDirective,
                menu_aside_toggle_directive_1.MenuAsideToggleDirective,
                quick_sidebar_offcanvas_directive_1.QuickSidebarOffcanvasDirective,
                quick_search_directive_1.QuickSearchDirective,
                clipboard_directive_1.ClipboardDirective,
                portlet_directive_1.PortletDirective,
                // pipes
                first_letter_pipe_1.FirstLetterPipe,
                time_elapsed_pipe_1.TimeElapsedPipe,
                join_pipe_1.JoinPipe,
                get_object_pipe_1.GetObjectPipe,
                console_log_pipe_1.ConsoleLogPipe,
                safe_pipe_1.SafePipe
            ],
            exports: [
                // directives
                menu_aside_directive_1.MenuAsideDirective,
                menu_aside_offcanvas_directive_1.MenuAsideOffcanvasDirective,
                menu_horizontal_offcanvas_directive_1.MenuHorizontalOffcanvasDirective,
                menu_horizontal_directive_1.MenuHorizontalDirective,
                scroll_top_directive_1.ScrollTopDirective,
                header_directive_1.HeaderDirective,
                menu_aside_toggle_directive_1.MenuAsideToggleDirective,
                quick_sidebar_offcanvas_directive_1.QuickSidebarOffcanvasDirective,
                quick_search_directive_1.QuickSearchDirective,
                clipboard_directive_1.ClipboardDirective,
                portlet_directive_1.PortletDirective,
                // pipes
                first_letter_pipe_1.FirstLetterPipe,
                time_elapsed_pipe_1.TimeElapsedPipe,
                join_pipe_1.JoinPipe,
                get_object_pipe_1.GetObjectPipe,
                console_log_pipe_1.ConsoleLogPipe,
                safe_pipe_1.SafePipe
            ],
            providers: []
        })
    ], CoreModule);
    return CoreModule;
}());
exports.CoreModule = CoreModule;
//# sourceMappingURL=core.module.js.map