"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var user_model_1 = require("../models/user.model");
var user_detail_model_1 = require("../models/user-detail.model");
var http_utils_service_1 = require("./http-utils.service");
var token_storage_service_1 = require("../auth/token-storage.service");
var CurrentUser = /** @class */ (function () {
    function CurrentUser(tokenStorage, httpUtilsService) {
        this.tokenStorage = tokenStorage;
        this.httpUtilsService = httpUtilsService;
        this.currentUser = null;
        this.userUpdated$ = new rxjs_1.BehaviorSubject(this.currentUser);
        this.get();
    }
    CurrentUser.prototype.get = function () {
        var _this = this;
        this.httpUtilsService.get('auth/user/').subscribe(function (user) {
            if (!user.detail) {
                user.detail = new user_detail_model_1.UserDetailModel();
            }
            _this.set(user);
        });
    };
    CurrentUser.prototype.set = function (currentUser) {
        this.currentUser = currentUser;
        this.userUpdated$.next(this.currentUser);
    };
    CurrentUser.prototype.clear = function () {
        this.currentUser = new user_model_1.UserModel();
        this.userUpdated$.next(this.currentUser);
    };
    CurrentUser = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [token_storage_service_1.TokenStorage,
            http_utils_service_1.HttpUtilsService])
    ], CurrentUser);
    return CurrentUser;
}());
exports.CurrentUser = CurrentUser;
//# sourceMappingURL=current-user.service.js.map