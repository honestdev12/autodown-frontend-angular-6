"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var core_1 = require("@angular/core");
var operators_1 = require("rxjs/operators");
var router_1 = require("@angular/router");
var utils_service_1 = require("./utils.service");
var layout_config_storage_service_1 = require("./layout-config-storage.service");
var layout_1 = require("../../config/layout");
var LayoutConfigService = /** @class */ (function () {
    function LayoutConfigService(router, utils, layoutConfigStorageService) {
        var _this = this;
        this.router = router;
        this.utils = utils;
        this.layoutConfigStorageService = layoutConfigStorageService;
        // default config
        this.layoutConfig = new layout_1.LayoutConfig();
        // register on config changed event and set default config
        this.onLayoutConfigUpdated$ = new rxjs_1.BehaviorSubject(this.layoutConfig);
        this.router.events
            .pipe(operators_1.filter(function (event) { return event instanceof router_1.NavigationEnd; }), operators_1.mergeMap(function () { return _this.layoutConfigStorageService.loadConfig(); }))
            .subscribe(function (config) {
            _this.layoutConfig = config;
            _this.onLayoutConfigUpdated$.next(config);
        });
    }
    /**
     * Reset existing configurations
     * NOTE: This method will remove older config and pass only new;
     * @param model
     * @param doNotSave
     */
    LayoutConfigService.prototype.setModel = function (model, doNotSave) {
        // merge and replace existing config object
        // deep merge for mutltidimentional arrays
        this.layoutConfig = Object.assign({}, this.layoutConfig, model);
        if (!doNotSave) {
            this.layoutConfigStorageService.saveConfig(this.layoutConfig);
        }
        // fire off an event that all subscribers will listen
        this.onLayoutConfigUpdated$.next(this.layoutConfig);
    };
    LayoutConfigService.prototype.reloadSavedConfig = function () {
        this.setModel(new layout_1.LayoutConfig(this.getSavedConfig()), true);
    };
    /**
     * Set current config as default template.
     * This config is changeable via layout builder.
     * Useful when want to reset layout without clearing the config at layout
     */
    LayoutConfigService.prototype.getSavedConfig = function () {
        return this.layoutConfigStorageService.loadConfig();
    };
    LayoutConfigService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            utils_service_1.UtilsService,
            layout_config_storage_service_1.LayoutConfigStorageService])
    ], LayoutConfigService);
    return LayoutConfigService;
}());
exports.LayoutConfigService = LayoutConfigService;
//# sourceMappingURL=layout-config.service.js.map