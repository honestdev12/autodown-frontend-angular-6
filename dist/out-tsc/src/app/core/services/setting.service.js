"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_utils_service_1 = require("./http-utils.service");
var rxjs_1 = require("rxjs");
var setting_model_1 = require("../models/setting.model");
var SettingService = /** @class */ (function () {
    function SettingService(httpUtils) {
        this.httpUtils = httpUtils;
        this.API_LINK = 'admin/config';
        this.setting = new setting_model_1.SettingModel();
        this.settingUpdated$ = new rxjs_1.BehaviorSubject(this.setting);
        this.get();
    }
    SettingService.prototype.get = function () {
        var _this = this;
        this.httpUtils.get(this.API_LINK + '/').subscribe(function (setting) { return _this.set(setting); });
    };
    SettingService.prototype.set = function (setting) {
        this.setting = setting;
        this.settingUpdated$.next(this.setting);
    };
    SettingService.prototype.update = function (data) {
        var _this = this;
        this.httpUtils.post(this.API_LINK + '/', data).subscribe(function (setting) { return _this.set(setting); });
    };
    SettingService.prototype.clear = function () {
        this.setting = null;
        this.settingUpdated$.next(this.setting);
    };
    SettingService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_utils_service_1.HttpUtilsService])
    ], SettingService);
    return SettingService;
}());
exports.SettingService = SettingService;
//# sourceMappingURL=setting.service.js.map