"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_utils_service_1 = require("./http-utils.service");
var _moment = require("moment");
var date_fields = [
    'start_date',
    'end_date',
    'bill_date'
];
var InvoiceService = /** @class */ (function () {
    function InvoiceService(httpUtils) {
        this.httpUtils = httpUtils;
        this.API_LINK = 'invoice';
    }
    InvoiceService.prototype.formatData = function (data) {
        for (var i in date_fields) {
            var f = date_fields[i];
            if (data[f]) {
                data[f] = _moment(data[f], 'YYYY-MM-DD').toISOString();
            }
        }
        return data;
    };
    InvoiceService.prototype.getAPILink = function (admin, paid, userId) {
        if (admin === void 0) { admin = false; }
        if (paid === void 0) { paid = null; }
        if (userId === void 0) { userId = null; }
        var api_link = this.API_LINK + '/';
        if (paid == 'paid' || paid == 'unpaid')
            api_link = paid + '/' + api_link;
        if (userId)
            api_link = userId + '/' + api_link;
        if (admin)
            api_link = 'admin/' + api_link;
        return api_link;
    };
    InvoiceService.prototype.get = function (queryParams, paid, admin, userId) {
        if (paid === void 0) { paid = null; }
        if (admin === void 0) { admin = false; }
        if (userId === void 0) { userId = null; }
        return this.httpUtils.get(this.getAPILink(admin, paid, userId), queryParams);
    };
    InvoiceService.prototype.update = function (id, data, admin) {
        if (admin === void 0) { admin = false; }
        return this.httpUtils.put(this.getAPILink(admin), id, this.formatData(data));
    };
    InvoiceService.prototype.create = function (data, admin) {
        if (admin === void 0) { admin = false; }
        return this.httpUtils.post(this.getAPILink(admin), this.formatData(data));
    };
    InvoiceService.prototype.delete = function (id, admin) {
        if (admin === void 0) { admin = false; }
        return this.httpUtils.delete(this.getAPILink(admin), id);
    };
    InvoiceService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_utils_service_1.HttpUtilsService])
    ], InvoiceService);
    return InvoiceService;
}());
exports.InvoiceService = InvoiceService;
//# sourceMappingURL=invoice.service.js.map