"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var pages_1 = require("../../config/pages");
var router_1 = require("@angular/router");
var objectPath = require("object-path");
var PageConfigService = /** @class */ (function () {
    function PageConfigService(router) {
        this.router = router;
        this.configModel = new pages_1.PagesConfig();
        this.onPageUpdated$ = new rxjs_1.BehaviorSubject(this.configModel);
    }
    PageConfigService.prototype.setModel = function (menuModel) {
        this.configModel = Object.assign(this.configModel, menuModel);
        this.onPageUpdated$.next(this.configModel);
    };
    PageConfigService.prototype.getCurrentPageConfig = function () {
        return objectPath.get(this.configModel, 'config.' + this.router.url.substring(1).replace(/\//g, '.'));
    };
    PageConfigService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router])
    ], PageConfigService);
    return PageConfigService;
}());
exports.PageConfigService = PageConfigService;
//# sourceMappingURL=page-config.service.js.map