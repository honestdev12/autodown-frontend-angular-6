"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_utils_service_1 = require("./http-utils.service");
var SpiderJobService = /** @class */ (function () {
    function SpiderJobService(httpUtils) {
        this.httpUtils = httpUtils;
        this.API_LINK = 'spider_job';
    }
    SpiderJobService.prototype.get = function (vendor, queryParams) {
        return this.httpUtils.get(vendor + '/' + this.API_LINK + '/', queryParams);
    };
    SpiderJobService.prototype.update = function (vendor, id, data) {
        return this.httpUtils.put(vendor + '/' + this.API_LINK + '/', id, data);
    };
    SpiderJobService.prototype.create = function (vendor, data) {
        return this.httpUtils.post(vendor + '/' + this.API_LINK + '/', data);
    };
    SpiderJobService.prototype.delete = function (vendor, id) {
        return this.httpUtils.delete(vendor + '/' + this.API_LINK + '/', id);
    };
    SpiderJobService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_utils_service_1.HttpUtilsService])
    ], SpiderJobService);
    return SpiderJobService;
}());
exports.SpiderJobService = SpiderJobService;
//# sourceMappingURL=spider-job.service.js.map