"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var token_storage_service_1 = require("../auth/token-storage.service");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var api_1 = require("../../config/api");
var query_params_model_1 = require("../models/query-params.model");
var chart_params_model_1 = require("../models/chart-params.model");
var HttpUtilsService = /** @class */ (function () {
    function HttpUtilsService(tokenStorage, httpClient) {
        this.tokenStorage = tokenStorage;
        this.httpClient = httpClient;
    }
    HttpUtilsService.prototype.getFindHTTPParams = function (queryParams) {
        var params;
        if (queryParams instanceof query_params_model_1.QueryParamsModel) {
            params = new http_1.HttpParams()
                .set('sort', queryParams.sort)
                .set('page', (queryParams.page + 1).toString())
                .set('page_size', queryParams.page_size.toString());
        }
        else if (queryParams instanceof chart_params_model_1.ChartParamsModel) {
            params = new http_1.HttpParams()
                .set('spider_id', queryParams.spider_id.toString())
                .set('page', queryParams.period);
        }
        return params;
    };
    HttpUtilsService.prototype.getHTTPHeader = function (secure) {
        if (secure === void 0) { secure = true; }
        if (secure) {
            return this.tokenStorage.getAccessToken().pipe(operators_1.map(function (token) { return new http_1.HttpHeaders({ 'Authorization': 'Token ' + token }); }));
        }
        else {
            var headers = new http_1.HttpHeaders({});
            return rxjs_1.of(headers);
        }
    };
    HttpUtilsService.prototype.get = function (url, queryParams, secure) {
        var _this = this;
        if (queryParams === void 0) { queryParams = null; }
        if (secure === void 0) { secure = true; }
        return this.getHTTPHeader(secure).pipe(operators_1.switchMap(function (headers) {
            var options = { 'headers': headers };
            if (queryParams) {
                options['params'] = _this.getFindHTTPParams(queryParams);
            }
            return _this.httpClient.get(api_1.AppConstants.baseURL + url, options);
        }));
    };
    HttpUtilsService.prototype.post = function (url, data, secure) {
        var _this = this;
        if (secure === void 0) { secure = true; }
        return this.getHTTPHeader(secure).pipe(operators_1.switchMap(function (headers) {
            headers = headers.set('Content-Type', 'application/json');
            return _this.httpClient.post(api_1.AppConstants.baseURL + url, JSON.stringify(data), { 'headers': headers });
        }));
    };
    HttpUtilsService.prototype.put = function (url, id, data, secure) {
        var _this = this;
        if (secure === void 0) { secure = true; }
        return this.getHTTPHeader(secure).pipe(operators_1.switchMap(function (headers) {
            headers = headers.set('Content-Type', 'application/json');
            return _this.httpClient.put(api_1.AppConstants.baseURL + url + '/' + id.toString() + '/', JSON.stringify(data), { 'headers': headers });
        }));
    };
    HttpUtilsService.prototype.delete = function (url, id) {
        var _this = this;
        return this.getHTTPHeader(true).pipe(operators_1.switchMap(function (headers) {
            return _this.httpClient.delete(api_1.AppConstants.baseURL + url + '/' + id.toString() + '/', { 'headers': headers });
        }));
    };
    HttpUtilsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [token_storage_service_1.TokenStorage, http_1.HttpClient])
    ], HttpUtilsService);
    return HttpUtilsService;
}());
exports.HttpUtilsService = HttpUtilsService;
//# sourceMappingURL=http-utils.service.js.map