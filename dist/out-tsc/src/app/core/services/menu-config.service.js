"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var router_1 = require("@angular/router");
var menu_1 = require("../../config/menu");
var current_user_service_1 = require("./current-user.service");
var MenuConfigService = /** @class */ (function () {
    function MenuConfigService(router, currentUser) {
        var _this = this;
        this.router = router;
        this.currentUser = currentUser;
        this.configModel = new menu_1.MenuConfig();
        this.onMenuUpdated$ = new rxjs_1.BehaviorSubject(this.configModel);
        this.menuHasChanged = false;
        this.configModel.basePath = this.router.url.indexOf('admin') >= 0 ? 'admin' : 'vendor';
        this.router.events
            .subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                if (_this.menuHasChanged)
                    _this.resetModel();
                if (event.url.indexOf('admin') >= 0 && _this.configModel.basePath != 'admin') {
                    _this.configModel.basePath = 'admin';
                    _this.setModel(_this.configModel);
                }
                else if (event.url.indexOf('admin') < 0 && _this.configModel.basePath != 'vendor') {
                    _this.configModel.basePath = 'vendor';
                    _this.setModel(_this.configModel);
                }
            }
        });
    }
    MenuConfigService.prototype.setModel = function (menuModel) {
        this.configModel = Object.assign(this.configModel, menuModel);
        this.onMenuUpdated$.next(this.configModel);
        this.menuHasChanged = true;
    };
    MenuConfigService.prototype.resetModel = function () {
        this.configModel = new menu_1.MenuConfig();
        this.onMenuUpdated$.next(this.configModel);
        this.menuHasChanged = false;
    };
    MenuConfigService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router, current_user_service_1.CurrentUser])
    ], MenuConfigService);
    return MenuConfigService;
}());
exports.MenuConfigService = MenuConfigService;
//# sourceMappingURL=menu-config.service.js.map