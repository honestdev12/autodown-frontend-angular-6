"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_utils_service_1 = require("./http-utils.service");
var ScheduleService = /** @class */ (function () {
    function ScheduleService(httpUtils) {
        this.httpUtils = httpUtils;
        this.API_LINK = 'schedule';
    }
    ScheduleService.prototype.get = function (queryParams) {
        return this.httpUtils.get(this.API_LINK + '/', queryParams);
    };
    ScheduleService.prototype.update = function (id, data) {
        return this.httpUtils.put(this.API_LINK + '/', id, data);
    };
    ScheduleService.prototype.create = function (data) {
        return this.httpUtils.post(this.API_LINK + '/', data);
    };
    ScheduleService.prototype.delete = function (id) {
        return this.httpUtils.delete(this.API_LINK + '/', id);
    };
    ScheduleService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_utils_service_1.HttpUtilsService])
    ], ScheduleService);
    return ScheduleService;
}());
exports.ScheduleService = ScheduleService;
//# sourceMappingURL=schedule.service.js.map