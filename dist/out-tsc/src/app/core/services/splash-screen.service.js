"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/animations");
var router_1 = require("@angular/router");
var SplashScreenService = /** @class */ (function () {
    function SplashScreenService(animationBuilder, router) {
        this.animationBuilder = animationBuilder;
        this.router = router;
    }
    SplashScreenService.prototype.init = function (element) {
        var _this = this;
        // Get the splash screen element
        this.splashElement = element;
        // Hide it on the first NavigationEnd event
        var routerEvents = this.router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationEnd) {
                _this.hide();
                routerEvents.unsubscribe();
            }
        });
    };
    SplashScreenService.prototype.show = function () {
        var _this = this;
        this.player = this.animationBuilder
            .build([
            animations_1.style({ opacity: '0', zIndex: '99999' }),
            animations_1.animate('600ms ease', animations_1.style({ opacity: '1' }))
        ])
            .create(this.splashElement);
        setTimeout(function () {
            _this.player.play();
        }, 0);
    };
    SplashScreenService.prototype.hide = function () {
        var _this = this;
        this.player = this.animationBuilder
            .build([
            animations_1.style({ opacity: '1' }),
            animations_1.animate('600ms ease', animations_1.style({ opacity: '0' }))
        ])
            .create(this.splashElement);
        setTimeout(function () {
            _this.player.onDone(function () { return (_this.splashElement.style.display = 'none'); });
            _this.player.play();
        }, 0);
    };
    SplashScreenService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [animations_1.AnimationBuilder,
            router_1.Router])
    ], SplashScreenService);
    return SplashScreenService;
}());
exports.SplashScreenService = SplashScreenService;
//# sourceMappingURL=splash-screen.service.js.map