"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var layout_config_service_1 = require("../layout-config.service");
var class_init_service_1 = require("../class-init.service");
var objectPath = require("object-path");
var HeaderService = /** @class */ (function () {
    function HeaderService(layoutConfigService, classInitService) {
        var _this = this;
        this.layoutConfigService = layoutConfigService;
        this.classInitService = classInitService;
        // minimize offset
        this.attrMinimizeOffset = '200';
        // minimize offset on mobile
        this.attrMinimizeMobileOffset = '200';
        // subscribe to classes update
        this.classInitService.onClassesUpdated$.subscribe(function (classes) {
            _this.headerMenuCloseClass = classes.header_menu_close.join(' ');
        });
        this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (model) {
            var config = model.config;
            var containerClass = ['m-container', 'm-container--full-height'];
            var selfLayout = objectPath.get(config, 'self.layout');
            if (selfLayout === 'boxed' || selfLayout === 'wide') {
                containerClass.push('m-container--responsive m-container--xxl');
            }
            else {
                containerClass.push('m-container--fluid');
            }
            _this.containerClass = containerClass.join(' ');
            // get menu header display option
            _this.menuHeaderDisplay = objectPath.get(config, 'menu.header.display');
            // minimize desktop/mobile
            _this.attrMinimizeDesktopEnabled = objectPath.get(config, 'header.self.fixed.minimize.desktop.enabled');
            _this.attrMinimizeMobileEnabled = objectPath.get(config, 'header.self.fixed.minimize.mobile.enabled');
            _this.attrMinimizeOffset = objectPath.get(config, 'header.self.fixed.minimize.desktop.offset');
            _this.attrMinimizeMobileOffset = objectPath.get(config, 'header.self.fixed.minimize.mobile.offset');
        });
    }
    HeaderService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [layout_config_service_1.LayoutConfigService,
            class_init_service_1.ClassInitService])
    ], HeaderService);
    return HeaderService;
}());
exports.HeaderService = HeaderService;
//# sourceMappingURL=header.service.js.map