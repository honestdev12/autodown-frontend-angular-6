"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var layout_1 = require("../../config/layout");
var core_1 = require("@angular/core");
var utils_service_1 = require("./utils.service");
var LayoutConfigStorageService = /** @class */ (function () {
    function LayoutConfigStorageService(utils) {
        this.utils = utils;
    }
    LayoutConfigStorageService.prototype.saveConfig = function (layoutConfig) {
        if (layoutConfig != null) {
            // config storage
            localStorage.setItem('layoutConfig', JSON.stringify(layoutConfig));
        }
    };
    LayoutConfigStorageService.prototype.getSavedConfig = function () {
        var config = localStorage.getItem('layoutConfig');
        try {
            return rxjs_1.of(JSON.parse(config));
        }
        catch (e) { }
    };
    LayoutConfigStorageService.prototype.loadConfig = function () {
        return this.getSavedConfig().pipe(operators_1.map(function (config) {
            return Object.assign({}, new layout_1.LayoutConfig(), config);
        }));
    };
    LayoutConfigStorageService.prototype.resetConfig = function () {
        localStorage.removeItem('layoutConfig');
    };
    LayoutConfigStorageService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [utils_service_1.UtilsService])
    ], LayoutConfigStorageService);
    return LayoutConfigStorageService;
}());
exports.LayoutConfigStorageService = LayoutConfigStorageService;
//# sourceMappingURL=layout-config-storage.service.js.map