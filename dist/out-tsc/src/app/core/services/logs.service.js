"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var utils_service_1 = require("./utils.service");
var operators_1 = require("rxjs/operators");
var LogsService = /** @class */ (function () {
    function LogsService(http, utils) {
        this.http = http;
        this.utils = utils;
        this.API_URL = 'api';
        this.API_ENDPOINT = '/logs';
    }
    LogsService.prototype.getData = function (params) {
        var url = this.API_URL + this.API_ENDPOINT;
        if (params) {
            url += '?' + this.utils.urlParam(params);
        }
        return this.http
            .get(url)
            .pipe(operators_1.tap(function (message) { }));
    };
    LogsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, utils_service_1.UtilsService])
    ], LogsService);
    return LogsService;
}());
exports.LogsService = LogsService;
//# sourceMappingURL=logs.service.js.map