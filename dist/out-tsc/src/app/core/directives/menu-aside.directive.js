"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var layout_config_service_1 = require("../services/layout-config.service");
var objectPath = require("object-path");
var layout_ref_service_1 = require("../services/layout/layout-ref.service");
var operators_1 = require("rxjs/operators");
var MenuAsideDirective = /** @class */ (function () {
    function MenuAsideDirective(el, layoutConfigService, layoutRefService) {
        var _this = this;
        this.el = el;
        this.layoutConfigService = layoutConfigService;
        this.layoutRefService = layoutRefService;
        this.layoutConfigService.onLayoutConfigUpdated$
            .pipe(operators_1.mergeMap(function (config) {
            _this.config = config;
            return _this.layoutRefService.layoutRefs$;
        }))
            .subscribe(function (layout) {
            _this.layout = layout;
            _this.initMenu();
        });
    }
    MenuAsideDirective.prototype.initMenu = function () {
        if (!this.layout.hasOwnProperty('header')) {
            return;
        }
        var menuDesktopMode = 'accordion';
        if (mUtil.attr(this.el.nativeElement, 'm-menu-dropdown') === '1') {
            menuDesktopMode = 'dropdown';
        }
        var scroll;
        if (mUtil.attr(this.el.nativeElement, 'm-menu-scrollable') === '1') {
            var headerHeight_1 = parseInt(window.getComputedStyle(objectPath.get(this.layout, 'header'))['height'], null);
            scroll = {
                height: function () {
                    return mUtil.getViewPort().height - headerHeight_1;
                }
            };
        }
        var options = {
            // vertical scroll
            scroll: scroll,
            // submenu setup
            submenu: {
                desktop: {
                    // by default the menu mode set to accordion in desktop mode
                    default: menuDesktopMode,
                    // whenever body has this class switch the menu mode to dropdown
                    state: {
                        body: 'm-aside-left--minimize',
                        mode: 'dropdown'
                    }
                },
                tablet: 'accordion',
                mobile: 'accordion' // menu set to accordion in mobile mode
            },
            // accordion setup
            accordion: {
                autoScroll: false,
                expandAll: false
            }
        };
        // init the mMenu plugin
        if (this.menu instanceof mMenu) {
            this.menu.update(options);
        }
        else {
            this.menu = new mMenu(this.el.nativeElement, options);
        }
    };
    MenuAsideDirective.prototype.ngAfterViewInit = function () { };
    MenuAsideDirective.prototype.ngOnDestroy = function () { };
    MenuAsideDirective = __decorate([
        core_1.Directive({
            selector: '[mMenuAside]'
        }),
        __metadata("design:paramtypes", [core_1.ElementRef,
            layout_config_service_1.LayoutConfigService,
            layout_ref_service_1.LayoutRefService])
    ], MenuAsideDirective);
    return MenuAsideDirective;
}());
exports.MenuAsideDirective = MenuAsideDirective;
//# sourceMappingURL=menu-aside.directive.js.map