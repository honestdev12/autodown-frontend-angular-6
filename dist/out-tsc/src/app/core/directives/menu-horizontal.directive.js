"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var MenuHorizontalDirective = /** @class */ (function () {
    function MenuHorizontalDirective(el) {
        this.el = el;
    }
    MenuHorizontalDirective.prototype.ngAfterViewInit = function () {
        // init the mMenu plugin
        this.menu = new mMenu(this.el.nativeElement, {
            submenu: {
                desktop: 'dropdown',
                tablet: 'accordion',
                mobile: 'accordion'
            },
            accordion: {
                slideSpeed: 200,
                autoScroll: true,
                expandAll: false // allow having multiple expanded accordions in the menu
            }
        });
    };
    MenuHorizontalDirective.prototype.ngOnDestroy = function () { };
    MenuHorizontalDirective = __decorate([
        core_1.Directive({
            selector: '[mMenuHorizontal]'
        }),
        __metadata("design:paramtypes", [core_1.ElementRef])
    ], MenuHorizontalDirective);
    return MenuHorizontalDirective;
}());
exports.MenuHorizontalDirective = MenuHorizontalDirective;
//# sourceMappingURL=menu-horizontal.directive.js.map