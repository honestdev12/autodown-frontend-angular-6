"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var layout_ref_service_1 = require("../services/layout/layout-ref.service");
var objectPath = require("object-path");
var layout_config_service_1 = require("../services/layout-config.service");
var rxjs_1 = require("rxjs");
var PortletDirective = /** @class */ (function () {
    function PortletDirective(el, layoutRefService, layoutConfigService) {
        this.el = el;
        this.layoutRefService = layoutRefService;
        this.layoutConfigService = layoutConfigService;
        this.class = this.el.nativeElement.classList;
    }
    PortletDirective.prototype.onResize = function (event) {
        if (this.portlet instanceof mPortlet && objectPath.get(this.options, 'enableSticky')) {
            this.portlet.updateSticky();
        }
    };
    PortletDirective.prototype.ngOnInit = function () { };
    PortletDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        var cls = objectPath.get(this.options, 'class');
        if (Array.isArray(cls)) {
            cls.forEach(function (c) {
                _this.class.add(c);
            });
        }
        else if (cls) {
            this.class.add(cls);
        }
        if (objectPath.get(this.options, 'enableSticky')) {
            rxjs_1.combineLatest(this.layoutRefService.layoutRefs$, this.layoutConfigService.onLayoutConfigUpdated$).subscribe(function (result) {
                if (_this.portlet instanceof mPortlet) {
                    _this.portlet.updateSticky();
                }
                else {
                    _this.initPortlet(result[0], result[1]);
                }
            });
        }
        if (objectPath.get(this.options, 'headOverlay')) {
            this.class.add('m-portlet--head-overlay');
        }
        if (objectPath.get(this.options, 'headLarge')) {
            this.class.add('m-portlet--head-lg');
        }
    };
    PortletDirective.prototype.initPortlet = function (res, config) {
        if (typeof res === undefined || res === null) {
            return;
        }
        // check if all the required element exist
        var hasAllParts = true;
        ['header', 'content', 'asideLeft'].forEach(function (part) {
            if (!res.hasOwnProperty(part)) {
                hasAllParts = false;
            }
        });
        if (!hasAllParts) {
            return;
        }
        var headerHeight = parseInt(window.getComputedStyle(objectPath.get(res, 'header'))['height'], null);
        var contentEl = window.getComputedStyle(objectPath.get(res, 'content'));
        var asideLeftEl = window.getComputedStyle(objectPath.get(res, 'asideLeft'));
        var options = {
            sticky: {
                offset: headerHeight + parseInt(objectPath.get(config, 'config.portlet.sticky.offset') || 0, null),
                zIndex: 100,
                position: {
                    top: function () {
                        return headerHeight;
                    },
                    left: function () {
                        var left = parseInt(contentEl['paddingLeft'], null);
                        if (mUtil.isInResponsiveRange('desktop')) {
                            left += parseInt(asideLeftEl['width'], null);
                        }
                        return left;
                    },
                    right: function () {
                        return parseInt(contentEl['paddingRight'], null);
                    }
                }
            }
        };
        this.options = Object.assign(this.options, options);
        this.portlet = new mPortlet(this.el.nativeElement, this.options);
        this.portlet.initSticky();
    };
    PortletDirective.prototype.ngOnDestroy = function () {
        if (this.portlet instanceof mPortlet && objectPath.get(this.options, 'enableSticky')) {
            this.portlet.destroySticky();
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], PortletDirective.prototype, "options", void 0);
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], PortletDirective.prototype, "class", void 0);
    __decorate([
        core_1.HostListener('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], PortletDirective.prototype, "onResize", null);
    PortletDirective = __decorate([
        core_1.Directive({
            selector: '[mPortlet]'
        }),
        __metadata("design:paramtypes", [core_1.ElementRef,
            layout_ref_service_1.LayoutRefService,
            layout_config_service_1.LayoutConfigService])
    ], PortletDirective);
    return PortletDirective;
}());
exports.PortletDirective = PortletDirective;
//# sourceMappingURL=portlet.directive.js.map