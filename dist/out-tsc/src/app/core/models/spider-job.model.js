"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var schedule_model_1 = require("./schedule.model");
var dropbox_model_1 = require("./dropbox.model");
var SpiderJobModel = /** @class */ (function () {
    function SpiderJobModel() {
        this.username = '';
        this.password = '';
        this.status = true;
        this.directory = '';
        this.name = '';
        this.dropbox = new dropbox_model_1.DropboxModel();
        this.schedule = new schedule_model_1.ScheduleModel();
    }
    return SpiderJobModel;
}());
exports.SpiderJobModel = SpiderJobModel;
//# sourceMappingURL=spider-job.model.js.map