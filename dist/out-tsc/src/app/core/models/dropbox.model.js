"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DropboxModel = /** @class */ (function () {
    function DropboxModel() {
        this.name = '';
        this.token = '';
    }
    return DropboxModel;
}());
exports.DropboxModel = DropboxModel;
//# sourceMappingURL=dropbox.model.js.map