"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ChartParamsModel = /** @class */ (function () {
    // constructor overrides
    function ChartParamsModel(_sort_id, _period) {
        if (_sort_id === void 0) { _sort_id = -1; }
        if (_period === void 0) { _period = 'week'; }
        this.spider_id = _sort_id;
        this.period = _period;
    }
    return ChartParamsModel;
}());
exports.ChartParamsModel = ChartParamsModel;
//# sourceMappingURL=chart-params.model.js.map