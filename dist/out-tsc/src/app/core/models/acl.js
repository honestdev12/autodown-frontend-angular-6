"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AclModel = /** @class */ (function () {
    function AclModel() {
        // default permissions
        this.permissions = {
            ADMIN: ['canDoAnything'],
            USER: ['canDoLimitedThings']
        };
        // store an object of current user roles
        this.currentUserRoles = {};
    }
    return AclModel;
}());
exports.AclModel = AclModel;
//# sourceMappingURL=acl.js.map