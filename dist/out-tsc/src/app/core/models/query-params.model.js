"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var QueryParamsModel = /** @class */ (function () {
    // constructor overrides
    function QueryParamsModel(_sortOrder, _sort, _page, _pageSize) {
        if (_sortOrder === void 0) { _sortOrder = 'asc'; }
        if (_sort === void 0) { _sort = ''; }
        if (_page === void 0) { _page = 0; }
        if (_pageSize === void 0) { _pageSize = 10; }
        if (_sortOrder == 'desc')
            this.sort = '-' + _sort;
        else
            this.sort = _sort;
        this.page = _page;
        this.page_size = _pageSize;
    }
    return QueryParamsModel;
}());
exports.QueryParamsModel = QueryParamsModel;
//# sourceMappingURL=query-params.model.js.map