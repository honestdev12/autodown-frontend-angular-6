"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _moment = require("moment");
var InvoiceModel = /** @class */ (function () {
    function InvoiceModel(_id, _total_count, _price_amount, _start_date, _end_date, _bill_date, _paid, _payment_type, _payment_id, _rate) {
        if (_id === void 0) { _id = null; }
        if (_total_count === void 0) { _total_count = 0; }
        if (_price_amount === void 0) { _price_amount = 0; }
        if (_start_date === void 0) { _start_date = null; }
        if (_end_date === void 0) { _end_date = null; }
        if (_bill_date === void 0) { _bill_date = null; }
        if (_paid === void 0) { _paid = false; }
        if (_payment_type === void 0) { _payment_type = 'PayPal'; }
        if (_payment_id === void 0) { _payment_id = null; }
        if (_rate === void 0) { _rate = 0; }
        this.id = _id;
        this.total_count = _total_count;
        this.price_amount = _price_amount;
        this.start_date = _start_date ? _start_date.split('T')[0] : _moment().format('YYYY-MM-DD');
        this.end_date = _end_date ? _end_date.split('T')[0] : _moment().format('YYYY-MM-DD');
        this.bill_date = _bill_date;
        this.paid = _paid;
        this.payment_type = _payment_type;
        this.payment_id = _payment_id;
        this.rate = _rate;
    }
    return InvoiceModel;
}());
exports.InvoiceModel = InvoiceModel;
//# sourceMappingURL=invoice.model.js.map