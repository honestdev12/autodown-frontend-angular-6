"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var user_detail_model_1 = require("./user-detail.model");
var UserModel = /** @class */ (function () {
    function UserModel() {
        this.username = '';
        this.email = '';
        this.roles = [];
        this.first_name = '';
        this.last_name = '';
        this.unpaid_inv = 0;
        this.detail = new user_detail_model_1.UserDetailModel();
    }
    return UserModel;
}());
exports.UserModel = UserModel;
//# sourceMappingURL=user.model.js.map