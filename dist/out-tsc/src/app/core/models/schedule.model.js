"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ScheduleModel = /** @class */ (function () {
    function ScheduleModel() {
        this.hours = 1;
        this.days = 0;
        this.weeks = 0;
        this.months = 0;
    }
    return ScheduleModel;
}());
exports.ScheduleModel = ScheduleModel;
//# sourceMappingURL=schedule.model.js.map