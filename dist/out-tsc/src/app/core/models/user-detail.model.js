"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserDetailModel = /** @class */ (function () {
    function UserDetailModel() {
        this.avatar = '';
        this.linkedin = '';
        this.facebook = '';
        this.twitter = '';
        this.instagram = '';
        this.address = '';
        this.city = '';
        this.phone_number = '';
        this.company_name = '';
        this.occupation = '';
        this.post_code = '';
        this.state = '';
    }
    return UserDetailModel;
}());
exports.UserDetailModel = UserDetailModel;
//# sourceMappingURL=user-detail.model.js.map