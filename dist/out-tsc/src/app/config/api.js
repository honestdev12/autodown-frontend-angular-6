"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppConstants = /** @class */ (function () {
    function AppConstants() {
    }
    Object.defineProperty(AppConstants, "baseURL", {
        get: function () { return location.protocol + '//' + location.host + "/api/v1/"; },
        enumerable: true,
        configurable: true
    });
    return AppConstants;
}());
exports.AppConstants = AppConstants;
//# sourceMappingURL=api.js.map