"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PagesConfig = /** @class */ (function () {
    function PagesConfig() {
        this.config = {};
        this.config = {
            '/': {
                page: {
                    title: 'Vendor',
                    desc: 'Show Vendor Websites'
                }
            },
            '/admin': {
                page: {
                    title: 'Admin',
                    desc: 'Mangement'
                }
            },
            '/invoice': {
                page: {
                    title: 'Invoice',
                    desc: 'Show Invoices'
                }
            },
            '/payment-setup': {
                page: {
                    title: 'Payment Setup',
                    desc: 'Setting your payment account'
                }
            },
            '/all-vendors': {
                page: {
                    title: 'All Vendors',
                    desc: 'Show all vendors'
                }
            },
            '/my-vendors': {
                page: {
                    title: 'My Vendors',
                    desc: 'Show my vendors'
                }
            },
            builder: {
                page: { title: 'Layout Builder', desc: 'Layout builder' }
            },
            header: {
                actions: {
                    page: { title: 'Actions', desc: 'actions example page' }
                }
            },
            profile: {
                page: { title: 'User Profile', desc: '' }
            },
            404: {
                page: { title: '404 Not Found', desc: '', subheader: false }
            }
        };
    }
    return PagesConfig;
}());
exports.PagesConfig = PagesConfig;
//# sourceMappingURL=pages.js.map