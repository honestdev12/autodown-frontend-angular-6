"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line:no-shadowed-variable
var MenuConfig = /** @class */ (function () {
    function MenuConfig() {
        this.config = {};
        this.basePath = 'vendor';
        this.config = {
            header: {
                vendor: {
                    self: {},
                    items: [
                        {
                            title: 'All Vendors',
                            root: true,
                            page: '/all-vendors',
                            icon: 'flaticon-home-1'
                        },
                        {
                            title: 'My Vendors',
                            root: true,
                            page: '/my-vendors',
                            icon: 'flaticon-app'
                        },
                        {
                            title: 'Invoices',
                            root: true,
                            icon: 'flaticon-coins',
                            page: '/invoice'
                        },
                        {
                            title: 'Payment Setup',
                            root: true,
                            page: '/payment-setup',
                            icon: 'flaticon-settings'
                        },
                        {
                            title: 'Activity',
                            root: true,
                            page: '/',
                            icon: 'flaticon-graph'
                        }
                    ]
                },
                admin: {
                    self: {},
                    items: [
                        {
                            title: 'Dashboard',
                            root: true,
                            page: '/admin',
                        }, {
                            title: 'Invoices',
                            root: true,
                            page: '/admin/invoice',
                            icon: 'flaticon-coins'
                        }, {
                            title: 'Setting',
                            root: true,
                            page: '/admin/setting',
                            icon: 'flaticon-settings'
                        },
                        {
                            title: 'Users',
                            root: true,
                            page: '/admin/users',
                            icon: 'flaticon-users'
                        }
                    ]
                }
            },
            aside: {
                self: {},
                vendor: {
                    self: {},
                    items: [
                        { section: "Vendor Websites" },
                        {
                            title: "Pepco",
                            root: true,
                            page: '/vendor/Pepco'
                        },
                        {
                            title: "Washington Gas",
                            root: true,
                            page: '/vendor/Washington Gas'
                        },
                        {
                            title: "MyDCWater",
                            root: true,
                            page: '/vendor/MyDCWater'
                        },
                        {
                            title: "MyCheckFree",
                            root: true,
                            page: '/vendor/MyCheckFree'
                        },
                        {
                            title: "BusinessComcast",
                            root: true,
                            page: '/vendor/BusinessComcast'
                        }
                    ],
                },
                admin: {
                    self: {},
                    items: [
                        {
                            title: 'Dashboard',
                            root: true,
                            page: '/admin',
                        }, {
                            title: 'Invoices',
                            root: true,
                            page: '/admin/invoice',
                            icon: 'flaticon-coins'
                        }, {
                            title: 'Setting',
                            root: true,
                            page: '/admin/setting',
                            icon: 'flaticon-settings'
                        },
                        {
                            title: 'Users',
                            root: true,
                            page: '/admin/users',
                            icon: 'flaticon-users'
                        }
                    ]
                }
            }
        };
    }
    return MenuConfig;
}());
exports.MenuConfig = MenuConfig;
//# sourceMappingURL=menu.js.map