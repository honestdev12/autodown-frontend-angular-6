"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var collections_1 = require("@angular/cdk/collections");
var material_1 = require("@angular/material");
// RXJS
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
// Models
var query_params_model_1 = require("../../../core/models/query-params.model");
var spider_job_model_1 = require("../../../core/models/spider-job.model");
// Services
var spider_job_service_1 = require("../../../core/services/spider-job.service");
var schedule_service_1 = require("../../../core/services/schedule.service");
var dropbox_service_1 = require("../../../core/services/dropbox.service");
var router_1 = require("@angular/router");
var material_2 = require("@angular/material");
var spider_dialog_component_1 = require("./spider-dialog.component");
var upload_dialog_component_1 = require("./upload-dialog.component");
var confirm_dialog_component_1 = require("../../../content/partials/content/general/confirm-dialog/confirm-dialog.component");
var SpiderComponent = /** @class */ (function () {
    function SpiderComponent(spiderJobService, dropboxService, scheduleService, route, dialog) {
        this.spiderJobService = spiderJobService;
        this.dropboxService = dropboxService;
        this.scheduleService = scheduleService;
        this.route = route;
        this.dialog = dialog;
        this.dropboxes = [];
        this.spider_jobs = [];
        this.is_empty = true;
        this.len = 0;
        this.displayedColumns = ['name', 'username', 'password', 'directory', 'cDropbox', 'cStatus', 'actions'];
        this.new_spider = new spider_job_model_1.SpiderJobModel();
        this.loading$ = rxjs_1.of(true);
        this.selection = new collections_1.SelectionModel(true, []);
    }
    SpiderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.vendor = params['vendor'];
            _this.loadItems(true);
        });
        this.dropboxService.get().subscribe(function (res) { return _this.dropboxes = res.results; });
        this.sort.sortChange.subscribe(function () { return (_this.paginator.pageIndex = 0); });
        rxjs_1.merge(this.sort.sortChange, this.paginator.page)
            .pipe(operators_1.tap(function () {
            _this.is_empty = false;
            _this.loadItems();
        }))
            .subscribe();
        this.loadItems(true);
    };
    SpiderComponent.prototype.loadItems = function (first_time) {
        var _this = this;
        if (first_time === void 0) { first_time = false; }
        this.new_spider = new spider_job_model_1.SpiderJobModel();
        var queryParams = new query_params_model_1.QueryParamsModel(this.sort.direction, this.sort.active, this.paginator.pageIndex, first_time ? 10 : this.paginator.pageSize);
        this.loading$ = rxjs_1.of(true);
        this.spiderJobService.get(this.vendor, queryParams).subscribe(function (res) {
            _this.spider_jobs = res.results;
            _this.is_empty = _this.spider_jobs.length == 0;
            _this.len = _this.spider_jobs.length;
            _this.loading$ = rxjs_1.of(false);
        });
    };
    SpiderComponent.prototype.openSpiderDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(spider_dialog_component_1.SpiderDialog, {
            width: '60vw',
            data: { 'vendor': this.vendor, 'spider': this.new_spider, 'dropboxes': this.dropboxes, new: true }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result)
                _this.loadItems();
        });
    };
    SpiderComponent.prototype.update = function (spider_job) {
        var _this = this;
        var dialogRef = this.dialog.open(spider_dialog_component_1.SpiderDialog, {
            width: '60vw',
            data: { 'vendor': this.vendor, 'spider': spider_job, 'dropboxes': this.dropboxes, new: false }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result)
                _this.loadItems();
        });
    };
    SpiderComponent.prototype.upload = function (spider_job) {
        var dialogRef = this.dialog.open(upload_dialog_component_1.UploadDialog, {
            width: '60vw',
            data: spider_job
        });
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    SpiderComponent.prototype.delete = function (spider_job) {
        var _this = this;
        var dialogRef = this.dialog.open(confirm_dialog_component_1.ConfirmDialog, {
            width: '40vw',
            data: ''
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result)
                _this.spiderJobService.delete(_this.vendor, spider_job.id).subscribe(function (res) { return _this.loadItems(); });
        });
    };
    SpiderComponent.prototype.update_schedule = function (spider_job) {
        var _this = this;
        this.scheduleService.update(spider_job.schedule.id, spider_job.schedule).subscribe(function (res) {
            _this.loadItems();
        });
    };
    SpiderComponent.prototype.getItemStatusString = function (status) {
        if (status === void 0) { status = false; }
        switch (status) {
            case true:
                return 'Enabled';
            case false:
                return 'Disabled';
        }
        return '';
    };
    SpiderComponent.prototype.getItemCssClassByStatus = function (status) {
        if (status === void 0) { status = false; }
        switch (status) {
            case true:
                return 'success';
            case false:
                return 'metal';
        }
        return '';
    };
    __decorate([
        core_1.ViewChild(material_1.MatPaginator),
        __metadata("design:type", material_1.MatPaginator)
    ], SpiderComponent.prototype, "paginator", void 0);
    __decorate([
        core_1.ViewChild(material_1.MatSort),
        __metadata("design:type", material_1.MatSort)
    ], SpiderComponent.prototype, "sort", void 0);
    SpiderComponent = __decorate([
        core_1.Component({
            selector: 'm-spider',
            templateUrl: './spider.component.html'
        }),
        __metadata("design:paramtypes", [spider_job_service_1.SpiderJobService,
            dropbox_service_1.DropboxService,
            schedule_service_1.ScheduleService,
            router_1.ActivatedRoute,
            material_2.MatDialog])
    ], SpiderComponent);
    return SpiderComponent;
}());
exports.SpiderComponent = SpiderComponent;
//# sourceMappingURL=spider.component.js.map