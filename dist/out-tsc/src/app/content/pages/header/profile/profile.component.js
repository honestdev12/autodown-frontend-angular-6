"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var current_user_service_1 = require("../../../../core/services/current-user.service");
var user_service_1 = require("../../../../core/services/user.service");
var user_model_1 = require("../../../../core/models/user.model");
var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(currentUser, userService, ref) {
        this.currentUser = currentUser;
        this.userService = userService;
        this.ref = ref;
        this.user = new user_model_1.UserModel();
        this.spinner = {
            active: false,
            spinnerSize: 18,
            raised: true,
            buttonColor: 'primary',
            spinnerColor: 'accent',
            fullWidth: false
        };
        this.message = '';
        this.messageType = 'success';
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentUser.userUpdated$.subscribe(function (user) { return _this.user = user; }, function (err) { return _this.errorHandler(err); });
    };
    ProfileComponent.prototype.save = function () {
        var _this = this;
        this.spinner.active = false;
        this.userService.update(this.user.id, this.user).subscribe(function (user) { return _this.successHandler(user); }, function (err) { return _this.errorHandler(err); });
    };
    ProfileComponent.prototype.successHandler = function (user) {
        this.messageType = 'success';
        this.message = 'Updated Successfully!';
        this.user = user;
        this.ref.detectChanges();
        this.spinner.active = true;
    };
    ProfileComponent.prototype.errorHandler = function (err) {
        this.message = JSON.stringify(err.error);
        this.messageType = 'error';
        this.spinner.active = true;
    };
    ProfileComponent = __decorate([
        core_1.Component({
            selector: 'm-profile',
            templateUrl: './profile.component.html'
        }),
        __metadata("design:paramtypes", [current_user_service_1.CurrentUser,
            user_service_1.UserService,
            core_1.ChangeDetectorRef])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map