"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var pages_component_1 = require("./pages.component");
var spider_component_1 = require("./spider/spider.component");
var dropbox_component_1 = require("./dropbox/dropbox.component");
var ngx_permissions_1 = require("ngx-permissions");
var profile_component_1 = require("./header/profile/profile.component");
var error_page_component_1 = require("./snippets/error-page/error-page.component");
var spider_log_component_1 = require("./spider-log/spider-log.component");
var invoice_component_1 = require("./invoice/invoice.component");
var payment_setup_component_1 = require("./payment-setup/payment-setup.component");
var routes = [
    {
        path: '',
        component: pages_component_1.PagesComponent,
        canActivate: [ngx_permissions_1.NgxPermissionsGuard],
        data: {
            permissions: {
                only: ['ADMIN', 'USER'],
                redirectTo: '/login'
            }
        },
        children: [
            {
                path: '',
                loadChildren: './components/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'admin',
                loadChildren: './components/admin/admin.module#AdminModule',
                data: {
                    permissions: {
                        only: ['ADMIN'],
                        redirectTo: ''
                    }
                },
            },
            {
                path: 'builder',
                loadChildren: './builder/builder.module#BuilderModule'
            },
            {
                path: 'vendor/:vendor',
                component: spider_component_1.SpiderComponent
            },
            {
                path: 'spider-log/:spider_job_id',
                component: spider_log_component_1.SpiderLogComponent
            },
            {
                path: 'dropbox',
                component: dropbox_component_1.DropboxComponent
            },
            {
                path: 'profile',
                component: profile_component_1.ProfileComponent
            },
            {
                path: 'invoice',
                component: invoice_component_1.InvoiceComponent
            },
            {
                path: 'payment-setup',
                component: payment_setup_component_1.PaymentSetupComponent
            },
        ]
    },
    {
        path: 'login',
        canActivate: [ngx_permissions_1.NgxPermissionsGuard],
        loadChildren: './auth/auth.module#AuthModule',
        data: {
            permissions: {
                except: 'ADMIN'
            }
        },
    },
    {
        path: '404',
        component: error_page_component_1.ErrorPageComponent
    },
    {
        path: 'error/:type',
        component: error_page_component_1.ErrorPageComponent
    },
];
var PagesRoutingModule = /** @class */ (function () {
    function PagesRoutingModule() {
    }
    PagesRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], PagesRoutingModule);
    return PagesRoutingModule;
}());
exports.PagesRoutingModule = PagesRoutingModule;
//# sourceMappingURL=pages-routing.module.js.map