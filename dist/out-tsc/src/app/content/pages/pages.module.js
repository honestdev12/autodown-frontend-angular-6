"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var layout_module_1 = require("../layout/layout.module");
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var pages_routing_module_1 = require("./pages-routing.module");
var pages_component_1 = require("./pages.component");
var partials_module_1 = require("../partials/partials.module");
var widget_charts_module_1 = require("../partials/content/widgets/charts/widget-charts.module");
var action_component_1 = require("./header/action/action.component");
var profile_component_1 = require("./header/profile/profile.component");
var core_module_1 = require("../../core/core.module");
var angular_editor_1 = require("@kolkov/angular-editor");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/common/http");
var error_page_component_1 = require("./snippets/error-page/error-page.component");
var inner_component_1 = require("./components/inner/inner.component");
var spider_component_1 = require("./spider/spider.component");
var dropbox_component_1 = require("./dropbox/dropbox.component");
var spider_log_component_1 = require("./spider-log/spider-log.component");
var invoice_component_1 = require("./invoice/invoice.component");
var payment_setup_component_1 = require("./payment-setup/payment-setup.component");
var spider_dialog_component_1 = require("./spider/spider-dialog.component");
var invoice_dialog_component_1 = require("./invoice/invoice-dialog.component");
var payment_dialog_component_1 = require("./invoice/payment-dialog.component");
var dropbox_dialog_component_1 = require("./dropbox/dropbox-dialog.component");
var upload_dialog_component_1 = require("./spider/upload-dialog.component");
var confirm_dialog_component_1 = require("../../content/partials/content/general/confirm-dialog/confirm-dialog.component");
var info_dialog_component_1 = require("../../content/partials/content/general/info-dialog/info-dialog.component");
var material_moment_adapter_1 = require("@angular/material-moment-adapter");
var core_2 = require("@angular/material/core");
var ngx_perfect_scrollbar_1 = require("ngx-perfect-scrollbar");
var ngx_paypal_1 = require("ngx-paypal");
var ngx_stripe_1 = require("ngx-stripe");
var angular_material_fileupload_1 = require("angular-material-fileupload");
var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
// suppressScrollX: true
};
var MY_FORMATS = {
    parse: {
        dateInput: 'YYYY-MM-DD',
    },
    display: {
        dateInput: 'YYYY-MM-DD',
        monthYearLabel: 'YYYY MM',
        dateA11yLabel: 'YYYY-MM-DD',
        monthYearA11yLabel: 'YYYY MM',
    },
};
var material_1 = require("@angular/material");
var PagesModule = /** @class */ (function () {
    function PagesModule() {
    }
    PagesModule = __decorate([
        core_1.NgModule({
            declarations: [
                pages_component_1.PagesComponent,
                action_component_1.ActionComponent,
                profile_component_1.ProfileComponent,
                error_page_component_1.ErrorPageComponent,
                inner_component_1.InnerComponent,
                spider_component_1.SpiderComponent,
                payment_setup_component_1.PaymentSetupComponent,
                dropbox_component_1.DropboxComponent,
                spider_log_component_1.SpiderLogComponent,
                invoice_component_1.InvoiceComponent,
                spider_dialog_component_1.SpiderDialog,
                invoice_dialog_component_1.InvoiceDialog,
                payment_dialog_component_1.PaymentDialog,
                dropbox_dialog_component_1.DropboxDialog,
                upload_dialog_component_1.UploadDialog
            ],
            imports: [
                common_1.CommonModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                pages_routing_module_1.PagesRoutingModule,
                core_module_1.CoreModule,
                layout_module_1.LayoutModule,
                partials_module_1.PartialsModule,
                angular_editor_1.AngularEditorModule,
                material_1.MatButtonModule,
                material_1.MatInputModule,
                material_1.MatFormFieldModule,
                material_1.MatCheckboxModule,
                material_1.MatDialogModule,
                material_1.MatTableModule,
                material_1.MatIconModule,
                material_1.MatSortModule,
                material_1.MatProgressSpinnerModule,
                material_1.MatTableModule,
                material_1.MatPaginatorModule,
                material_1.MatSelectModule,
                material_1.MatProgressBarModule,
                material_1.MatTooltipModule,
                material_1.MatDatepickerModule,
                material_moment_adapter_1.MatMomentDateModule,
                angular_material_fileupload_1.MatFileUploadModule,
                ngx_perfect_scrollbar_1.PerfectScrollbarModule,
                widget_charts_module_1.WidgetChartsModule,
                ngx_paypal_1.NgxPayPalModule,
                ngx_stripe_1.NgxStripeModule.forRoot()
            ],
            entryComponents: [
                spider_dialog_component_1.SpiderDialog,
                confirm_dialog_component_1.ConfirmDialog,
                info_dialog_component_1.InfoDialog,
                invoice_dialog_component_1.InvoiceDialog,
                payment_dialog_component_1.PaymentDialog,
                dropbox_dialog_component_1.DropboxDialog,
                upload_dialog_component_1.UploadDialog,
            ],
            providers: [
                {
                    provide: ngx_perfect_scrollbar_1.PERFECT_SCROLLBAR_CONFIG,
                    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
                }, {
                    provide: core_2.MAT_DATE_FORMATS,
                    useValue: MY_FORMATS
                }
            ]
        })
    ], PagesModule);
    return PagesModule;
}());
exports.PagesModule = PagesModule;
//# sourceMappingURL=pages.module.js.map