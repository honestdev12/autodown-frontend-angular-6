"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var core_1 = require("@angular/core");
var objectPath = require("object-path");
var layout_config_service_1 = require("../../core/services/layout-config.service");
var class_init_service_1 = require("../../core/services/class-init.service");
var rxjs_1 = require("rxjs");
var layout_ref_service_1 = require("../../core/services/layout/layout-ref.service");
var animations_1 = require("@angular/animations");
var translation_service_1 = require("../../core/services/translation.service");
var PagesComponent = /** @class */ (function () {
    function PagesComponent(el, configService, classInitService, router, layoutRefService, animationBuilder, translationService) {
        var _this = this;
        this.el = el;
        this.configService = configService;
        this.classInitService = classInitService;
        this.router = router;
        this.layoutRefService = layoutRefService;
        this.animationBuilder = animationBuilder;
        this.translationService = translationService;
        this.classes = 'm-grid m-grid--hor m-grid--root m-page';
        this.selfLayout = 'blank';
        // class for the header container
        this.pageBodyClass$ = new rxjs_1.BehaviorSubject('');
        this.configService.onLayoutConfigUpdated$.subscribe(function (model) {
            var config = model.config;
            var pageBodyClass = '';
            _this.selfLayout = objectPath.get(config, 'self.layout');
            if (_this.selfLayout === 'boxed' || _this.selfLayout === 'wide') {
                pageBodyClass += ' m-container m-container--responsive m-container--xxl m-page__container';
            }
            _this.pageBodyClass$.next(pageBodyClass);
            _this.asideLeftDisplay = objectPath.get(config, 'aside.left.display');
            _this.asideRightDisplay = objectPath.get(config, 'aside.right.display');
        });
        this.classInitService.onClassesUpdated$.subscribe(function (classes) {
            _this.asideLeftCloseClass = objectPath.get(classes, 'aside_left_close');
        });
        // animate page load
        this.router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                if (_this.contenWrapper) {
                    // hide content
                    _this.contenWrapper.nativeElement.style.display = 'none';
                }
            }
            if (event instanceof router_1.NavigationEnd) {
                if (_this.contenWrapper) {
                    // show content back
                    _this.contenWrapper.nativeElement.style.display = '';
                    // animate the content
                    _this.animate(_this.contenWrapper.nativeElement);
                }
            }
        });
    }
    PagesComponent.prototype.ngOnInit = function () { };
    PagesComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.mContent) {
                // keep content element in the service
                _this.layoutRefService.addElement('content', _this.mContent.nativeElement);
            }
        });
    };
    /**
     * Animate page load
     */
    PagesComponent.prototype.animate = function (element) {
        this.player = this.animationBuilder
            .build([
            animations_1.style({ opacity: 0, transform: 'translateY(15px)' }),
            animations_1.animate('500ms ease', animations_1.style({ opacity: 1, transform: 'translateY(0)' })),
            animations_1.style({ transform: 'none' }),
        ])
            .create(element);
        this.player.play();
    };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], PagesComponent.prototype, "classes", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], PagesComponent.prototype, "selfLayout", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], PagesComponent.prototype, "asideLeftDisplay", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], PagesComponent.prototype, "asideRightDisplay", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], PagesComponent.prototype, "asideLeftCloseClass", void 0);
    __decorate([
        core_1.ViewChild('mContentWrapper'),
        __metadata("design:type", core_1.ElementRef)
    ], PagesComponent.prototype, "contenWrapper", void 0);
    __decorate([
        core_1.ViewChild('mContent'),
        __metadata("design:type", core_1.ElementRef)
    ], PagesComponent.prototype, "mContent", void 0);
    PagesComponent = __decorate([
        core_1.Component({
            selector: 'm-pages',
            templateUrl: './pages.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [core_1.ElementRef,
            layout_config_service_1.LayoutConfigService,
            class_init_service_1.ClassInitService,
            router_1.Router,
            layout_ref_service_1.LayoutRefService,
            animations_1.AnimationBuilder,
            translation_service_1.TranslationService])
    ], PagesComponent);
    return PagesComponent;
}());
exports.PagesComponent = PagesComponent;
//# sourceMappingURL=pages.component.js.map