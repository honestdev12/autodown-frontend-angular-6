"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var collections_1 = require("@angular/cdk/collections");
var material_1 = require("@angular/material");
// RXJS
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
// Models
var query_params_model_1 = require("../../../../../core/models/query-params.model");
var invoice_model_1 = require("../../../../../core/models/invoice.model");
var router_1 = require("@angular/router");
var admin_invoice_dialog_component_1 = require("./admin-invoice-dialog.component");
var confirm_dialog_component_1 = require("../../../../../content/partials/content/general/confirm-dialog/confirm-dialog.component");
var invoice_service_1 = require("../../../../../core/services/invoice.service");
var user_service_1 = require("../../../../../core/services/user.service");
var AdminInvoiceComponent = /** @class */ (function () {
    function AdminInvoiceComponent(route, dialog, invoiceService, userService) {
        this.route = route;
        this.dialog = dialog;
        this.invoiceService = invoiceService;
        this.userService = userService;
        this.invoices = [];
        this.is_empty = true;
        this.len = 0;
        this.displayedColumns = ['user', 'email', 'start_date', 'end_date', 'payment_type', 'payment_id', 'paid', 'count', 'amount', 'actions'];
        this.new_invoice = new invoice_model_1.InvoiceModel();
        this.loading$ = rxjs_1.of(true);
        this.selection = new collections_1.SelectionModel(true, []);
        this.userId = '';
        this.paid = 'unpaid';
        this.users = [];
    }
    AdminInvoiceComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.userId = 'userId' in params ? params['userId'] : '';
            _this.paid = 'paid' in params ? params['paid'] : 'unpaid';
        });
        this.userService.get().subscribe(function (res) { return _this.users = res.results; });
        this.sort.sortChange.subscribe(function () { return (_this.paginator.pageIndex = 0); });
        rxjs_1.merge(this.sort.sortChange, this.paginator.page)
            .pipe(operators_1.tap(function () {
            _this.is_empty = false;
            _this.loadItems();
        }))
            .subscribe();
        this.loadItems(true);
    };
    AdminInvoiceComponent.prototype.loadItems = function (first_time) {
        var _this = this;
        if (first_time === void 0) { first_time = false; }
        this.new_invoice = new invoice_model_1.InvoiceModel();
        var queryParams = new query_params_model_1.QueryParamsModel(this.sort.direction, this.sort.active, this.paginator.pageIndex, first_time ? 10 : this.paginator.pageSize);
        this.loading$ = rxjs_1.of(true);
        this.invoiceService.get(queryParams, this.paid, true, this.userId).subscribe(function (res) {
            _this.invoices = res.results;
            _this.loading$ = rxjs_1.of(false);
            _this.is_empty = _this.invoices.length == 0;
        });
    };
    AdminInvoiceComponent.prototype.createInvoiceDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(admin_invoice_dialog_component_1.AdminInvoiceDialog, {
            width: '60vw',
            data: {
                'invoice': this.new_invoice,
                new: true,
                'users': this.users
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result)
                _this.loadItems();
        });
    };
    AdminInvoiceComponent.prototype.editInvoiceDialog = function (invoice) {
        var _this = this;
        var dialogRef = this.dialog.open(admin_invoice_dialog_component_1.AdminInvoiceDialog, {
            width: '60vw',
            data: { 'invoice': invoice, new: false, 'users': this.users }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result)
                _this.loadItems();
        });
    };
    AdminInvoiceComponent.prototype.delete = function (invoice) {
        var _this = this;
        var dialogRef = this.dialog.open(confirm_dialog_component_1.ConfirmDialog, {
            width: '40vw',
            data: ''
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result)
                _this.invoiceService.delete(invoice.id).subscribe(function (res) { return _this.loadItems(); });
        });
    };
    AdminInvoiceComponent.prototype.getItemStatusString = function (status) {
        if (status === void 0) { status = false; }
        switch (status) {
            case true:
                return 'Paid';
            case false:
                return 'Unpaid';
        }
        return '';
    };
    AdminInvoiceComponent.prototype.getItemCssClassByStatus = function (status) {
        if (status === void 0) { status = false; }
        switch (status) {
            case true:
                return 'success';
            case false:
                return 'metal';
        }
        return '';
    };
    AdminInvoiceComponent.prototype.getTime = function (time) {
        if (time)
            return time.split('T')[0];
        else
            return '';
    };
    AdminInvoiceComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.invoices.length;
        return numSelected == numRows;
    };
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    AdminInvoiceComponent.prototype.masterToggle = function () {
        var _this = this;
        this.isAllSelected() ?
            this.selection.clear() :
            this.invoices.forEach(function (row) { return _this.selection.select(row); });
    };
    __decorate([
        core_1.ViewChild(material_1.MatPaginator),
        __metadata("design:type", material_1.MatPaginator)
    ], AdminInvoiceComponent.prototype, "paginator", void 0);
    __decorate([
        core_1.ViewChild(material_1.MatSort),
        __metadata("design:type", material_1.MatSort)
    ], AdminInvoiceComponent.prototype, "sort", void 0);
    AdminInvoiceComponent = __decorate([
        core_1.Component({
            selector: 'm-admin-invoice',
            templateUrl: './admin-invoice.component.html'
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            material_1.MatDialog,
            invoice_service_1.InvoiceService,
            user_service_1.UserService])
    ], AdminInvoiceComponent);
    return AdminInvoiceComponent;
}());
exports.AdminInvoiceComponent = AdminInvoiceComponent;
//# sourceMappingURL=admin-invoice.component.js.map