"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var layout_module_1 = require("../../../layout/layout.module");
var partials_module_1 = require("../../../partials/partials.module");
var list_timeline_module_1 = require("../../../partials/layout/quick-sidebar/list-timeline/list-timeline.module");
var widget_charts_module_1 = require("../../../partials/content/widgets/charts/widget-charts.module");
var admin_component_1 = require("./admin.component");
var admin_invoice_component_1 = require("./admin-invoice/admin-invoice.component");
var admin_invoice_dialog_component_1 = require("./admin-invoice/admin-invoice-dialog.component");
var setting_component_1 = require("./setting/setting.component");
var user_component_1 = require("./users/user.component");
var forms_1 = require("@angular/forms");
var material_1 = require("@angular/material");
var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                layout_module_1.LayoutModule,
                partials_module_1.PartialsModule,
                list_timeline_module_1.ListTimelineModule,
                widget_charts_module_1.WidgetChartsModule,
                material_1.MatButtonModule,
                material_1.MatInputModule,
                material_1.MatCheckboxModule,
                material_1.MatDialogModule,
                material_1.MatIconModule,
                material_1.MatSortModule,
                material_1.MatProgressSpinnerModule,
                material_1.MatTableModule,
                material_1.MatPaginatorModule,
                material_1.MatSelectModule,
                material_1.MatProgressBarModule,
                material_1.MatTooltipModule,
                material_1.MatDatepickerModule,
                forms_1.FormsModule,
                router_1.RouterModule.forChild([
                    {
                        path: '',
                        component: admin_component_1.AdminComponent
                    }, {
                        path: 'invoice',
                        component: admin_invoice_component_1.AdminInvoiceComponent
                    }, {
                        path: ':userId/:paid/invoice',
                        component: admin_invoice_component_1.AdminInvoiceComponent
                    }, {
                        path: 'setting',
                        component: setting_component_1.SettingComponent
                    }, {
                        path: 'users',
                        component: user_component_1.UserComponent
                    }
                ])
            ],
            providers: [],
            declarations: [
                admin_component_1.AdminComponent,
                admin_invoice_component_1.AdminInvoiceComponent,
                admin_invoice_dialog_component_1.AdminInvoiceDialog,
                setting_component_1.SettingComponent,
                user_component_1.UserComponent
            ],
            entryComponents: [
                admin_invoice_dialog_component_1.AdminInvoiceDialog
            ]
        })
    ], AdminModule);
    return AdminModule;
}());
exports.AdminModule = AdminModule;
//# sourceMappingURL=admin.module.js.map