"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var collections_1 = require("@angular/cdk/collections");
var material_1 = require("@angular/material");
// RXJS
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
// Models
var query_params_model_1 = require("../../../../../core/models/query-params.model");
var confirm_dialog_component_1 = require("../../../../../content/partials/content/general/confirm-dialog/confirm-dialog.component");
var user_service_1 = require("../../../../../core/services/user.service");
var UserComponent = /** @class */ (function () {
    function UserComponent(dialog, userService) {
        this.dialog = dialog;
        this.userService = userService;
        this.users = [];
        this.is_empty = true;
        this.len = 0;
        this.displayedColumns = ['username', 'user', 'email', 'unpaid_invoice', 'active', 'roles', 'actions',];
        this.loading$ = rxjs_1.of(true);
        this.selection = new collections_1.SelectionModel(true, []);
    }
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sort.sortChange.subscribe(function () { return (_this.paginator.pageIndex = 0); });
        rxjs_1.merge(this.sort.sortChange, this.paginator.page)
            .pipe(operators_1.tap(function () {
            _this.is_empty = false;
            _this.loadItems();
        }))
            .subscribe();
        this.loadItems(true);
    };
    UserComponent.prototype.loadItems = function (first_time) {
        var _this = this;
        if (first_time === void 0) { first_time = false; }
        var queryParams = new query_params_model_1.QueryParamsModel(this.sort.direction, this.sort.active, this.paginator.pageIndex, first_time ? 10 : this.paginator.pageSize);
        this.loading$ = rxjs_1.of(true);
        this.userService.get(queryParams).subscribe(function (res) {
            _this.users = res.results;
            _this.loading$ = rxjs_1.of(false);
            _this.is_empty = _this.users.length == 0;
        });
    };
    UserComponent.prototype.delete = function (user) {
        var _this = this;
        var dialogRef = this.dialog.open(confirm_dialog_component_1.ConfirmDialog, {
            width: '40vw',
            data: ''
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result)
                _this.userService.delete(user.id).subscribe(function (res) { return _this.loadItems(); });
        });
    };
    UserComponent.prototype.getItemStatusString = function (status) {
        if (status === void 0) { status = false; }
        switch (status) {
            case true:
                return 'Enabled';
            case false:
                return 'Disabled';
        }
        return '';
    };
    UserComponent.prototype.getItemCssClassByStatus = function (status) {
        if (status === void 0) { status = false; }
        switch (status) {
            case true:
                return 'success';
            case false:
                return 'metal';
        }
        return '';
    };
    UserComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.users.length;
        return numSelected == numRows;
    };
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    UserComponent.prototype.masterToggle = function () {
        var _this = this;
        this.isAllSelected() ?
            this.selection.clear() :
            this.users.forEach(function (row) { return _this.selection.select(row); });
    };
    __decorate([
        core_1.ViewChild(material_1.MatPaginator),
        __metadata("design:type", material_1.MatPaginator)
    ], UserComponent.prototype, "paginator", void 0);
    __decorate([
        core_1.ViewChild(material_1.MatSort),
        __metadata("design:type", material_1.MatSort)
    ], UserComponent.prototype, "sort", void 0);
    UserComponent = __decorate([
        core_1.Component({
            selector: 'm-user',
            templateUrl: './user.component.html'
        }),
        __metadata("design:paramtypes", [material_1.MatDialog,
            user_service_1.UserService])
    ], UserComponent);
    return UserComponent;
}());
exports.UserComponent = UserComponent;
//# sourceMappingURL=user.component.js.map