"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var layout_config_service_1 = require("../../../../core/services/layout-config.service");
var subheader_service_1 = require("../../../../core/services/layout/subheader.service");
var objectPath = require("object-path");
var AdminComponent = /** @class */ (function () {
    function AdminComponent(router, layoutConfigService, subheaderService) {
        this.router = router;
        this.layoutConfigService = layoutConfigService;
        this.subheaderService = subheaderService;
    }
    AdminComponent.prototype.ngOnInit = function () {
        // change page config, refer to config/layout.ts
        var newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.aside.left.display', false);
        this.layoutConfigService.setModel(newLayoutOption, true);
    };
    AdminComponent = __decorate([
        core_1.Component({
            selector: 'm-admin',
            templateUrl: './admin.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [router_1.Router,
            layout_config_service_1.LayoutConfigService,
            subheader_service_1.SubheaderService])
    ], AdminComponent);
    return AdminComponent;
}());
exports.AdminComponent = AdminComponent;
//# sourceMappingURL=admin.component.js.map