"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var setting_service_1 = require("../../../../../core/services/setting.service");
var setting_model_1 = require("../../../../../core/models/setting.model");
var SettingComponent = /** @class */ (function () {
    function SettingComponent(settingService, ref) {
        this.settingService = settingService;
        this.ref = ref;
        this.spinner = {
            active: false,
            spinnerSize: 18,
            raised: true,
            buttonColor: 'primary',
            spinnerColor: 'accent',
            fullWidth: false
        };
        this.message = null;
        this.messageType = 'success';
        this.setting = new setting_model_1.SettingModel();
    }
    SettingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.settingService.settingUpdated$.subscribe(function (setting) {
            _this.setting = setting;
            _this.ref.detectChanges();
        }, function (err) { return console.log(err); });
    };
    SettingComponent.prototype.save = function () {
        this.spinner.active = false;
        this.settingService.update(this.setting);
    };
    SettingComponent.prototype.successHandler = function (setting) {
        this.setting = setting;
        this.messageType = 'success';
        this.message = 'Updated Successfully!';
        this.ref.detectChanges();
        this.spinner.active = false;
    };
    SettingComponent.prototype.errorHandler = function (err) {
        this.message = JSON.stringify(err.error);
        this.messageType = 'error';
        this.spinner.active = false;
    };
    SettingComponent = __decorate([
        core_1.Component({
            selector: 'm-setting',
            templateUrl: './setting.component.html'
        }),
        __metadata("design:paramtypes", [setting_service_1.SettingService,
            core_1.ChangeDetectorRef])
    ], SettingComponent);
    return SettingComponent;
}());
exports.SettingComponent = SettingComponent;
//# sourceMappingURL=setting.component.js.map