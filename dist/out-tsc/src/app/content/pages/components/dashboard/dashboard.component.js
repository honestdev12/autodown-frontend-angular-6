"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var layout_config_service_1 = require("../../../../core/services/layout-config.service");
var subheader_service_1 = require("../../../../core/services/layout/subheader.service");
var objectPath = require("object-path");
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(router, layoutConfigService, subheaderService) {
        this.router = router;
        this.layoutConfigService = layoutConfigService;
        this.subheaderService = subheaderService;
        this.lineChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true
        };
        this.lineChartLabels = ['2018-10-3', '2018-10-4', '2018-10-5', '2018-10-6', '2018-10-7', '2018-10-8', '2018-10-9'];
        this.lineChartType = 'line';
        this.lineChartLegend = true;
        this.lineChartData = [
            { data: [65, 59, 80, 81, 56, 55, 40], label: 'Spider 1' },
            { data: [28, 48, 40, 19, 86, 27, 90], label: 'Spider 2' }
        ];
        this.periods = ['week', 'month'];
        this.period = 'week';
    }
    DashboardComponent.prototype.ngOnInit = function () {
        // change page config, refer to config/layout.ts
        var newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.aside.left.display', false);
        // const newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.menu.header.display', false);
        this.layoutConfigService.setModel(newLayoutOption, true);
    };
    var _a;
    __decorate([
        core_1.ViewChild("baseChart"),
        __metadata("design:type", typeof (_a = typeof BaseChartDirective !== "undefined" && BaseChartDirective) === "function" ? _a : Object)
    ], DashboardComponent.prototype, "chart", void 0);
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'm-dashboard',
            templateUrl: './dashboard.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [router_1.Router,
            layout_config_service_1.LayoutConfigService,
            subheader_service_1.SubheaderService])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map