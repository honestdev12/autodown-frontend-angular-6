"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var paypal_service_1 = require("../../../core/services/paypal.service");
var ngx_paypal_1 = require("ngx-paypal");
var rxjs_1 = require("rxjs");
var PaymentSetupComponent = /** @class */ (function () {
    function PaymentSetupComponent(payPalService) {
        this.payPalService = payPalService;
        this.message = null;
        this.messageType = 'success';
    }
    PaymentSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.payPalConfig = new ngx_paypal_1.PayPalConfig(ngx_paypal_1.PayPalIntegrationType.ServerSideREST, ngx_paypal_1.PayPalEnvironment.Sandbox, {
            commit: false,
            button: {
                label: 'pay',
                size: 'large',
                shape: 'rect',
                color: 'blue',
                tagline: false
            },
            onPaymentComplete: function (data, actions) {
            },
            onCancel: function (data, actions) {
                console.log('OnCancel');
            },
            onError: function (err) {
                console.log('OnError');
            },
            onAuthorize: function (data, actions) {
                return _this.payPalService.excute({
                    paymentID: data.paymentID,
                    payerID: data.payerID
                }).pipe(function (res) { return rxjs_1.of(console.log(res)); });
            }
        });
    };
    PaymentSetupComponent = __decorate([
        core_1.Component({
            selector: 'm-payment-setup',
            templateUrl: './payment-setup.component.html'
        }),
        __metadata("design:paramtypes", [paypal_service_1.PayPalService])
    ], PaymentSetupComponent);
    return PaymentSetupComponent;
}());
exports.PaymentSetupComponent = PaymentSetupComponent;
//# sourceMappingURL=payment-setup.component.js.map