"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var layout_module_1 = require("../../layout/layout.module");
var builder_component_1 = require("./builder.component");
var partials_module_1 = require("../../partials/partials.module");
var forms_1 = require("@angular/forms");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var material_1 = require("@angular/material");
var ngx_highlightjs_1 = require("ngx-highlightjs");
var ngx_perfect_scrollbar_1 = require("ngx-perfect-scrollbar");
var BuilderModule = /** @class */ (function () {
    function BuilderModule() {
    }
    BuilderModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                layout_module_1.LayoutModule,
                partials_module_1.PartialsModule,
                forms_1.FormsModule,
                ng_bootstrap_1.NgbModule,
                material_1.MatTabsModule,
                ngx_perfect_scrollbar_1.PerfectScrollbarModule,
                ngx_highlightjs_1.HighlightModule.forRoot({ theme: 'googlecode' }),
                router_1.RouterModule.forChild([
                    {
                        path: '',
                        component: builder_component_1.BuilderComponent
                    }
                ])
            ],
            providers: [],
            declarations: [builder_component_1.BuilderComponent]
        })
    ], BuilderModule);
    return BuilderModule;
}());
exports.BuilderModule = BuilderModule;
//# sourceMappingURL=builder.module.js.map