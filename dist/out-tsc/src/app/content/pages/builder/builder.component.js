"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var layout_config_storage_service_1 = require("../../../core/services/layout-config-storage.service");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var layout_config_service_1 = require("../../../core/services/layout-config.service");
var class_init_service_1 = require("../../../core/services/class-init.service");
var layout_1 = require("../../../config/layout");
var BuilderComponent = /** @class */ (function () {
    function BuilderComponent(layoutConfigService, classInitService, layoutConfigStorageService) {
        var _this = this;
        this.layoutConfigService = layoutConfigService;
        this.classInitService = classInitService;
        this.layoutConfigStorageService = layoutConfigStorageService;
        this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (config) {
            _this.model = config.config;
        });
    }
    BuilderComponent.prototype.ngOnInit = function () { };
    BuilderComponent.prototype.submitPreview = function (form) {
        this.layoutConfigService.setModel(new layout_1.LayoutConfig(this.model));
    };
    BuilderComponent.prototype.resetPreview = function (event) {
        event.preventDefault();
        this.layoutConfigStorageService.resetConfig();
        location.reload();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], BuilderComponent.prototype, "model", void 0);
    __decorate([
        core_1.ViewChild('builderForm'),
        __metadata("design:type", forms_1.NgForm)
    ], BuilderComponent.prototype, "form", void 0);
    BuilderComponent = __decorate([
        core_1.Component({
            selector: 'm-builder',
            templateUrl: './builder.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [layout_config_service_1.LayoutConfigService,
            class_init_service_1.ClassInitService,
            layout_config_storage_service_1.LayoutConfigStorageService])
    ], BuilderComponent);
    return BuilderComponent;
}());
exports.BuilderComponent = BuilderComponent;
//# sourceMappingURL=builder.component.js.map