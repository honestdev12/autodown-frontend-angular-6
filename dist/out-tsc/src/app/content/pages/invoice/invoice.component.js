"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var collections_1 = require("@angular/cdk/collections");
var material_1 = require("@angular/material");
// RXJS
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
var objectPath = require("object-path");
// Models
var query_params_model_1 = require("../../../core/models/query-params.model");
var invoice_model_1 = require("../../../core/models/invoice.model");
var router_1 = require("@angular/router");
var material_2 = require("@angular/material");
var payment_dialog_component_1 = require("./payment-dialog.component");
var confirm_dialog_component_1 = require("../../../content/partials/content/general/confirm-dialog/confirm-dialog.component");
var info_dialog_component_1 = require("../../../content/partials/content/general/info-dialog/info-dialog.component");
var layout_config_service_1 = require("../../../core/services/layout-config.service");
var invoice_service_1 = require("../../../core/services/invoice.service");
var InvoiceComponent = /** @class */ (function () {
    function InvoiceComponent(route, dialog, layoutConfigService, invoiceService) {
        this.route = route;
        this.dialog = dialog;
        this.layoutConfigService = layoutConfigService;
        this.invoiceService = invoiceService;
        this.invoices = [];
        this.is_empty = true;
        this.len = 0;
        this.displayedColumns = ['select', 'start_date', 'end_date', 'bill_date', 'amount', 'payment_type', 'payment_id', 'paid', 'actions'];
        this.new_invoice = new invoice_model_1.InvoiceModel();
        this.loading$ = rxjs_1.of(true);
        this.selection = new collections_1.SelectionModel(true, []);
    }
    InvoiceComponent.prototype.ngOnInit = function () {
        var _this = this;
        // change page config, refer to config/layout.ts
        var newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.aside.left.display', false);
        // const newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.menu.header.display', false);
        this.layoutConfigService.setModel(newLayoutOption, true);
        this.sort.sortChange.subscribe(function () { return (_this.paginator.pageIndex = 0); });
        rxjs_1.merge(this.sort.sortChange, this.paginator.page)
            .pipe(operators_1.tap(function () {
            _this.is_empty = false;
            _this.loadItems();
        }))
            .subscribe();
        this.loadItems(true);
    };
    InvoiceComponent.prototype.loadItems = function (first_time) {
        var _this = this;
        if (first_time === void 0) { first_time = false; }
        this.new_invoice = new invoice_model_1.InvoiceModel();
        var queryParams = new query_params_model_1.QueryParamsModel(this.sort.direction, this.sort.active, this.paginator.pageIndex, first_time ? 10 : this.paginator.pageSize);
        this.loading$ = rxjs_1.of(true);
        this.invoiceService.get(queryParams).subscribe(function (res) {
            _this.invoices = res.results;
            _this.loading$ = rxjs_1.of(false);
            _this.is_empty = _this.invoices.length == 0;
        });
    };
    // createInvoiceDialog(): void {
    // 	const dialogRef = this.dialog.open(InvoiceDialog, {
    //       	width: '60vw',
    //       	data: {'invoice': this.new_invoice, new: true}
    //     });
    //     dialogRef.afterClosed().subscribe(result => {
    //     	if (result) this.loadItems();
    //     });
    // }
    // editInvoiceDialog(invoice: InvoiceModel): void {
    // 	const dialogRef = this.dialog.open(InvoiceDialog, {
    //       	width: '60vw',
    //       	data: {'invoice': invoice, new: false}
    //     });
    //     dialogRef.afterClosed().subscribe(result => {
    //     	if (result) this.loadItems();
    //     });
    // }
    InvoiceComponent.prototype.openPaymentDialog = function (invoice) {
        var _this = this;
        if (invoice === void 0) { invoice = null; }
        var invoices;
        invoices = invoice ? [invoice] : this.selection.selected.filter(function (invoice) { return !invoice.paid; });
        if (invoices.length == 0) {
            var dialogRef = this.dialog.open(info_dialog_component_1.InfoDialog, {
                width: '600px',
                data: 'Please select the unpaid invoice!'
            });
        }
        else {
            var dialogRef = this.dialog.open(payment_dialog_component_1.PaymentDialog, {
                width: '600px',
                data: invoices
            });
            dialogRef.afterClosed().subscribe(function (result) {
                if (result)
                    _this.loadItems();
            });
        }
    };
    InvoiceComponent.prototype.delete = function (invoice) {
        var _this = this;
        var dialogRef = this.dialog.open(confirm_dialog_component_1.ConfirmDialog, {
            width: '40vw',
            data: ''
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result)
                _this.invoiceService.delete(invoice.id).subscribe(function (res) { return _this.loadItems(); });
        });
    };
    InvoiceComponent.prototype.getItemStatusString = function (status) {
        if (status === void 0) { status = false; }
        switch (status) {
            case true:
                return 'Paid';
            case false:
                return 'Unpaid';
        }
        return '';
    };
    InvoiceComponent.prototype.getItemCssClassByStatus = function (status) {
        if (status === void 0) { status = false; }
        switch (status) {
            case true:
                return 'success';
            case false:
                return 'metal';
        }
        return '';
    };
    InvoiceComponent.prototype.getTime = function (time) {
        if (time)
            return time.split('T')[0];
        else
            return '';
    };
    InvoiceComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.invoices.length;
        return numSelected == numRows;
    };
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    InvoiceComponent.prototype.masterToggle = function () {
        var _this = this;
        this.isAllSelected() ?
            this.selection.clear() :
            this.invoices.forEach(function (row) { return _this.selection.select(row); });
    };
    __decorate([
        core_1.ViewChild(material_1.MatPaginator),
        __metadata("design:type", material_1.MatPaginator)
    ], InvoiceComponent.prototype, "paginator", void 0);
    __decorate([
        core_1.ViewChild(material_1.MatSort),
        __metadata("design:type", material_1.MatSort)
    ], InvoiceComponent.prototype, "sort", void 0);
    InvoiceComponent = __decorate([
        core_1.Component({
            selector: 'm-invoice',
            templateUrl: './invoice.component.html'
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            material_2.MatDialog,
            layout_config_service_1.LayoutConfigService,
            invoice_service_1.InvoiceService])
    ], InvoiceComponent);
    return InvoiceComponent;
}());
exports.InvoiceComponent = InvoiceComponent;
//# sourceMappingURL=invoice.component.js.map