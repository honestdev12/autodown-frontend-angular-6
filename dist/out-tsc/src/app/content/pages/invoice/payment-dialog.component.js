"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var material_1 = require("@angular/material");
var ngx_paypal_1 = require("ngx-paypal");
var paypal_service_1 = require("../../../core/services/paypal.service");
var ngx_stripe_1 = require("ngx-stripe");
var forms_1 = require("@angular/forms");
var PaymentDialog = /** @class */ (function () {
    function PaymentDialog(dialogRef, data, payPalService, fb, stripeService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.payPalService = payPalService;
        this.fb = fb;
        this.stripeService = stripeService;
        this.cardOptions = {
            style: {
                base: {
                    iconColor: '#666EE8',
                    color: '#31325F',
                    lineHeight: '40px',
                    fontWeight: 300,
                    fontSize: '18px',
                    '::placeholder': {
                        color: '#CFD7E0'
                    }
                }
            }
        };
        this.elementsOptions = {
            locale: 'es'
        };
        this.displayedColumns = ['no', 'period', 'price'];
        this.price = 0;
    }
    PaymentDialog.prototype.ngOnInit = function () {
        this.price = this.data.map(function (invoice) { return invoice.price_amount; }).reduce(function (prev, cur) { return prev + cur; });
        this.payPalConfig = new ngx_paypal_1.PayPalConfig(ngx_paypal_1.PayPalIntegrationType.ClientSideREST, ngx_paypal_1.PayPalEnvironment.Sandbox, {
            commit: false,
            button: {
                label: 'pay',
                size: 'small',
                shape: 'rect',
                color: 'blue',
                tagline: false
            },
            client: {
                sandbox: 'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R'
            },
            onPaymentComplete: function (data, actions) {
            },
            onCancel: function (data, actions) {
                console.log('OnCancel');
            },
            onError: function (err) {
                console.log('OnError');
            },
            transactions: [{
                    amount: {
                        currency: 'USD',
                        total: this.price
                    }
                }]
        });
        this.stripeTest = this.fb.group({
            name: ['', [forms_1.Validators.required]]
        });
        this.stripeService.setKey('environment_key');
    };
    PaymentDialog.prototype.getTime = function (time) {
        if (time)
            return time.split('T')[0];
        else
            return '';
    };
    PaymentDialog.prototype.cancel = function () {
        this.dialogRef.close(false);
    };
    PaymentDialog.prototype.pay = function () {
        var name = this.stripeTest.get('name').value;
        this.stripeService
            .createToken(this.card.getCard(), { name: name })
            .subscribe(function (result) {
            if (result.token) {
                // Use the token to create a charge or a customer
                // https://stripe.com/docs/charges
                console.log(result.token.id);
            }
            else if (result.error) {
                console.log(result.error.message);
            }
        });
    };
    __decorate([
        core_1.ViewChild(ngx_stripe_1.StripeCardComponent),
        __metadata("design:type", ngx_stripe_1.StripeCardComponent)
    ], PaymentDialog.prototype, "card", void 0);
    PaymentDialog = __decorate([
        core_1.Component({
            selector: 'payment-dialog',
            templateUrl: './payment-dialog.component.html',
        }),
        __param(1, core_1.Inject(material_1.MAT_DIALOG_DATA)),
        __metadata("design:paramtypes", [material_1.MatDialogRef, Array, paypal_service_1.PayPalService,
            forms_1.FormBuilder,
            ngx_stripe_1.StripeService])
    ], PaymentDialog);
    return PaymentDialog;
}());
exports.PaymentDialog = PaymentDialog;
//# sourceMappingURL=payment-dialog.component.js.map