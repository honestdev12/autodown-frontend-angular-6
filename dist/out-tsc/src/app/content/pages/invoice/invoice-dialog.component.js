"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var material_1 = require("@angular/material");
var invoice_service_1 = require("../../../core/services/invoice.service");
var InvoiceDialog = /** @class */ (function () {
    function InvoiceDialog(dialogRef, data, invoiceService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.invoiceService = invoiceService;
        this.message = null;
        this.messageType = 'success';
    }
    InvoiceDialog.prototype.cancel = function () {
        this.dialogRef.close(false);
    };
    InvoiceDialog.prototype.save = function () {
        var _this = this;
        this.invoiceService.create(this.data.invoice).subscribe(function (invoice) { return _this.successHandler(invoice); }, function (err) { return _this.errorHandler(err); });
    };
    InvoiceDialog.prototype.update = function () {
        var _this = this;
        this.invoiceService.update(this.data.invoice.id, this.data.invoice).subscribe(function (invoice) { return _this.successHandler(invoice); }, function (err) { return _this.errorHandler(err); });
    };
    InvoiceDialog.prototype.errorHandler = function (err) {
        this.message = JSON.stringify(err.error);
        this.messageType = 'error';
    };
    InvoiceDialog.prototype.successHandler = function (invoice) {
        var _this = this;
        this.messageType = 'success';
        this.message = this.data.new ? 'Added Successfully.' : 'Updated Successfully';
        setTimeout(function () {
            _this.message = null;
            _this.dialogRef.close(true);
        }, 1000);
    };
    InvoiceDialog = __decorate([
        core_1.Component({
            selector: 'invoice-dialog',
            templateUrl: './invoice-dialog.component.html',
        }),
        __param(1, core_1.Inject(material_1.MAT_DIALOG_DATA)),
        __metadata("design:paramtypes", [material_1.MatDialogRef, Object, invoice_service_1.InvoiceService])
    ], InvoiceDialog);
    return InvoiceDialog;
}());
exports.InvoiceDialog = InvoiceDialog;
//# sourceMappingURL=invoice-dialog.component.js.map