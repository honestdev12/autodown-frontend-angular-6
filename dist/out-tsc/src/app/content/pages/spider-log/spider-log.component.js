"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var collections_1 = require("@angular/cdk/collections");
var material_1 = require("@angular/material");
// RXJS
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
// Models
var query_params_model_1 = require("../../../core/models/query-params.model");
// Services
var spider_log_service_1 = require("../../../core/services/spider-log.service");
var router_1 = require("@angular/router");
var material_2 = require("@angular/material");
var SpiderLogComponent = /** @class */ (function () {
    function SpiderLogComponent(spiderLogService, route, dialog) {
        this.spiderLogService = spiderLogService;
        this.route = route;
        this.dialog = dialog;
        this.spider_logs = [];
        this.is_empty = true;
        this.len = 0;
        this.displayedColumns = ['no', 'file_name', 'file_link', 'vendor_name', 'bill_date', 'invoice_id', 'account_number'];
        this.loading$ = rxjs_1.of(true);
        this.selection = new collections_1.SelectionModel(true, []);
    }
    SpiderLogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.spider_job_id = params['spider_job_id'];
            _this.loadItems(true);
        });
        this.sort.sortChange.subscribe(function () { return (_this.paginator.pageIndex = 0); });
        rxjs_1.merge(this.sort.sortChange, this.paginator.page)
            .pipe(operators_1.tap(function () {
            _this.is_empty = false;
            _this.loadItems();
        }))
            .subscribe();
        this.loadItems(true);
    };
    SpiderLogComponent.prototype.loadItems = function (first_time) {
        var _this = this;
        if (first_time === void 0) { first_time = false; }
        var queryParams = new query_params_model_1.QueryParamsModel(this.sort.direction, this.sort.active, this.paginator.pageIndex, first_time ? 10 : this.paginator.pageSize);
        this.loading$ = rxjs_1.of(true);
        this.spiderLogService.get(queryParams, this.spider_job_id).subscribe(function (res) {
            _this.spider_logs = res.results;
            _this.is_empty = _this.spider_logs.length == 0;
            _this.len = _this.spider_logs.length;
            _this.loading$ = rxjs_1.of(false);
        });
        this.selection.clear();
    };
    __decorate([
        core_1.ViewChild(material_1.MatPaginator),
        __metadata("design:type", material_1.MatPaginator)
    ], SpiderLogComponent.prototype, "paginator", void 0);
    __decorate([
        core_1.ViewChild(material_1.MatSort),
        __metadata("design:type", material_1.MatSort)
    ], SpiderLogComponent.prototype, "sort", void 0);
    SpiderLogComponent = __decorate([
        core_1.Component({
            selector: 'm-spider-log',
            templateUrl: './spider-log.component.html'
        }),
        __metadata("design:paramtypes", [spider_log_service_1.SpiderLogService,
            router_1.ActivatedRoute,
            material_2.MatDialog])
    ], SpiderLogComponent);
    return SpiderLogComponent;
}());
exports.SpiderLogComponent = SpiderLogComponent;
//# sourceMappingURL=spider-log.component.js.map