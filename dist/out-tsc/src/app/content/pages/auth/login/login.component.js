"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var authentication_service_1 = require("../../../../core/auth/authentication.service");
var router_1 = require("@angular/router");
var rxjs_1 = require("rxjs");
var auth_notice_service_1 = require("../../../../core/auth/auth-notice.service");
var forms_1 = require("@angular/forms");
var objectPath = require("object-path");
var core_2 = require("@ngx-translate/core");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, router, authNoticeService, translate, cdr) {
        this.authService = authService;
        this.router = router;
        this.authNoticeService = authNoticeService;
        this.translate = translate;
        this.cdr = cdr;
        this.model = { username: '', password: '' };
        this.classes = 'm-login__signin';
        this.actionChange = new rxjs_1.Subject();
        this.loading = false;
        this.errors = [];
        this.spinner = {
            active: false,
            spinnerSize: 18,
            raised: true,
            buttonColor: 'primary',
            spinnerColor: 'accent',
            fullWidth: false
        };
    }
    LoginComponent.prototype.submit = function () {
        var _this = this;
        this.spinner.active = true;
        if (this.validate(this.f)) {
            this.authService.login(this.model).subscribe(function (res) {
                _this.router.navigate(['/']);
            }, function (err) {
                _this.authNoticeService.setNotice(_this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'error');
                _this.spinner.active = false;
                _this.cdr.detectChanges();
            });
        }
    };
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.ngOnDestroy = function () {
        this.authNoticeService.setNotice(null);
    };
    LoginComponent.prototype.validate = function (f) {
        if (f.form.status === 'VALID') {
            return true;
        }
        this.errors = [];
        if (objectPath.get(f, 'form.controls.email.errors.email')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.INVALID', { name: this.translate.instant('AUTH.INPUT.EMAIL') }));
        }
        if (objectPath.get(f, 'form.controls.email.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.EMAIL') }));
        }
        if (objectPath.get(f, 'form.controls.password.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.INVALID', { name: this.translate.instant('AUTH.INPUT.PASSWORD') }));
        }
        if (objectPath.get(f, 'form.controls.password.errors.minlength')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.MIN_LENGTH', { name: this.translate.instant('AUTH.INPUT.PASSWORD') }));
        }
        if (this.errors.length > 0) {
            this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
            this.spinner.active = false;
        }
        return false;
    };
    LoginComponent.prototype.forgotPasswordPage = function (event) {
        this.action = 'forgot-password';
        this.actionChange.next(this.action);
    };
    LoginComponent.prototype.register = function (event) {
        this.action = 'register';
        this.actionChange.next(this.action);
    };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", String)
    ], LoginComponent.prototype, "classes", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], LoginComponent.prototype, "actionChange", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], LoginComponent.prototype, "action", void 0);
    __decorate([
        core_1.ViewChild('f'),
        __metadata("design:type", forms_1.NgForm)
    ], LoginComponent.prototype, "f", void 0);
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'm-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.scss'],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
            router_1.Router,
            auth_notice_service_1.AuthNoticeService,
            core_2.TranslateService,
            core_1.ChangeDetectorRef])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map