"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var authentication_service_1 = require("../../../../core/auth/authentication.service");
var forms_1 = require("@angular/forms");
var objectPath = require("object-path");
var auth_notice_service_1 = require("../../../../core/auth/auth-notice.service");
var core_2 = require("@ngx-translate/core");
var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(authService, authNoticeService, translate) {
        this.authService = authService;
        this.authNoticeService = authNoticeService;
        this.translate = translate;
        this.model = { email: '' };
        this.actionChange = new rxjs_1.Subject();
        this.loading = false;
        this.errors = [];
        this.spinner = {
            active: false,
            spinnerSize: 18,
            raised: true,
            buttonColor: 'primary',
            spinnerColor: 'accent',
            fullWidth: false
        };
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () { };
    ForgotPasswordComponent.prototype.loginPage = function (event) {
        event.preventDefault();
        this.action = 'login';
        this.actionChange.next(this.action);
    };
    ForgotPasswordComponent.prototype.submit = function () {
        var _this = this;
        this.spinner.active = true;
        if (this.validate(this.f)) {
            this.authService.requestPassword(this.model).subscribe(function (response) {
                if (typeof response !== 'undefined') {
                    _this.action = 'login';
                    _this.actionChange.next(_this.action);
                }
                else {
                    // tslint:disable-next-line:max-line-length
                    _this.authNoticeService.setNotice(_this.translate.instant('AUTH.VALIDATION.NOT_FOUND', { name: _this.translate.instant('AUTH.INPUT.EMAIL') }), 'error');
                }
                _this.spinner.active = false;
            });
        }
    };
    ForgotPasswordComponent.prototype.validate = function (f) {
        if (f.form.status === 'VALID') {
            return true;
        }
        this.errors = [];
        if (objectPath.get(f, 'form.controls.email.errors.email')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.INVALID', { name: this.translate.instant('AUTH.INPUT.EMAIL') }));
        }
        if (objectPath.get(f, 'form.controls.email.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.EMAIL') }));
        }
        if (this.errors.length > 0) {
            this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
            this.spinner.active = false;
        }
        return false;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ForgotPasswordComponent.prototype, "action", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ForgotPasswordComponent.prototype, "actionChange", void 0);
    __decorate([
        core_1.ViewChild('f'),
        __metadata("design:type", forms_1.NgForm)
    ], ForgotPasswordComponent.prototype, "f", void 0);
    ForgotPasswordComponent = __decorate([
        core_1.Component({
            selector: 'm-forgot-password',
            templateUrl: './forgot-password.component.html',
            styleUrls: ['./forgot-password.component.scss']
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
            auth_notice_service_1.AuthNoticeService,
            core_2.TranslateService])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());
exports.ForgotPasswordComponent = ForgotPasswordComponent;
//# sourceMappingURL=forgot-password.component.js.map