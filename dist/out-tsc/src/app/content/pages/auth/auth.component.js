"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var layout_config_service_1 = require("../../../core/services/layout-config.service");
var layout_1 = require("../../../config/layout");
var auth_notice_service_1 = require("../../../core/auth/auth-notice.service");
var translation_service_1 = require("../../../core/services/translation.service");
var AuthComponent = /** @class */ (function () {
    function AuthComponent(layoutConfigService, authNoticeService, translationService) {
        this.layoutConfigService = layoutConfigService;
        this.authNoticeService = authNoticeService;
        this.translationService = translationService;
        this.id = 'm_login';
        // tslint:disable-next-line:max-line-length
        this.classses = 'm-grid m-grid--hor m-grid--root m-page';
        this.action = 'login';
        this.today = Date.now();
    }
    AuthComponent.prototype.ngOnInit = function () {
        var _this = this;
        // set login layout to blank
        this.layoutConfigService.setModel(new layout_1.LayoutConfig({ content: { skin: '' } }), true);
        this.translationService.getSelectedLanguage().subscribe(function (lang) {
            if (lang) {
                setTimeout(function () { return _this.translationService.setLanguage(lang); });
            }
        });
    };
    AuthComponent.prototype.ngOnDestroy = function () {
        // reset back to fluid
        this.layoutConfigService.reloadSavedConfig();
    };
    AuthComponent.prototype.register = function () {
        this.action = 'register';
    };
    __decorate([
        core_1.HostBinding('id'),
        __metadata("design:type", Object)
    ], AuthComponent.prototype, "id", void 0);
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], AuthComponent.prototype, "classses", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], AuthComponent.prototype, "action", void 0);
    AuthComponent = __decorate([
        core_1.Component({
            selector: 'm-auth',
            templateUrl: './auth.component.html',
            styleUrls: ['./auth.component.scss']
        }),
        __metadata("design:paramtypes", [layout_config_service_1.LayoutConfigService,
            auth_notice_service_1.AuthNoticeService,
            translation_service_1.TranslationService])
    ], AuthComponent);
    return AuthComponent;
}());
exports.AuthComponent = AuthComponent;
//# sourceMappingURL=auth.component.js.map