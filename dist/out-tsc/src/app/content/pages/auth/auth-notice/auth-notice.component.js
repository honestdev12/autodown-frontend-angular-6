"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_notice_service_1 = require("../../../../core/auth/auth-notice.service");
var AuthNoticeComponent = /** @class */ (function () {
    function AuthNoticeComponent(authNoticeService) {
        this.authNoticeService = authNoticeService;
        this.message = '';
    }
    AuthNoticeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authNoticeService.onNoticeChanged$.subscribe(function (notice) {
            if (notice) {
                _this.message = notice.message;
                _this.type = notice.type;
            }
        });
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], AuthNoticeComponent.prototype, "type", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], AuthNoticeComponent.prototype, "message", void 0);
    AuthNoticeComponent = __decorate([
        core_1.Component({
            selector: 'm-auth-notice',
            templateUrl: './auth-notice.component.html',
            styleUrls: ['./auth-notice.component.scss']
        }),
        __metadata("design:paramtypes", [auth_notice_service_1.AuthNoticeService])
    ], AuthNoticeComponent);
    return AuthNoticeComponent;
}());
exports.AuthNoticeComponent = AuthNoticeComponent;
//# sourceMappingURL=auth-notice.component.js.map