"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var auth_component_1 = require("./auth.component");
var login_component_1 = require("./login/login.component");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var register_component_1 = require("./register/register.component");
var forgot_password_component_1 = require("./forgot-password/forgot-password.component");
var material_1 = require("@angular/material");
var core_2 = require("@ngx-translate/core");
var spinner_button_module_1 = require("../../partials/content/general/spinner-button/spinner-button.module");
var auth_notice_component_1 = require("./auth-notice/auth-notice.component");
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                material_1.MatButtonModule,
                material_1.MatInputModule,
                material_1.MatFormFieldModule,
                material_1.MatCheckboxModule,
                core_2.TranslateModule.forChild(),
                spinner_button_module_1.SpinnerButtonModule,
                router_1.RouterModule.forChild([
                    {
                        path: '',
                        component: auth_component_1.AuthComponent
                    }
                ])
            ],
            providers: [],
            declarations: [
                auth_component_1.AuthComponent,
                login_component_1.LoginComponent,
                register_component_1.RegisterComponent,
                forgot_password_component_1.ForgotPasswordComponent,
                auth_notice_component_1.AuthNoticeComponent
            ]
        })
    ], AuthModule);
    return AuthModule;
}());
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map