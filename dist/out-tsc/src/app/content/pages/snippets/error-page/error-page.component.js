"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var ErrorPageComponent = /** @class */ (function () {
    function ErrorPageComponent(route) {
        this.route = route;
        this.classes = 'm-grid m-grid--hor m-grid--root m-page';
    }
    ErrorPageComponent.prototype.ngOnInit = function () {
        this.errorType = +this.route.snapshot.paramMap.get('type');
        if (!this.errorType) {
            this.errorType = Math.floor((Math.random() * 6) + 1);
        }
    };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", String)
    ], ErrorPageComponent.prototype, "classes", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], ErrorPageComponent.prototype, "errorType", void 0);
    ErrorPageComponent = __decorate([
        core_1.Component({
            selector: 'm-error-page',
            templateUrl: './error-page.component.html',
            styleUrls: ['./error-page.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute])
    ], ErrorPageComponent);
    return ErrorPageComponent;
}());
exports.ErrorPageComponent = ErrorPageComponent;
//# sourceMappingURL=error-page.component.js.map