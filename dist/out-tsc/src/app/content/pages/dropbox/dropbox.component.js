"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var collections_1 = require("@angular/cdk/collections");
var material_1 = require("@angular/material");
// RXJS
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
// Models
var query_params_model_1 = require("../../../core/models/query-params.model");
// Services
var dropbox_service_1 = require("../../../core/services/dropbox.service");
var current_user_service_1 = require("../../../core/services/current-user.service");
var dropbox_model_1 = require("../../../core/models/dropbox.model");
var material_2 = require("@angular/material");
var dropbox_dialog_component_1 = require("./dropbox-dialog.component");
var confirm_dialog_component_1 = require("../../../content/partials/content/general/confirm-dialog/confirm-dialog.component");
var DropboxComponent = /** @class */ (function () {
    function DropboxComponent(dropboxService, currentUser, dialog) {
        this.dropboxService = dropboxService;
        this.currentUser = currentUser;
        this.dialog = dialog;
        this.dropboxes = [];
        this.displayedColumns = ['name', 'token', 'actions'];
        this.message = null;
        this.messageType = 'success';
        this.new_dropbox = new dropbox_model_1.DropboxModel();
        this.selection = new collections_1.SelectionModel(true, []);
    }
    DropboxComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sort.sortChange.subscribe(function () { return (_this.paginator.pageIndex = 0); });
        rxjs_1.merge(this.sort.sortChange, this.paginator.page)
            .pipe(operators_1.tap(function () {
            _this.loadItems();
        }))
            .subscribe();
        // First load
        this.currentUser.userUpdated$.subscribe(function (user) {
            _this.new_dropbox['user'] = user.id;
        });
        this.loadItems(true);
    };
    DropboxComponent.prototype.loadItems = function (first_time) {
        var _this = this;
        if (first_time === void 0) { first_time = false; }
        var queryParams = new query_params_model_1.QueryParamsModel(this.sort.direction, this.sort.active, this.paginator.pageIndex, first_time ? 10 : this.paginator.pageSize);
        this.dropboxService.get(queryParams).subscribe(function (res) {
            _this.dropboxes = res.results;
            _this.new_dropbox = new dropbox_model_1.DropboxModel();
        });
        this.selection.clear();
    };
    DropboxComponent.prototype.delete = function (dropbox) {
        var _this = this;
        var dialogRef = this.dialog.open(confirm_dialog_component_1.ConfirmDialog, {
            width: '40vw',
            data: ''
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result)
                _this.dropboxService.delete(dropbox.id).subscribe(function (res) { return _this.loadItems(); });
        });
    };
    DropboxComponent.prototype.openDialog = function (dropbox) {
        var _this = this;
        if (dropbox === void 0) { dropbox = null; }
        var data = {};
        if (dropbox) {
            data = { 'dropbox': dropbox, new: false };
        }
        else
            data = { 'dropbox': this.new_dropbox, new: true };
        var dialogRef = this.dialog.open(dropbox_dialog_component_1.DropboxDialog, {
            width: '60vw',
            'data': data
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result)
                _this.loadItems();
        });
    };
    __decorate([
        core_1.ViewChild(material_1.MatPaginator),
        __metadata("design:type", material_1.MatPaginator)
    ], DropboxComponent.prototype, "paginator", void 0);
    __decorate([
        core_1.ViewChild(material_1.MatSort),
        __metadata("design:type", material_1.MatSort)
    ], DropboxComponent.prototype, "sort", void 0);
    DropboxComponent = __decorate([
        core_1.Component({
            selector: 'm-vendor',
            templateUrl: './dropbox.component.html'
        }),
        __metadata("design:paramtypes", [dropbox_service_1.DropboxService,
            current_user_service_1.CurrentUser,
            material_2.MatDialog])
    ], DropboxComponent);
    return DropboxComponent;
}());
exports.DropboxComponent = DropboxComponent;
//# sourceMappingURL=dropbox.component.js.map