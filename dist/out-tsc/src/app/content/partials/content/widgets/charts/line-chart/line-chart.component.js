"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var chart_data_service_1 = require("../../../../../../core/services/chart-data.service");
var chart_params_model_1 = require("../../../../../../core/models/chart-params.model");
var ng2_charts_1 = require("ng2-charts");
var LineChartComponent = /** @class */ (function () {
    function LineChartComponent(chartDataService) {
        this.chartDataService = chartDataService;
        this.lineChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true
        };
        this.lineChartLabels = ['2018-10-3', '2018-10-4', '2018-10-5', '2018-10-6', '2018-10-7', '2018-10-8', '2018-10-9'];
        this.lineChartType = 'line';
        this.lineChartLegend = true;
        this.lineChartData = [
            { data: [65, 59, 80, 81, 56, 55, 40], label: 'Spider 1' },
            { data: [28, 48, 40, 19, 86, 27, 90], label: 'Spider 2' }
        ];
        this.periods = ['week', 'month'];
        this.spiderId = -1;
        this.period = 'week';
    }
    LineChartComponent.prototype.ngOnInit = function () {
        this.loadItems();
    };
    LineChartComponent.prototype.loadItems = function () {
        var _this = this;
        var queryParams = new chart_params_model_1.ChartParamsModel(this.spiderId, this.period);
        this.chartDataService.get(queryParams).subscribe(function (res) {
            var _a, _b;
            _this.lineChartLabels.length = 0;
            (_a = _this.lineChartLabels).push.apply(_a, res.labels);
            _this.lineChartData.length = 0;
            (_b = _this.lineChartData).push.apply(_b, res.data);
            if (_this.chart !== undefined) {
                _this.chart.ngOnDestroy();
                _this.chart.chart = _this.chart.getChartBuilder(_this.chart.ctx);
            }
        });
    };
    LineChartComponent.prototype.chartClicked = function (e) {
    };
    LineChartComponent.prototype.chartHovered = function (e) {
    };
    LineChartComponent.prototype.changePeriod = function (e) {
        this.loadItems();
    };
    __decorate([
        core_1.Input('spider-id'),
        __metadata("design:type", Object)
    ], LineChartComponent.prototype, "spiderId", void 0);
    __decorate([
        core_1.ViewChild("baseChart"),
        __metadata("design:type", ng2_charts_1.BaseChartDirective)
    ], LineChartComponent.prototype, "chart", void 0);
    LineChartComponent = __decorate([
        core_1.Component({
            selector: 'm-line-chart',
            templateUrl: './line-chart.component.html',
            styleUrls: ['./line-chart.component.scss']
        }),
        __metadata("design:paramtypes", [chart_data_service_1.ChartDataService])
    ], LineChartComponent);
    return LineChartComponent;
}());
exports.LineChartComponent = LineChartComponent;
//# sourceMappingURL=line-chart.component.js.map