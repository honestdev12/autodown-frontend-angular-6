"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var query_results_model_1 = require("../../../../../../core/models/query-results.model");
// Why not use MatTableDataSource?
/*  In this example, we will not be using the built-in MatTableDataSource because its designed for filtering,
    sorting and pagination of a client - side data array.
    Read the article: 'https://blog.angular-university.io/angular-material-data-table/'
**/
var DataTableDataSource = /** @class */ (function () {
    function DataTableDataSource(dataTableService) {
        var _this = this;
        this.dataTableService = dataTableService;
        this.entitySubject = new rxjs_1.BehaviorSubject([]);
        this.hasItems = false; // Need to show message: 'No records found
        // Loading | Progress bar
        this.loadingSubject = new rxjs_1.BehaviorSubject(false);
        // Paginator | Paginators count
        this.paginatorTotalSubject = new rxjs_1.BehaviorSubject(0);
        this.loading$ = this.loadingSubject.asObservable();
        this.paginatorTotal$ = this.paginatorTotalSubject.asObservable();
        this.paginatorTotal$.subscribe(function (res) { return _this.hasItems = res > 0; });
    }
    DataTableDataSource.prototype.connect = function (collectionViewer) {
        // Connecting data source
        return this.entitySubject.asObservable();
    };
    DataTableDataSource.prototype.disconnect = function (collectionViewer) {
        // Disonnecting data source
        this.entitySubject.complete();
        this.loadingSubject.complete();
        this.paginatorTotalSubject.complete();
    };
    DataTableDataSource.prototype.baseFilter = function (_entities, _queryParams) {
        var entitiesResult = _entities;
        // Sorting
        // start
        if (_queryParams.sort) {
            entitiesResult = this.sortArray(_entities, _queryParams.sort);
        }
        // end
        // Paginator
        // start
        var totalCount = entitiesResult.length;
        var initialPos = _queryParams.page * _queryParams.page_size;
        entitiesResult = entitiesResult.slice(initialPos, initialPos + _queryParams.page_size);
        // end
        var queryResults = new query_results_model_1.QueryResultsModel();
        queryResults.items = entitiesResult;
        queryResults.totalCount = totalCount;
        return queryResults;
    };
    DataTableDataSource.prototype.loadItems = function (queryParams) {
        var _this = this;
        this.loadingSubject.next(true);
        this.dataTableService.getAllItems().pipe(operators_1.tap(function (res) {
            var result = _this.baseFilter(res, queryParams);
            _this.entitySubject.next(result.items);
            _this.paginatorTotalSubject.next(result.totalCount);
        }), operators_1.catchError(function (err) { return rxjs_1.of(new query_results_model_1.QueryResultsModel([], err)); }), operators_1.finalize(function () { return _this.loadingSubject.next(false); })).subscribe();
    };
    DataTableDataSource.prototype.sortArray = function (_incomingArray, _sortField) {
        if (_sortField === void 0) { _sortField = ''; }
        var _sortOrder = _sortField.indexOf('-') === 0 ? 'desc' : 'asc';
        var sort = _sortField.indexOf('-') === 0 ? _sortField.slice(1) : 'desc';
        if (!sort) {
            return _incomingArray;
        }
        var result = [];
        result = _incomingArray.sort(function (a, b) {
            if (a[sort] < b[sort]) {
                return _sortOrder === 'asc' ? -1 : 1;
            }
            if (a[sort] > b[sort]) {
                return _sortOrder === 'asc' ? 1 : -1;
            }
            return 0;
        });
        return result;
    };
    return DataTableDataSource;
}());
exports.DataTableDataSource = DataTableDataSource;
//# sourceMappingURL=data-table.data-source.js.map