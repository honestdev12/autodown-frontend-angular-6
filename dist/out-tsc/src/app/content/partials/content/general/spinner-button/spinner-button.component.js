"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SpinnerButtonComponent = /** @class */ (function () {
    function SpinnerButtonComponent() {
    }
    SpinnerButtonComponent.prototype.ngOnInit = function () { };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], SpinnerButtonComponent.prototype, "options", void 0);
    SpinnerButtonComponent = __decorate([
        core_1.Component({
            selector: 'm-spinner-button',
            templateUrl: './spinner-button.component.html',
            styleUrls: ['./spinner-button.component.scss'],
        }),
        __metadata("design:paramtypes", [])
    ], SpinnerButtonComponent);
    return SpinnerButtonComponent;
}());
exports.SpinnerButtonComponent = SpinnerButtonComponent;
//# sourceMappingURL=spinner-button.component.js.map