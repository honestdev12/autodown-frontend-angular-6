"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@ngx-loading-bar/core");
var rxjs_1 = require("rxjs");
var objectPath = require("object-path");
var PortletComponent = /** @class */ (function () {
    function PortletComponent(loader) {
        this.loader = loader;
        this.loader.complete();
    }
    PortletComponent.prototype.ngAfterViewInit = function () {
        // hide portlet footer if no content
        if (this.elFooter && this.elFooter.nativeElement.children.length == 0) {
            this.elFooter.nativeElement.style.display = 'none';
            this.elPortlet.nativeElement.classList.add('m-portlet--full-height');
        }
        // add portlet responsive mobile for sticky portlet
        if (objectPath.get(this.options, 'enableSticky')) {
            this.elPortlet.nativeElement.classList.add('m-portlet--responsive-mobile');
        }
        // hide portlet header tools if no content
        if (this.elHeadTools && this.elHeadTools.nativeElement.children.length == 0) {
            this.elHeadTools.nativeElement.style.display = 'none';
        }
    };
    PortletComponent.prototype.ngOnInit = function () { };
    __decorate([
        core_1.Input(),
        __metadata("design:type", rxjs_1.Observable)
    ], PortletComponent.prototype, "loading$", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], PortletComponent.prototype, "options", void 0);
    __decorate([
        core_1.ViewChild('mPortlet'),
        __metadata("design:type", core_1.ElementRef)
    ], PortletComponent.prototype, "elPortlet", void 0);
    __decorate([
        core_1.ViewChild('mPortletHead'),
        __metadata("design:type", core_1.ElementRef)
    ], PortletComponent.prototype, "elHead", void 0);
    __decorate([
        core_1.ViewChild('mPortletBody'),
        __metadata("design:type", core_1.ElementRef)
    ], PortletComponent.prototype, "elBody", void 0);
    __decorate([
        core_1.ViewChild('mPortletFooter'),
        __metadata("design:type", core_1.ElementRef)
    ], PortletComponent.prototype, "elFooter", void 0);
    __decorate([
        core_1.ViewChild('mPortletHeadTools'),
        __metadata("design:type", core_1.ElementRef)
    ], PortletComponent.prototype, "elHeadTools", void 0);
    PortletComponent = __decorate([
        core_1.Component({
            selector: 'm-portlet',
            templateUrl: './portlet.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [core_2.LoadingBarService])
    ], PortletComponent);
    return PortletComponent;
}());
exports.PortletComponent = PortletComponent;
//# sourceMappingURL=portlet.component.js.map