"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var messenger_component_1 = require("./messenger.component");
var messenger_in_component_1 = require("./messenger-in/messenger-in.component");
var messenger_out_component_1 = require("./messenger-out/messenger-out.component");
var ngx_perfect_scrollbar_1 = require("ngx-perfect-scrollbar");
var core_module_1 = require("../../../../../core/core.module");
var MessengerModule = /** @class */ (function () {
    function MessengerModule() {
    }
    MessengerModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, core_module_1.CoreModule, ngx_perfect_scrollbar_1.PerfectScrollbarModule],
            declarations: [
                messenger_component_1.MessengerComponent,
                messenger_in_component_1.MessengerInComponent,
                messenger_out_component_1.MessengerOutComponent
            ],
            exports: [messenger_component_1.MessengerComponent, messenger_in_component_1.MessengerInComponent, messenger_out_component_1.MessengerOutComponent]
        })
    ], MessengerModule);
    return MessengerModule;
}());
exports.MessengerModule = MessengerModule;
//# sourceMappingURL=messenger.module.js.map