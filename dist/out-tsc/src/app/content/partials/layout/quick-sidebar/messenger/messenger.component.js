"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var messenger_service_1 = require("../../../../../core/services/messenger.service");
var rxjs_1 = require("rxjs");
var MessengerComponent = /** @class */ (function () {
    function MessengerComponent(messengerService) {
        this.messengerService = messengerService;
    }
    MessengerComponent.prototype.ngOnInit = function () {
        this.messages = this.messengerService.getData();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", rxjs_1.Observable)
    ], MessengerComponent.prototype, "messages", void 0);
    MessengerComponent = __decorate([
        core_1.Component({
            selector: 'm-messenger',
            templateUrl: './messenger.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [messenger_service_1.MessengerService])
    ], MessengerComponent);
    return MessengerComponent;
}());
exports.MessengerComponent = MessengerComponent;
//# sourceMappingURL=messenger.component.js.map