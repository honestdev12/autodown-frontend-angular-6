"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var list_timeline_component_1 = require("./list-timeline.component");
var timeline_item_component_1 = require("./timeline-item/timeline-item.component");
var core_module_1 = require("../../../../../core/core.module");
var ListTimelineModule = /** @class */ (function () {
    function ListTimelineModule() {
    }
    ListTimelineModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, core_module_1.CoreModule],
            declarations: [list_timeline_component_1.ListTimelineComponent, timeline_item_component_1.TimelineItemComponent],
            exports: [list_timeline_component_1.ListTimelineComponent, timeline_item_component_1.TimelineItemComponent]
        })
    ], ListTimelineModule);
    return ListTimelineModule;
}());
exports.ListTimelineModule = ListTimelineModule;
//# sourceMappingURL=list-timeline.module.js.map