"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TimelineItemComponent = /** @class */ (function () {
    function TimelineItemComponent() {
        this.classes = 'm-list-timeline__item';
    }
    TimelineItemComponent.prototype.ngOnInit = function () {
        if (this.item.read) {
            this.classes += ' m-list-timeline__item--read';
        }
    };
    TimelineItemComponent.prototype.badgeClass = function () {
        var badges = {
            urgent: 'm-badge--info',
            important: 'm-badge--warning',
            resolved: 'm-badge--success',
            pending: 'm-badge--danger'
        };
        if (this.item.tags.length > 0) {
            return badges[this.item.tags[0]];
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], TimelineItemComponent.prototype, "item", void 0);
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], TimelineItemComponent.prototype, "classes", void 0);
    TimelineItemComponent = __decorate([
        core_1.Component({
            selector: 'm-timeline-item',
            templateUrl: './timeline-item.component.html',
            styleUrls: ['./timeline-item.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], TimelineItemComponent);
    return TimelineItemComponent;
}());
exports.TimelineItemComponent = TimelineItemComponent;
//# sourceMappingURL=timeline-item.component.js.map