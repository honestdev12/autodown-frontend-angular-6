"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var logs_service_1 = require("../../../../../core/services/logs.service");
var rxjs_1 = require("rxjs");
var ListTimelineComponent = /** @class */ (function () {
    function ListTimelineComponent(logsService) {
        this.logsService = logsService;
    }
    ListTimelineComponent.prototype.ngOnInit = function () {
        this.logList = this.logsService.getData({ types: this.type });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ListTimelineComponent.prototype, "type", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ListTimelineComponent.prototype, "heading", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", rxjs_1.Observable)
    ], ListTimelineComponent.prototype, "logList", void 0);
    ListTimelineComponent = __decorate([
        core_1.Component({
            selector: 'm-list-timeline',
            templateUrl: './list-timeline.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [logs_service_1.LogsService])
    ], ListTimelineComponent);
    return ListTimelineComponent;
}());
exports.ListTimelineComponent = ListTimelineComponent;
//# sourceMappingURL=list-timeline.component.js.map