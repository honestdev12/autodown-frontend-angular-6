"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var quick_sidebar_component_1 = require("./layout/quick-sidebar/quick-sidebar.component");
var scroll_top_component_1 = require("./layout/scroll-top/scroll-top.component");
var tooltips_component_1 = require("./layout/tooltips/tooltips.component");
var list_settings_component_1 = require("./layout/quick-sidebar/list-settings/list-settings.component");
var messenger_module_1 = require("./layout/quick-sidebar/messenger/messenger.module");
var core_module_1 = require("../../core/core.module");
var list_timeline_module_1 = require("./layout/quick-sidebar/list-timeline/list-timeline.module");
var ngx_perfect_scrollbar_1 = require("ngx-perfect-scrollbar");
var notice_component_1 = require("./content/general/notice/notice.component");
var portlet_module_1 = require("./content/general/portlet/portlet.module");
var spinner_button_module_1 = require("./content/general/spinner-button/spinner-button.module");
var blog_component_1 = require("./content/widgets/general/blog/blog.component");
var finance_stats_component_1 = require("./content/widgets/general/finance-stats/finance-stats.component");
var packages_component_1 = require("./content/widgets/general/packages/packages.component");
var tasks_component_1 = require("./content/widgets/general/tasks/tasks.component");
var support_tickets_component_1 = require("./content/widgets/general/support-tickets/support-tickets.component");
var recent_activities_component_1 = require("./content/widgets/general/recent-activities/recent-activities.component");
var recent_notifications_component_1 = require("./content/widgets/general/recent-notifications/recent-notifications.component");
var audit_log_component_1 = require("./content/widgets/general/audit-log/audit-log.component");
var best_seller_component_1 = require("./content/widgets/general/best-seller/best-seller.component");
var author_profit_component_1 = require("./content/widgets/general/author-profit/author-profit.component");
var data_table_component_1 = require("./content/widgets/general/data-table/data-table.component");
var widget_charts_module_1 = require("./content/widgets/charts/widget-charts.module");
var stat_component_1 = require("./content/widgets/general/stat/stat.component");
var confirm_dialog_component_1 = require("./content/general/confirm-dialog/confirm-dialog.component");
var info_dialog_component_1 = require("./content/general/info-dialog/info-dialog.component");
var ngx_perfect_scrollbar_2 = require("ngx-perfect-scrollbar");
var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
// suppressScrollX: true
};
var MY_FORMATS = {
    parse: {
        dateInput: 'YYYY-MM-DD',
    },
    display: {
        dateInput: 'YYYY-MM-DD',
        monthYearLabel: 'YYYY MM',
        dateA11yLabel: 'YYYY-MM-DD',
        monthYearA11yLabel: 'YYYY MM',
    },
};
var material_1 = require("@angular/material");
var PartialsModule = /** @class */ (function () {
    function PartialsModule() {
    }
    PartialsModule = __decorate([
        core_1.NgModule({
            declarations: [
                quick_sidebar_component_1.QuickSidebarComponent,
                scroll_top_component_1.ScrollTopComponent,
                tooltips_component_1.TooltipsComponent,
                list_settings_component_1.ListSettingsComponent,
                notice_component_1.NoticeComponent,
                blog_component_1.BlogComponent,
                finance_stats_component_1.FinanceStatsComponent,
                packages_component_1.PackagesComponent,
                tasks_component_1.TasksComponent,
                support_tickets_component_1.SupportTicketsComponent,
                recent_activities_component_1.RecentActivitiesComponent,
                recent_notifications_component_1.RecentNotificationsComponent,
                audit_log_component_1.AuditLogComponent,
                best_seller_component_1.BestSellerComponent,
                author_profit_component_1.AuthorProfitComponent,
                data_table_component_1.DataTableComponent,
                stat_component_1.StatComponent,
                confirm_dialog_component_1.ConfirmDialog,
                info_dialog_component_1.InfoDialog
            ],
            exports: [
                quick_sidebar_component_1.QuickSidebarComponent,
                scroll_top_component_1.ScrollTopComponent,
                tooltips_component_1.TooltipsComponent,
                list_settings_component_1.ListSettingsComponent,
                notice_component_1.NoticeComponent,
                blog_component_1.BlogComponent,
                finance_stats_component_1.FinanceStatsComponent,
                packages_component_1.PackagesComponent,
                tasks_component_1.TasksComponent,
                support_tickets_component_1.SupportTicketsComponent,
                recent_activities_component_1.RecentActivitiesComponent,
                recent_notifications_component_1.RecentNotificationsComponent,
                audit_log_component_1.AuditLogComponent,
                best_seller_component_1.BestSellerComponent,
                author_profit_component_1.AuthorProfitComponent,
                data_table_component_1.DataTableComponent,
                stat_component_1.StatComponent,
                confirm_dialog_component_1.ConfirmDialog,
                info_dialog_component_1.InfoDialog,
                portlet_module_1.PortletModule,
                spinner_button_module_1.SpinnerButtonModule
            ],
            imports: [
                common_1.CommonModule,
                router_1.RouterModule,
                ng_bootstrap_1.NgbModule,
                ngx_perfect_scrollbar_1.PerfectScrollbarModule,
                messenger_module_1.MessengerModule,
                list_timeline_module_1.ListTimelineModule,
                core_module_1.CoreModule,
                portlet_module_1.PortletModule,
                spinner_button_module_1.SpinnerButtonModule,
                material_1.MatButtonModule,
                material_1.MatFormFieldModule,
                material_1.MatInputModule,
                material_1.MatCheckboxModule,
                material_1.MatDialogModule,
                material_1.MatIconModule,
                material_1.MatSortModule,
                material_1.MatProgressSpinnerModule,
                material_1.MatTableModule,
                material_1.MatPaginatorModule,
                material_1.MatSelectModule,
                material_1.MatProgressBarModule,
                material_1.MatTooltipModule,
                material_1.MatDatepickerModule,
                material_1.MatExpansionModule,
                widget_charts_module_1.WidgetChartsModule
            ],
            providers: [
                {
                    provide: ngx_perfect_scrollbar_2.PERFECT_SCROLLBAR_CONFIG,
                    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
                }
            ]
        })
    ], PartialsModule);
    return PartialsModule;
}());
exports.PartialsModule = PartialsModule;
//# sourceMappingURL=partials.module.js.map