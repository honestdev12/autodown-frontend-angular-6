"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var list_timeline_module_1 = require("../partials/layout/quick-sidebar/list-timeline/list-timeline.module");
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var header_component_1 = require("./header/header.component");
var aside_left_component_1 = require("./aside/aside-left.component");
var footer_component_1 = require("./footer/footer.component");
var subheader_component_1 = require("./subheader/subheader.component");
var brand_component_1 = require("./header/brand/brand.component");
var menu_section_component_1 = require("./aside/menu-section/menu-section.component");
var topbar_component_1 = require("./header/topbar/topbar.component");
var core_module_1 = require("../../core/core.module");
var user_profile_component_1 = require("./header/topbar/user-profile/user-profile.component");
var search_dropdown_component_1 = require("./header/topbar/search-dropdown/search-dropdown.component");
var notification_component_1 = require("./header/topbar/notification/notification.component");
var quick_action_component_1 = require("./header/topbar/quick-action/quick-action.component");
var menu_horizontal_component_1 = require("./header/menu-horizontal/menu-horizontal.component");
var aside_right_component_1 = require("./aside/aside-right/aside-right.component");
var search_default_component_1 = require("./header/topbar/search-default/search-default.component");
var header_search_component_1 = require("./header/header-search/header-search.component");
var language_selector_component_1 = require("./header/topbar/language-selector/language-selector.component");
var ngx_perfect_scrollbar_1 = require("ngx-perfect-scrollbar");
var ngx_perfect_scrollbar_2 = require("ngx-perfect-scrollbar");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var material_1 = require("@angular/material");
var core_2 = require("@ngx-translate/core");
var core_3 = require("@ngx-loading-bar/core");
var forms_1 = require("@angular/forms");
var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
// suppressScrollX: true
};
var LayoutModule = /** @class */ (function () {
    function LayoutModule() {
    }
    LayoutModule = __decorate([
        core_1.NgModule({
            declarations: [
                header_component_1.HeaderComponent,
                footer_component_1.FooterComponent,
                subheader_component_1.SubheaderComponent,
                brand_component_1.BrandComponent,
                // topbar components
                topbar_component_1.TopbarComponent,
                user_profile_component_1.UserProfileComponent,
                search_dropdown_component_1.SearchDropdownComponent,
                notification_component_1.NotificationComponent,
                quick_action_component_1.QuickActionComponent,
                language_selector_component_1.LanguageSelectorComponent,
                // aside left menu components
                aside_left_component_1.AsideLeftComponent,
                menu_section_component_1.MenuSectionComponent,
                // horizontal menu components
                menu_horizontal_component_1.MenuHorizontalComponent,
                // aside right component
                aside_right_component_1.AsideRightComponent,
                search_default_component_1.SearchDefaultComponent,
                header_search_component_1.HeaderSearchComponent,
            ],
            exports: [
                header_component_1.HeaderComponent,
                footer_component_1.FooterComponent,
                subheader_component_1.SubheaderComponent,
                brand_component_1.BrandComponent,
                // topbar components
                topbar_component_1.TopbarComponent,
                user_profile_component_1.UserProfileComponent,
                search_dropdown_component_1.SearchDropdownComponent,
                notification_component_1.NotificationComponent,
                quick_action_component_1.QuickActionComponent,
                language_selector_component_1.LanguageSelectorComponent,
                // aside left menu components
                aside_left_component_1.AsideLeftComponent,
                // MenuSectionComponent,
                // horizontal menu components
                menu_horizontal_component_1.MenuHorizontalComponent,
                // aside right component
                aside_right_component_1.AsideRightComponent
            ],
            providers: [
                {
                    provide: ngx_perfect_scrollbar_2.PERFECT_SCROLLBAR_CONFIG,
                    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
                }
            ],
            imports: [
                common_1.CommonModule,
                router_1.RouterModule,
                core_module_1.CoreModule,
                ngx_perfect_scrollbar_1.PerfectScrollbarModule,
                ng_bootstrap_1.NgbModule,
                forms_1.FormsModule,
                list_timeline_module_1.ListTimelineModule,
                material_1.MatProgressBarModule,
                material_1.MatTabsModule,
                material_1.MatButtonModule,
                material_1.MatTooltipModule,
                core_2.TranslateModule.forChild(),
                core_3.LoadingBarModule.forRoot(),
            ]
        })
    ], LayoutModule);
    return LayoutModule;
}());
exports.LayoutModule = LayoutModule;
//# sourceMappingURL=layout.module.js.map