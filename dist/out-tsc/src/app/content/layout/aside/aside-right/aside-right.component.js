"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AsideRightComponent = /** @class */ (function () {
    function AsideRightComponent() {
        this.classes = 'm-grid__item m-aside-right';
    }
    AsideRightComponent.prototype.ngOnInit = function () { };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], AsideRightComponent.prototype, "classes", void 0);
    AsideRightComponent = __decorate([
        core_1.Component({
            selector: 'm-aside-right',
            templateUrl: './aside-right.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [])
    ], AsideRightComponent);
    return AsideRightComponent;
}());
exports.AsideRightComponent = AsideRightComponent;
//# sourceMappingURL=aside-right.component.js.map