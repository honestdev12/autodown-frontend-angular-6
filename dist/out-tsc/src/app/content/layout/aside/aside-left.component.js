"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var operators_1 = require("rxjs/operators");
var router_1 = require("@angular/router");
var menu_aside_offcanvas_directive_1 = require("../../../core/directives/menu-aside-offcanvas.directive");
var class_init_service_1 = require("../../../core/services/class-init.service");
var layout_config_service_1 = require("../../../core/services/layout-config.service");
var layout_ref_service_1 = require("../../../core/services/layout/layout-ref.service");
var menu_aside_service_1 = require("../../../core/services/layout/menu-aside.service");
var common_1 = require("@angular/common");
var AsideLeftComponent = /** @class */ (function () {
    function AsideLeftComponent(el, classInitService, menuAsideService, layoutConfigService, router, layoutRefService, document) {
        var _this = this;
        this.el = el;
        this.classInitService = classInitService;
        this.menuAsideService = menuAsideService;
        this.layoutConfigService = layoutConfigService;
        this.router = router;
        this.layoutRefService = layoutRefService;
        this.document = document;
        this.classes = 'm-grid__item m-aside-left';
        this.id = 'm_aside_left';
        this.currentRouteUrl = '';
        // subscribe to menu classes update
        this.classInitService.onClassesUpdated$.subscribe(function (classes) {
            // join the classes array and pass to variable
            _this.classes = 'm-grid__item m-aside-left ' + classes.aside_left.join(' ');
        });
    }
    AsideLeftComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.mMenuAsideOffcanvas = new menu_aside_offcanvas_directive_1.MenuAsideOffcanvasDirective(_this.el);
            // manually call the directives' lifecycle hook method
            _this.mMenuAsideOffcanvas.ngAfterViewInit();
            // keep aside left element reference
            _this.layoutRefService.addElement('asideLeft', _this.el.nativeElement);
        });
    };
    AsideLeftComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentRouteUrl = this.router.url.split(/[?#]/)[0];
        this.router.events
            .pipe(operators_1.filter(function (event) { return event instanceof router_1.NavigationEnd; }))
            .subscribe(function (event) { return _this.currentRouteUrl = _this.router.url.split(/[?#]/)[0]; });
    };
    AsideLeftComponent.prototype.isMenuItemIsActive = function (item) {
        if (item.submenu) {
            return this.isMenuRootItemIsActive(item);
        }
        if (!item.page) {
            return false;
        }
        // dashboard
        if (item.page !== '/' && this.currentRouteUrl.startsWith(item.page)) {
            return true;
        }
        return this.currentRouteUrl === item.page;
    };
    AsideLeftComponent.prototype.isMenuRootItemIsActive = function (item) {
        var result = false;
        for (var _i = 0, _a = item.submenu; _i < _a.length; _i++) {
            var subItem = _a[_i];
            result = this.isMenuItemIsActive(subItem);
            if (result) {
                return true;
            }
        }
        return false;
    };
    /**
     * Use for fixed left aside menu, to show menu on mouseenter event.
     * @param e Event
     */
    AsideLeftComponent.prototype.mouseEnter = function (e) {
        var _this = this;
        // check if the left aside menu is fixed
        if (this.document.body.classList.contains('m-aside-left--fixed')) {
            if (this.outsideTm) {
                clearTimeout(this.outsideTm);
                this.outsideTm = null;
            }
            this.insideTm = setTimeout(function () {
                // if the left aside menu is minimized
                if (_this.document.body.classList.contains('m-aside-left--minimize') && mUtil.isInResponsiveRange('desktop')) {
                    // show the left aside menu
                    _this.document.body.classList.remove('m-aside-left--minimize');
                    _this.document.body.classList.add('m-aside-left--minimize-hover');
                }
            }, 300);
        }
    };
    /**
     * Use for fixed left aside menu, to show menu on mouseenter event.
     * @param e Event
     */
    AsideLeftComponent.prototype.mouseLeave = function (e) {
        var _this = this;
        if (this.document.body.classList.contains('m-aside-left--fixed')) {
            if (this.insideTm) {
                clearTimeout(this.insideTm);
                this.insideTm = null;
            }
            this.outsideTm = setTimeout(function () {
                // if the left aside menu is expand
                if (_this.document.body.classList.contains('m-aside-left--minimize-hover') && mUtil.isInResponsiveRange('desktop')) {
                    // hide back the left aside menu
                    _this.document.body.classList.remove('m-aside-left--minimize-hover');
                    _this.document.body.classList.add('m-aside-left--minimize');
                }
            }, 500);
        }
    };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], AsideLeftComponent.prototype, "classes", void 0);
    __decorate([
        core_1.HostBinding('id'),
        __metadata("design:type", Object)
    ], AsideLeftComponent.prototype, "id", void 0);
    __decorate([
        core_1.HostBinding('attr.mMenuAsideOffcanvas'),
        __metadata("design:type", menu_aside_offcanvas_directive_1.MenuAsideOffcanvasDirective)
    ], AsideLeftComponent.prototype, "mMenuAsideOffcanvas", void 0);
    AsideLeftComponent = __decorate([
        core_1.Component({
            selector: 'm-aside-left',
            templateUrl: './aside-left.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __param(6, core_1.Inject(common_1.DOCUMENT)),
        __metadata("design:paramtypes", [core_1.ElementRef,
            class_init_service_1.ClassInitService,
            menu_aside_service_1.MenuAsideService,
            layout_config_service_1.LayoutConfigService,
            router_1.Router,
            layout_ref_service_1.LayoutRefService,
            Document])
    ], AsideLeftComponent);
    return AsideLeftComponent;
}());
exports.AsideLeftComponent = AsideLeftComponent;
//# sourceMappingURL=aside-left.component.js.map