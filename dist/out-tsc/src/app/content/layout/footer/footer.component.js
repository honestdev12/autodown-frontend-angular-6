"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var layout_config_service_1 = require("../../../core/services/layout-config.service");
var objectPath = require("object-path");
var FooterComponent = /** @class */ (function () {
    function FooterComponent(configService) {
        var _this = this;
        this.configService = configService;
        this.classes = 'm-grid__item m-footer';
        this.footerContainerClass$ = new rxjs_1.BehaviorSubject('');
        this.configService.onLayoutConfigUpdated$.subscribe(function (model) {
            var config = model.config;
            var pageBodyClass = '';
            var selfLayout = objectPath.get(config, 'self.layout');
            if (selfLayout === 'boxed' || selfLayout === 'wide') {
                pageBodyClass += 'm-container--responsive m-container--xxl';
            }
            else {
                pageBodyClass += 'm-container--fluid';
            }
            _this.footerContainerClass$.next(pageBodyClass);
        });
    }
    FooterComponent.prototype.ngOnInit = function () { };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], FooterComponent.prototype, "classes", void 0);
    FooterComponent = __decorate([
        core_1.Component({
            selector: 'm-footer',
            templateUrl: './footer.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [layout_config_service_1.LayoutConfigService])
    ], FooterComponent);
    return FooterComponent;
}());
exports.FooterComponent = FooterComponent;
//# sourceMappingURL=footer.component.js.map