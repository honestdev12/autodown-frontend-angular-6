"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var header_service_1 = require("../../../core/services/layout/header.service");
var current_user_service_1 = require("../../../core/services/current-user.service");
var router_1 = require("@angular/router");
var layout_ref_service_1 = require("../../../core/services/layout/layout-ref.service");
var core_2 = require("@ngx-loading-bar/core");
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router, layoutRefService, headerService, loader, currentUser) {
        var _this = this;
        this.router = router;
        this.layoutRefService = layoutRefService;
        this.headerService = headerService;
        this.loader = loader;
        this.currentUser = currentUser;
        this.showSearch = false;
        // page progress bar percentage
        this.router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                // set page progress bar loading to start on NavigationStart event router
                _this.loader.start();
            }
            if (event instanceof router_1.RouteConfigLoadStart) {
                _this.loader.increment(35);
            }
            if (event instanceof router_1.RouteConfigLoadEnd) {
                _this.loader.increment(75);
            }
            if (event instanceof router_1.NavigationEnd || event instanceof router_1.NavigationCancel) {
                // set page progress bar loading to end on NavigationEnd event router
                _this.loader.complete();
            }
        });
        this.currentUser.userUpdated$.subscribe(function (user) { return _this.user = user; });
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.showSearch = window.innerWidth >= 1025;
    };
    HeaderComponent.prototype.ngAfterViewInit = function () {
        // keep header element in the service
        this.layoutRefService.addElement('header', this.mHeader.nativeElement);
    };
    __decorate([
        core_1.ViewChild('mHeader'),
        __metadata("design:type", core_1.ElementRef)
    ], HeaderComponent.prototype, "mHeader", void 0);
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'm-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.scss'],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [router_1.Router,
            layout_ref_service_1.LayoutRefService,
            header_service_1.HeaderService,
            core_2.LoadingBarService,
            current_user_service_1.CurrentUser])
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;
//# sourceMappingURL=header.component.js.map