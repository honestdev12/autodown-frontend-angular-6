"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_model_1 = require("../../../../core/models/user.model");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var TopbarComponent = /** @class */ (function () {
    function TopbarComponent(router) {
        this.router = router;
        this.id = 'm_header_nav';
        this.classes = 'm-stack__item m-stack__item--right m-header-head';
        this.showSearch = false;
    }
    TopbarComponent.prototype.ngOnInit = function () {
        this.showSearch = window.innerWidth < 1025;
    };
    TopbarComponent.prototype.ngAfterViewInit = function () { };
    __decorate([
        core_1.HostBinding('id'),
        __metadata("design:type", Object)
    ], TopbarComponent.prototype, "id", void 0);
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], TopbarComponent.prototype, "classes", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], TopbarComponent.prototype, "searchType", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", user_model_1.UserModel)
    ], TopbarComponent.prototype, "user", void 0);
    TopbarComponent = __decorate([
        core_1.Component({
            selector: 'm-topbar',
            templateUrl: './topbar.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], TopbarComponent);
    return TopbarComponent;
}());
exports.TopbarComponent = TopbarComponent;
//# sourceMappingURL=topbar.component.js.map