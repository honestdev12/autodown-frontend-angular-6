"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var NotificationComponent = /** @class */ (function () {
    function NotificationComponent() {
        var _this = this;
        // tslint:disable-next-line:max-line-length
        this.classes = 'm-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width';
        this.attrDropdownToggle = 'click';
        this.attrDropdownPersisten = 'true';
        // animate icon shake and dot blink
        setInterval(function () {
            _this.animateShake = 'm-animate-shake';
            _this.animateBlink = 'm-animate-blink';
        }, 3000);
        setInterval(function () { return (_this.animateShake = _this.animateBlink = ''); }, 6000);
    }
    NotificationComponent.prototype.ngOnInit = function () { };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], NotificationComponent.prototype, "classes", void 0);
    __decorate([
        core_1.HostBinding('attr.m-dropdown-toggle'),
        __metadata("design:type", Object)
    ], NotificationComponent.prototype, "attrDropdownToggle", void 0);
    __decorate([
        core_1.HostBinding('attr.m-dropdown-persistent'),
        __metadata("design:type", Object)
    ], NotificationComponent.prototype, "attrDropdownPersisten", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], NotificationComponent.prototype, "animateShake", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], NotificationComponent.prototype, "animateBlink", void 0);
    NotificationComponent = __decorate([
        core_1.Component({
            selector: 'm-notification',
            templateUrl: './notification.component.html',
            styleUrls: ['./notification.component.scss'],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [])
    ], NotificationComponent);
    return NotificationComponent;
}());
exports.NotificationComponent = NotificationComponent;
//# sourceMappingURL=notification.component.js.map