"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var operators_1 = require("rxjs/operators");
var core_1 = require("@angular/core");
var translation_service_1 = require("../../../../../core/services/translation.service");
var router_1 = require("@angular/router");
var LanguageSelectorComponent = /** @class */ (function () {
    function LanguageSelectorComponent(translationService, router, el) {
        this.translationService = translationService;
        this.router = router;
        this.el = el;
        // tslint:disable-next-line:max-line-length
        this.classes = 'm-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--arrow m-dropdown--align-right m-dropdown--header-bg-fill m-dropdown--mobile-full-width';
        this.mDropdownToggle = 'click';
        this.languages = [
            {
                lang: 'en',
                country: 'USA',
                flag: 'assets/app/media/img/flags/020-flag.svg'
            },
            {
                lang: 'ch',
                country: 'China',
                flag: 'assets/app/media/img/flags/015-china.svg'
            },
            {
                lang: 'es',
                country: 'Spain',
                flag: 'assets/app/media/img/flags/016-spain.svg'
            },
            {
                lang: 'jp',
                country: 'Japan',
                flag: 'assets/app/media/img/flags/014-japan.svg'
            },
            {
                lang: 'de',
                country: 'Germany',
                flag: 'assets/app/media/img/flags/017-germany.svg'
            },
            {
                lang: 'fr',
                country: 'France',
                flag: 'assets/app/media/img/flags/019-france.svg'
            },
        ];
    }
    LanguageSelectorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setSelectedLanguage();
        this.router.events
            .pipe(operators_1.filter(function (event) { return event instanceof router_1.NavigationStart; }))
            .subscribe(function (event) {
            _this.setSelectedLanguage();
        });
    };
    LanguageSelectorComponent.prototype.setLanguage = function (lang) {
        var _this = this;
        this.languages.forEach(function (language) {
            if (language.lang === lang) {
                language.active = true;
                _this.language = language;
            }
            else {
                language.active = false;
            }
        });
        this.translationService.setLanguage(lang);
        this.el.nativeElement.classList.remove('m-dropdown--open');
    };
    LanguageSelectorComponent.prototype.setSelectedLanguage = function () {
        var _this = this;
        this.translationService.getSelectedLanguage().subscribe(function (lang) {
            if (lang) {
                setTimeout(function () {
                    _this.setLanguage(lang);
                });
            }
        });
    };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], LanguageSelectorComponent.prototype, "classes", void 0);
    __decorate([
        core_1.HostBinding('attr.m-dropdown-toggle'),
        __metadata("design:type", Object)
    ], LanguageSelectorComponent.prototype, "mDropdownToggle", void 0);
    LanguageSelectorComponent = __decorate([
        core_1.Component({
            selector: 'm-language-selector',
            templateUrl: './language-selector.component.html',
            styleUrls: ['./language-selector.component.scss']
        }),
        __metadata("design:paramtypes", [translation_service_1.TranslationService,
            router_1.Router,
            core_1.ElementRef])
    ], LanguageSelectorComponent);
    return LanguageSelectorComponent;
}());
exports.LanguageSelectorComponent = LanguageSelectorComponent;
//# sourceMappingURL=language-selector.component.js.map