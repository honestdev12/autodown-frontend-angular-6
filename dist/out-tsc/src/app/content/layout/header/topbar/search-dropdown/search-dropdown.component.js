"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var layout_config_service_1 = require("../../../../../core/services/layout-config.service");
var objectPath = require("object-path");
var quick_search_directive_1 = require("../../../../../core/directives/quick-search.directive");
var quick_search_service_1 = require("../../../../../core/services/quick-search.service");
var SearchDropdownComponent = /** @class */ (function () {
    function SearchDropdownComponent(layoutConfigService, el, quickSearchService) {
        var _this = this;
        this.layoutConfigService = layoutConfigService;
        this.el = el;
        this.quickSearchService = quickSearchService;
        this.classes = '';
        this.id = 'm_quicksearch';
        this.attrDropdownToggle = 'click';
        this.attrDropdownPersistent = '1';
        this.attrQuicksearchMode = 'dropdown';
        this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (model) {
            var config = model.config;
            _this.classes =
                // tslint:disable-next-line:max-line-length
                'm-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light m-list-search m-list-search--skin-light';
            _this.classes +=
                ' m-dropdown--skin-' +
                    objectPath.get(config, 'header.search.dropdown.skin');
        });
    }
    SearchDropdownComponent.prototype.ngOnInit = function () { };
    SearchDropdownComponent.prototype.ngOnDestroy = function () {
        this.onSearch.unsubscribe();
    };
    SearchDropdownComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        Promise.resolve(null).then(function () {
            _this.mQuickSearchDirective = new quick_search_directive_1.QuickSearchDirective(_this.el);
            _this.mQuickSearchDirective.ngAfterViewInit();
            // listen to search event
            _this.onSearch = _this.mQuickSearchDirective.onSearch$.subscribe(function (mQuickSearch) {
                mQuickSearch.showProgress();
                _this.quickSearchService.search().subscribe(function (result) {
                    // append search result
                    mQuickSearch.showResult(result[0]);
                    mQuickSearch.hideProgress();
                });
            });
        });
    };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], SearchDropdownComponent.prototype, "classes", void 0);
    __decorate([
        core_1.HostBinding('id'),
        __metadata("design:type", Object)
    ], SearchDropdownComponent.prototype, "id", void 0);
    __decorate([
        core_1.HostBinding('attr.m-dropdown-toggle'),
        __metadata("design:type", Object)
    ], SearchDropdownComponent.prototype, "attrDropdownToggle", void 0);
    __decorate([
        core_1.HostBinding('attr.m-dropdown-persistent'),
        __metadata("design:type", Object)
    ], SearchDropdownComponent.prototype, "attrDropdownPersistent", void 0);
    __decorate([
        core_1.HostBinding('attr.m-quicksearch-mode'),
        __metadata("design:type", Object)
    ], SearchDropdownComponent.prototype, "attrQuicksearchMode", void 0);
    __decorate([
        core_1.HostBinding('attr.mQuickSearch'),
        __metadata("design:type", quick_search_directive_1.QuickSearchDirective)
    ], SearchDropdownComponent.prototype, "mQuickSearchDirective", void 0);
    SearchDropdownComponent = __decorate([
        core_1.Component({
            selector: 'm-search-dropdown',
            templateUrl: './search-dropdown.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [layout_config_service_1.LayoutConfigService,
            core_1.ElementRef,
            quick_search_service_1.QuickSearchService])
    ], SearchDropdownComponent);
    return SearchDropdownComponent;
}());
exports.SearchDropdownComponent = SearchDropdownComponent;
//# sourceMappingURL=search-dropdown.component.js.map