"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var authentication_service_1 = require("../../../../../core/auth/authentication.service");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var platform_browser_1 = require("@angular/platform-browser");
var user_model_1 = require("../../../../../core/models/user.model");
var UserProfileComponent = /** @class */ (function () {
    function UserProfileComponent(router, authService, sanitizer) {
        this.router = router;
        this.authService = authService;
        this.sanitizer = sanitizer;
        // tslint:disable-next-line:max-line-length
        this.classes = 'm-nav__item m-topbar__user-profile m-topbar__user-profile--img m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light';
        this.attrDropdownToggle = 'click';
    }
    UserProfileComponent.prototype.ngOnInit = function () {
    };
    UserProfileComponent.prototype.logout = function () {
        this.authService.logout(true);
    };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], UserProfileComponent.prototype, "classes", void 0);
    __decorate([
        core_1.HostBinding('attr.m-dropdown-toggle'),
        __metadata("design:type", Object)
    ], UserProfileComponent.prototype, "attrDropdownToggle", void 0);
    __decorate([
        core_1.ViewChild('mProfileDropdown'),
        __metadata("design:type", core_1.ElementRef)
    ], UserProfileComponent.prototype, "mProfileDropdown", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", user_model_1.UserModel)
    ], UserProfileComponent.prototype, "user", void 0);
    UserProfileComponent = __decorate([
        core_1.Component({
            selector: 'm-user-profile',
            templateUrl: './user-profile.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [router_1.Router,
            authentication_service_1.AuthenticationService,
            platform_browser_1.DomSanitizer])
    ], UserProfileComponent);
    return UserProfileComponent;
}());
exports.UserProfileComponent = UserProfileComponent;
//# sourceMappingURL=user-profile.component.js.map