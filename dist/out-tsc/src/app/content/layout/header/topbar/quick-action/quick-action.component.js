"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var QuickActionComponent = /** @class */ (function () {
    function QuickActionComponent() {
        // tslint:disable-next-line:max-line-length
        this.classes = 'm-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light';
        this.attrDropdownToggle = 'click';
    }
    QuickActionComponent.prototype.ngOnInit = function () { };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], QuickActionComponent.prototype, "classes", void 0);
    __decorate([
        core_1.HostBinding('attr.m-dropdown-toggle'),
        __metadata("design:type", Object)
    ], QuickActionComponent.prototype, "attrDropdownToggle", void 0);
    QuickActionComponent = __decorate([
        core_1.Component({
            selector: 'm-quick-action',
            templateUrl: './quick-action.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [])
    ], QuickActionComponent);
    return QuickActionComponent;
}());
exports.QuickActionComponent = QuickActionComponent;
//# sourceMappingURL=quick-action.component.js.map