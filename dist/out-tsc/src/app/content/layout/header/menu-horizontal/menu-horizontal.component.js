"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var operators_1 = require("rxjs/operators");
var objectPath = require("object-path");
var menu_horizontal_offcanvas_directive_1 = require("../../../../core/directives/menu-horizontal-offcanvas.directive");
var menu_horizontal_directive_1 = require("../../../../core/directives/menu-horizontal.directive");
var menu_horizontal_service_1 = require("../../../../core/services/layout/menu-horizontal.service");
var menu_config_service_1 = require("../../../../core/services/menu-config.service");
var class_init_service_1 = require("../../../../core/services/class-init.service");
var MenuHorizontalComponent = /** @class */ (function () {
    function MenuHorizontalComponent(el, classInitService, menuHorService, menuConfigService, router) {
        this.el = el;
        this.classInitService = classInitService;
        this.menuHorService = menuHorService;
        this.menuConfigService = menuConfigService;
        this.router = router;
        this.classes = '';
        this.id = 'm_header_menu';
        this.currentRouteUrl = '';
        this.itemsWithAsides = [];
        this.classes = this.menuHorService.menuClasses;
    }
    MenuHorizontalComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        Promise.resolve(null).then(function () {
            _this.mMenuHorOffcanvas = new menu_horizontal_offcanvas_directive_1.MenuHorizontalOffcanvasDirective(_this.el);
            _this.mMenuHorOffcanvas.ngAfterViewInit();
            _this.mMenuHorizontal = new menu_horizontal_directive_1.MenuHorizontalDirective(_this.el);
            _this.mMenuHorizontal.ngAfterViewInit();
        });
    };
    MenuHorizontalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentRouteUrl = this.router.url;
        this.menuHorService.menuList$.subscribe(function (menuItems) { return _this.fillAsides(menuItems); });
        this.shouldOverrideAsides();
        this.router.events
            .pipe(operators_1.filter(function (event) { return event instanceof router_1.NavigationEnd; }))
            .subscribe(function (event) {
            _this.currentRouteUrl = _this.router.url;
            _this.shouldOverrideAsides();
        });
    };
    MenuHorizontalComponent.prototype.shouldOverrideAsides = function () {
        var aside = this.getActiveItemAside();
        if (aside) {
            // override aside menu as secondary menu of current header menu
            this.menuConfigService.configModel.config.aside = aside;
            this.menuConfigService.setModel(this.menuConfigService.configModel);
        }
    };
    MenuHorizontalComponent.prototype.fillAsides = function (menuItems) {
        for (var _i = 0, menuItems_1 = menuItems; _i < menuItems_1.length; _i++) {
            var menuItem = menuItems_1[_i];
            if (menuItem.aside) {
                this.itemsWithAsides.push(menuItem);
            }
            if (menuItem.submenu && menuItem.submenu.items) {
                this.fillAsides(menuItem.submenu.items);
            }
        }
    };
    MenuHorizontalComponent.prototype.getActiveItemAside = function () {
        if (this.currentRouteUrl === '') {
            return null;
        }
        for (var _i = 0, _a = this.itemsWithAsides; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.page && item.page === this.currentRouteUrl) {
                return item.aside;
            }
        }
    };
    MenuHorizontalComponent.prototype.getItemCssClasses = function (item) {
        var cssClasses = 'm-menu__item';
        if (objectPath.get(item, 'submenu')) {
            cssClasses += ' m-menu__item--submenu';
        }
        if (objectPath.get(item, 'resizer')) {
            cssClasses += ' m-menu__item--resize';
        }
        if ((objectPath.get(item, 'root') &&
            objectPath.get(item, 'submenu.type') === 'classic') ||
            parseInt(objectPath.get(item, 'submenu.width'), 2) > 0) {
            cssClasses += ' m-menu__item--rel';
        }
        var customClass = objectPath.get(item, 'custom-class');
        if (customClass) {
            cssClasses += ' ' + customClass;
        }
        if (objectPath.get(item, 'icon-only')) {
            cssClasses += ' m-menu__item--icon-only';
        }
        if (item.submenu && this.isMenuItemIsActive(item)) {
            cssClasses += ' m-menu__item--active';
        }
        return cssClasses;
    };
    MenuHorizontalComponent.prototype.getItemAttrLinkRedirect = function (menuItem) {
        if (objectPath.get(menuItem, 'redirect')) {
            return '1';
        }
        return null;
    };
    MenuHorizontalComponent.prototype.getItemAttrResizeDesktopBreakpoint = function (menuItem) {
        if (objectPath.get(menuItem, 'resizer')) {
            return objectPath.get(menuItem, 'resize-breakpoint');
        }
        return null;
    };
    MenuHorizontalComponent.prototype.getItemAttrSubmenuToggle = function (menuItem) {
        var attrSubmenuToggle = 'hover';
        if (objectPath.get(menuItem, 'toggle') === 'click') {
            attrSubmenuToggle = 'click';
        }
        else if (objectPath.get(menuItem, 'submenu.type') === 'tabs') {
            attrSubmenuToggle = 'tabs';
        }
        else {
            // submenu toggle default to 'hover'
        }
        return attrSubmenuToggle;
    };
    MenuHorizontalComponent.prototype.getItemAttrSubmenuMode = function (menuItem) {
        return null;
    };
    MenuHorizontalComponent.prototype.getItemMenuSubmenuClass = function (menuItem) {
        var subClass = '';
        var subAlignment = objectPath.get(menuItem, 'submenu.alignment');
        if (subAlignment) {
            subClass += ' m-menu__submenu--' + subAlignment;
        }
        if (objectPath.get(menuItem, 'submenu.type') === 'classic') {
            subClass += ' m-menu__submenu--classic';
        }
        if (objectPath.get(menuItem, 'submenu.type') === 'tabs') {
            subClass += ' m-menu__submenu--tabs';
        }
        if (objectPath.get(menuItem, 'submenu.type') === 'mega') {
            if (objectPath.get(menuItem, 'submenu.width')) {
                subClass += ' m-menu__submenu--fixed';
            }
        }
        if (objectPath.get(menuItem, 'submenu.pull')) {
            subClass += ' m-menu__submenu--pull';
        }
        return subClass;
    };
    MenuHorizontalComponent.prototype.isMenuItemIsActive = function (item) {
        if (item.submenu) {
            return this.isMenuRootItemIsActive(item);
        }
        if (!item.page) {
            return false;
        }
        return item.page === this.currentRouteUrl;
    };
    MenuHorizontalComponent.prototype.isMenuRootItemIsActive = function (item) {
        if (item.submenu.items) {
            for (var _i = 0, _a = item.submenu.items; _i < _a.length; _i++) {
                var subItem = _a[_i];
                if (this.isMenuItemIsActive(subItem)) {
                    return true;
                }
            }
        }
        return false;
    };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], MenuHorizontalComponent.prototype, "classes", void 0);
    __decorate([
        core_1.HostBinding('id'),
        __metadata("design:type", Object)
    ], MenuHorizontalComponent.prototype, "id", void 0);
    __decorate([
        core_1.HostBinding('attr.mMenuHorizontalOffcanvas'),
        __metadata("design:type", menu_horizontal_offcanvas_directive_1.MenuHorizontalOffcanvasDirective)
    ], MenuHorizontalComponent.prototype, "mMenuHorOffcanvas", void 0);
    __decorate([
        core_1.HostBinding('attr.mMenuHorizontal'),
        __metadata("design:type", menu_horizontal_directive_1.MenuHorizontalDirective)
    ], MenuHorizontalComponent.prototype, "mMenuHorizontal", void 0);
    MenuHorizontalComponent = __decorate([
        core_1.Component({
            selector: 'm-menu-horizontal',
            templateUrl: './menu-horizontal.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [core_1.ElementRef,
            class_init_service_1.ClassInitService,
            menu_horizontal_service_1.MenuHorizontalService,
            menu_config_service_1.MenuConfigService,
            router_1.Router])
    ], MenuHorizontalComponent);
    return MenuHorizontalComponent;
}());
exports.MenuHorizontalComponent = MenuHorizontalComponent;
//# sourceMappingURL=menu-horizontal.component.js.map