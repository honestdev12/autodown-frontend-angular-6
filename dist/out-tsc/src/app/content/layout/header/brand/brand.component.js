"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var class_init_service_1 = require("../../../../core/services/class-init.service");
var layout_config_service_1 = require("../../../../core/services/layout-config.service");
var objectPath = require("object-path");
var common_1 = require("@angular/common");
var user_model_1 = require("../../../../core/models/user.model");
var router_1 = require("@angular/router");
var BrandComponent = /** @class */ (function () {
    function BrandComponent(classInitService, layoutConfigService, document, router) {
        var _this = this;
        this.classInitService = classInitService;
        this.layoutConfigService = layoutConfigService;
        this.document = document;
        this.router = router;
        this.classes = 'm-stack__item m-brand m-stack__item--left';
        this.menuAsideLeftSkin = '';
        this.menuAsideMinimizeDefault = false;
        this.menuAsideMinimizToggle = false;
        this.menuAsideDisplay = false;
        this.menuHeaderDisplay = true;
        this.headerLogo = '';
        this.brandText = '';
        this.brandText = router.url.indexOf('admin') >= 0 ? 'Admin' : 'Client';
        // subscribe to class update event
        this.classInitService.onClassesUpdated$.subscribe(function (classes) {
            _this.classes = 'm-stack__item m-brand m-stack__item--left ' + classes.brand.join(' ');
        });
        this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (model) {
            _this.menuAsideLeftSkin = objectPath.get(model, 'config.aside.left.skin');
            _this.menuAsideMinimizeDefault = objectPath.get(model, 'config.aside.left.minimize.default');
            _this.menuAsideMinimizToggle = objectPath.get(model, 'config.aside.left.minimize.toggle');
            _this.menuAsideDisplay = objectPath.get(model, 'config.menu.aside.display');
            _this.menuHeaderDisplay = objectPath.get(model, 'config.menu.header.display');
            var headerLogo = objectPath.get(model, 'config.header.self.logo');
            if (typeof headerLogo === 'object') {
                _this.headerLogo = objectPath.get(headerLogo, _this.menuAsideLeftSkin);
            }
            else {
                _this.headerLogo = headerLogo;
            }
        });
        this.router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                if (event.url.indexOf('admin') >= 0)
                    _this.brandText = 'Admin';
                else
                    _this.brandText = 'Client';
            }
        });
    }
    BrandComponent.prototype.ngOnInit = function () { };
    /**
     * Toggle class topbar show/hide
     * @param event
     */
    BrandComponent.prototype.clickTopbarToggle = function (event) {
        this.document.body.classList.toggle('m-topbar--on');
    };
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], BrandComponent.prototype, "classes", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], BrandComponent.prototype, "menuAsideLeftSkin", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], BrandComponent.prototype, "menuAsideMinimizeDefault", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], BrandComponent.prototype, "menuAsideMinimizToggle", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], BrandComponent.prototype, "menuAsideDisplay", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], BrandComponent.prototype, "menuHeaderDisplay", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], BrandComponent.prototype, "headerLogo", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", user_model_1.UserModel)
    ], BrandComponent.prototype, "user", void 0);
    BrandComponent = __decorate([
        core_1.Component({
            selector: 'm-brand',
            templateUrl: './brand.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __param(2, core_1.Inject(common_1.DOCUMENT)),
        __metadata("design:paramtypes", [class_init_service_1.ClassInitService,
            layout_config_service_1.LayoutConfigService,
            Document,
            router_1.Router])
    ], BrandComponent);
    return BrandComponent;
}());
exports.BrandComponent = BrandComponent;
//# sourceMappingURL=brand.component.js.map