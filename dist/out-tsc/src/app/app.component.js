"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var layout_config_service_1 = require("./core/services/layout-config.service");
var class_init_service_1 = require("./core/services/class-init.service");
var translation_service_1 = require("./core/services/translation.service");
var objectPath = require("object-path");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var page_config_service_1 = require("./core/services/page-config.service");
var operators_1 = require("rxjs/operators");
var splash_screen_service_1 = require("./core/services/splash-screen.service");
var acl_service_1 = require("./core/services/acl.service");
// language list
var en_1 = require("./config/i18n/en");
var ch_1 = require("./config/i18n/ch");
var es_1 = require("./config/i18n/es");
var jp_1 = require("./config/i18n/jp");
var de_1 = require("./config/i18n/de");
var fr_1 = require("./config/i18n/fr");
// LIST KNOWN ISSUES
// [Violation] Added non-passive event listener; https://github.com/angular/angular/issues/8866
var AppComponent = /** @class */ (function () {
    function AppComponent(layoutConfigService, classInitService, sanitizer, translationService, router, pageConfigService, splashScreenService, aclService) {
        var _this = this;
        this.layoutConfigService = layoutConfigService;
        this.classInitService = classInitService;
        this.sanitizer = sanitizer;
        this.translationService = translationService;
        this.router = router;
        this.pageConfigService = pageConfigService;
        this.splashScreenService = splashScreenService;
        this.aclService = aclService;
        this.title = 'Metronic';
        this.classes = '';
        // subscribe to class update event
        this.classInitService.onClassesUpdated$.subscribe(function (classes) {
            // get body class array, join as string classes and pass to host binding class
            setTimeout(function () { return _this.classes = classes.body.join(' '); });
        });
        this.layoutConfigService.onLayoutConfigUpdated$.subscribe(function (model) {
            _this.classInitService.setConfig(model);
            _this.style = '';
            if (objectPath.get(model.config, 'self.layout') === 'boxed') {
                var backgroundImage = objectPath.get(model.config, 'self.background');
                if (backgroundImage) {
                    _this.style = _this.sanitizer.bypassSecurityTrustStyle('background-image: url(' + objectPath.get(model.config, 'self.background') + ')');
                }
            }
            // splash screen image
            _this.splashScreenImage = objectPath.get(model.config, 'loader.image');
        });
        // register translations
        this.translationService.loadTranslations(en_1.locale, ch_1.locale, es_1.locale, jp_1.locale, de_1.locale, fr_1.locale);
        // override config by router change from pages config
        this.router.events
            .pipe(operators_1.filter(function (event) { return event instanceof router_1.NavigationEnd; }))
            .subscribe(function (event) {
            _this.layoutConfigService.setModel({ page: objectPath.get(_this.pageConfigService.getCurrentPageConfig(), 'config') }, true);
        });
    }
    AppComponent.prototype.ngOnInit = function () { };
    AppComponent.prototype.ngAfterViewInit = function () {
        if (this.splashScreen) {
            this.splashScreenService.init(this.splashScreen.nativeElement);
        }
    };
    __decorate([
        core_1.HostBinding('style'),
        __metadata("design:type", Object)
    ], AppComponent.prototype, "style", void 0);
    __decorate([
        core_1.HostBinding('class'),
        __metadata("design:type", Object)
    ], AppComponent.prototype, "classes", void 0);
    __decorate([
        core_1.ViewChild('splashScreen', { read: core_1.ElementRef }),
        __metadata("design:type", core_1.ElementRef)
    ], AppComponent.prototype, "splashScreen", void 0);
    AppComponent = __decorate([
        core_1.Component({
            // tslint:disable-next-line:component-selector
            selector: 'body[m-root]',
            templateUrl: './app.component.html',
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [layout_config_service_1.LayoutConfigService,
            class_init_service_1.ClassInitService,
            platform_browser_1.DomSanitizer,
            translation_service_1.TranslationService,
            router_1.Router,
            page_config_service_1.PageConfigService,
            splash_screen_service_1.SplashScreenService,
            acl_service_1.AclService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map