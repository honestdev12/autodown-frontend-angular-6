"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var core_2 = require("@ngx-translate/core");
var http_1 = require("@angular/common/http");
require("hammerjs");
var environment_1 = require("../environments/environment");
var app_component_1 = require("./app.component");
var app_routing_module_1 = require("./app-routing.module");
var authentication_module_1 = require("./core/auth/authentication.module");
var ngx_permissions_1 = require("ngx-permissions");
var angular_in_memory_web_api_1 = require("angular-in-memory-web-api");
var fake_api_service_1 = require("./fake-api/fake-api.service");
var layout_module_1 = require("./content/layout/layout.module");
var partials_module_1 = require("./content/partials/partials.module");
var core_module_1 = require("./core/core.module");
var acl_service_1 = require("./core/services/acl.service");
var layout_config_service_1 = require("./core/services/layout-config.service");
var menu_config_service_1 = require("./core/services/menu-config.service");
var page_config_service_1 = require("./core/services/page-config.service");
var user_service_1 = require("./core/services/user.service");
var utils_service_1 = require("./core/services/utils.service");
var class_init_service_1 = require("./core/services/class-init.service");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var animations_1 = require("@angular/platform-browser/animations");
var platform_browser_2 = require("@angular/platform-browser");
var material_1 = require("@angular/material");
var overlay_1 = require("@angular/cdk/overlay");
var messenger_service_1 = require("./core/services/messenger.service");
var clipboard_sevice_1 = require("./core/services/clipboard.sevice");
var ngx_perfect_scrollbar_1 = require("ngx-perfect-scrollbar");
var layout_config_storage_service_1 = require("./core/services/layout-config-storage.service");
var logs_service_1 = require("./core/services/logs.service");
var quick_search_service_1 = require("./core/services/quick-search.service");
var subheader_service_1 = require("./core/services/layout/subheader.service");
var header_service_1 = require("./core/services/layout/header.service");
var menu_horizontal_service_1 = require("./core/services/layout/menu-horizontal.service");
var menu_aside_service_1 = require("./core/services/layout/menu-aside.service");
var layout_ref_service_1 = require("./core/services/layout/layout-ref.service");
var splash_screen_service_1 = require("./core/services/splash-screen.service");
var datatable_service_1 = require("./core/services/datatable.service");
var paypal_service_1 = require("./core/services/paypal.service");
// vendor 
var http_utils_service_1 = require("./core/services/http-utils.service");
var spider_job_service_1 = require("./core/services/spider-job.service");
var dropbox_service_1 = require("./core/services/dropbox.service");
var schedule_service_1 = require("./core/services/schedule.service");
var spider_log_service_1 = require("./core/services/spider-log.service");
var chart_data_service_1 = require("./core/services/chart-data.service");
var invoice_service_1 = require("./core/services/invoice.service");
var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
// suppressScrollX: true
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [app_component_1.AppComponent],
            imports: [
                animations_1.BrowserAnimationsModule,
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                http_1.HttpClientModule,
                environment_1.environment.isMockEnabled ? angular_in_memory_web_api_1.HttpClientInMemoryWebApiModule.forRoot(fake_api_service_1.FakeApiService) : [],
                layout_module_1.LayoutModule,
                partials_module_1.PartialsModule,
                core_module_1.CoreModule,
                overlay_1.OverlayModule,
                authentication_module_1.AuthenticationModule,
                ngx_permissions_1.NgxPermissionsModule.forRoot(),
                ng_bootstrap_1.NgbModule.forRoot(),
                core_2.TranslateModule.forRoot(),
                material_1.MatProgressSpinnerModule,
            ],
            providers: [
                acl_service_1.AclService,
                layout_config_service_1.LayoutConfigService,
                layout_config_storage_service_1.LayoutConfigStorageService,
                layout_ref_service_1.LayoutRefService,
                menu_config_service_1.MenuConfigService,
                page_config_service_1.PageConfigService,
                user_service_1.UserService,
                utils_service_1.UtilsService,
                class_init_service_1.ClassInitService,
                messenger_service_1.MessengerService,
                clipboard_sevice_1.ClipboardService,
                logs_service_1.LogsService,
                quick_search_service_1.QuickSearchService,
                datatable_service_1.DataTableService,
                splash_screen_service_1.SplashScreenService,
                {
                    provide: ngx_perfect_scrollbar_1.PERFECT_SCROLLBAR_CONFIG,
                    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
                },
                // vendor services
                http_utils_service_1.HttpUtilsService,
                spider_job_service_1.SpiderJobService,
                dropbox_service_1.DropboxService,
                schedule_service_1.ScheduleService,
                spider_log_service_1.SpiderLogService,
                chart_data_service_1.ChartDataService,
                invoice_service_1.InvoiceService,
                paypal_service_1.PayPalService,
                // template services
                subheader_service_1.SubheaderService,
                header_service_1.HeaderService,
                menu_horizontal_service_1.MenuHorizontalService,
                menu_aside_service_1.MenuAsideService,
                {
                    provide: platform_browser_2.HAMMER_GESTURE_CONFIG,
                    useClass: material_1.GestureConfig
                }
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map