"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AuthFakeDb = /** @class */ (function () {
    function AuthFakeDb() {
    }
    AuthFakeDb.users = [
        {
            id: 1,
            username: 'admin',
            password: 'demo',
            email: 'admin@demo.com',
            accessToken: 'access-token-' + Math.random(),
            refreshToken: 'access-token-' + Math.random(),
            roles: ['ADMIN'],
            pic: './assets/app/media/img/users/images.png',
            fullname: 'Mark Andre'
        },
        {
            id: 2,
            username: 'user',
            password: 'demo',
            email: 'user@gmail.com',
            accessToken: 'access-token-' + Math.random(),
            refreshToken: 'access-token-' + Math.random(),
            roles: ['USER'],
            pic: './assets/app/media/img/users/images.png',
            fullname: 'Megan'
        }
    ];
    AuthFakeDb.tokens = [
        {
            id: 1,
            accessToken: 'access-token-' + Math.random(),
            refreshToken: 'access-token-' + Math.random()
        }
    ];
    return AuthFakeDb;
}());
exports.AuthFakeDb = AuthFakeDb;
//# sourceMappingURL=auth.js.map