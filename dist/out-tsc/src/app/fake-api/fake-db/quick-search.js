"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var QuickSearchDb = /** @class */ (function () {
    function QuickSearchDb() {
    }
    QuickSearchDb.quickSearchHtml = [
        "<div class=\"m-list-search__results\">\n\t<span class=\"m-list-search__result-message m--hide\">\n\t\tNo record found\n\t</span>\n\n\t<span class=\"m-list-search__result-category m-list-search__result-category--first\">\n\t\tDocuments\n\t</span>\n\t<a href=\"#\" class=\"m-list-search__result-item\">\n\t\t<span class=\"m-list-search__result-item-icon\">\n\t\t\t<i class=\"flaticon-interface-3 m--font-warning\"></i>\n\t\t</span>\n\t\t<span class=\"m-list-search__result-item-text\">Annual finance report</span>\n\t</a>\n\t<a href=\"#\" class=\"m-list-search__result-item\">\n\t\t<span class=\"m-list-search__result-item-icon\">\n\t\t\t<i class=\"flaticon-share m--font-success\"></i>\n\t\t</span>\n\t\t<span class=\"m-list-search__result-item-text\">Company meeting schedule</span>\n\t</a>\n\t<a href=\"#\" class=\"m-list-search__result-item\">\n\t\t<span class=\"m-list-search__result-item-icon\">\n\t\t\t<i class=\"flaticon-paper-plane m--font-info\"></i>\n\t\t</span>\n\t\t<span class=\"m-list-search__result-item-text\">Project quotations</span>\n\t</a>\n\n\t<span class=\"m-list-search__result-category\">\n\t\tCustomers\n\t</span>\n\t<a href=\"#\" class=\"m-list-search__result-item\">\n\t\t<span class=\"m-list-search__result-item-pic\">\n\t\t\t<img class=\"m--img-rounded\" src=\"assets/app/media/img/users/user1.jpg\" title=\"\" />\n\t\t</span>\n\t\t<span class=\"m-list-search__result-item-text\">Amanda Anderson</span>\n\t</a>\n\t<a href=\"#\" class=\"m-list-search__result-item\">\n\t\t<span class=\"m-list-search__result-item-pic\">\n\t\t\t<img class=\"m--img-rounded\" src=\"assets/app/media/img/users/user2.jpg\" title=\"\" />\n\t\t</span>\n\t\t<span class=\"m-list-search__result-item-text\">Kennedy Lloyd</span>\n\t</a>\n\t<a href=\"#\" class=\"m-list-search__result-item\">\n\t\t<span class=\"m-list-search__result-item-pic\">\n\t\t\t<img class=\"m--img-rounded\" src=\"assets/app/media/img/users/user3.jpg\" title=\"\" />\n\t\t</span>\n\t\t<span class=\"m-list-search__result-item-text\">Megan Weldon</span>\n\t</a>\n\t<a href=\"#\" class=\"m-list-search__result-item\">\n\t\t<span class=\"m-list-search__result-item-pic\">\n\t\t\t<img class=\"m--img-rounded\" src=\"assets/app/media/img/users/images.png\" title=\"\" />\n\t\t</span>\n\t\t<span class=\"m-list-search__result-item-text\">Marc-Andr\u00E9 ter Stegen</span>\n\t</a>\n\n\t<span class=\"m-list-search__result-category\">\n\t\tFiles\n\t</span>\n\t<a href=\"#\" class=\"m-list-search__result-item\">\n\t\t<span class=\"m-list-search__result-item-icon\">\n\t\t\t<i class=\"flaticon-lifebuoy m--font-warning\"></i>\n\t\t</span>\n\t\t<span class=\"m-list-search__result-item-text\">Revenue report</span>\n\t</a>\n\t<a href=\"#\" class=\"m-list-search__result-item\">\n\t\t<span class=\"m-list-search__result-item-icon\">\n\t\t\t<i class=\"flaticon-coins m--font-primary\"></i>\n\t\t</span>\n\t\t<span class=\"m-list-search__result-item-text\">Anual finance report</span>\n\t</a>\n\t<a href=\"#\" class=\"m-list-search__result-item\">\n\t\t<span class=\"m-list-search__result-item-icon\">\n\t\t\t<i class=\"flaticon-calendar m--font-danger\"></i>\n\t\t</span>\n\t\t<span class=\"m-list-search__result-item-text\">Tax calculations</span>\n\t</a>\n</div>\n"
    ];
    QuickSearchDb.searchResult = [
        // documents
        {
            text: 'Annual finance report',
            category: 'documents',
            icon: '<i class="flaticon-interface-3 m--font-warning"></i>'
        },
        {
            text: 'Company meeting schedule',
            category: 'documents',
            icon: '<i class="flaticon-share m--font-success"></i>'
        },
        {
            text: 'Annual finance report',
            category: 'documents',
            icon: '<i class="flaticon-paper-plane m--font-info"></i>'
        },
        // customers
        {
            text: 'Amanda Anderson',
            category: 'customers',
            img: '<img class="m--img-rounded" src="./assets/app/media/img/users/user1.jpg" title=""/>'
        },
        {
            text: 'Kennedy Lloyd',
            category: 'customers',
            img: '<img class="m--img-rounded" src="./assets/app/media/img/users/user2.jpg" title=""/>'
        },
        {
            text: 'Megan Weldon',
            category: 'customers',
            img: '<img class="m--img-rounded" src="./assets/app/media/img/users/user13.jpg" title=""/>'
        },
        {
            text: 'Marc-André ter Stegen',
            category: 'customers',
            img: '<img class="m--img-rounded" src="./assets/app/media/img/users/images.png" title=""/>'
        },
        // files
        {
            text: 'Revenue report',
            category: 'files',
            icon: '<i class="flaticon-lifebuoy m--font-warning"></i>'
        },
        {
            text: 'Anual finance report',
            category: 'files',
            icon: '<i class="flaticon-coins m--font-primary"></i>'
        },
        {
            text: 'Tax calculations',
            category: 'files',
            icon: '<i class="flaticon-calendar m--font-danger"></i>'
        }
    ];
    return QuickSearchDb;
}());
exports.QuickSearchDb = QuickSearchDb;
//# sourceMappingURL=quick-search.js.map