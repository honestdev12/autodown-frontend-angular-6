"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var quick_search_1 = require("./fake-db/quick-search");
var core_1 = require("@angular/core");
var auth_1 = require("./fake-db/auth");
var messenger_1 = require("./fake-db/messenger");
var logs_1 = require("./fake-db/logs");
var cars_1 = require("./fake-db/cars");
// import { ECommerceDataContext } from './fake-db/e-commerce-db/_e-commerce.data-context';
var FakeApiService = /** @class */ (function () {
    function FakeApiService() {
    }
    FakeApiService.prototype.createDb = function () {
        return {
            // login and account
            login: auth_1.AuthFakeDb.users,
            refresh: auth_1.AuthFakeDb.tokens,
            register: auth_1.AuthFakeDb.users,
            // messenger
            messenger: messenger_1.MessengerDb.messages,
            // logs
            logs: logs_1.LogsDb.logs,
            quick_search: quick_search_1.QuickSearchDb.quickSearchHtml,
            // data-table
            cars: cars_1.CarsDb.cars
        };
    };
    FakeApiService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], FakeApiService);
    return FakeApiService;
}());
exports.FakeApiService = FakeApiService;
//# sourceMappingURL=fake-api.service.js.map