// tslint:disable-next-line:no-shadowed-variable
import {ConfigModel} from '../core/interfaces/config';

// tslint:disable-next-line:no-shadowed-variable
export class MenuConfig implements ConfigModel {
	public config: any = {};
	public basePath:string = 'client';

	constructor() {
		this.basePath = 'client';
		this.config = {
			header: {
				client: {
					self: {},
					items: [
						{
							title: 'Setup Accounts',
							root: true,
							page: '/',
							icon: 'flaticon-home-1'
						},
						{
							title: 'My Accounts',
							root: true,
							page: '/vendors/my',
							icon: 'flaticon-app'
						},
						{
							title: 'Invoices',
							root: true,
							icon: 'flaticon-coins',
							page: '/invoice'
						},
						{
							title: 'Payment Setup',
							root: true,
							page: '/payment-setup',
							icon: 'flaticon-settings'
						},
						{
							title: 'Activity',
							root: true,
							page: '/activities',
							icon: 'flaticon-graph'
						}
					]
				},
				admin: {
					self: {},
					items: [
						{
							title: 'Dashboard',
							root: true,
							page: '/admin',
						}, {
							title: 'Invoices',
							root: true,
							page: '/admin/invoice',
							icon: 'flaticon-coins'
						}, {
							title: 'Setting',
							root: true,
							page: '/admin/setting',
							icon: 'flaticon-settings'
						}, {
							title: 'Users',
							root: true,
							page: '/admin/users',
							icon: 'flaticon-users'
						}, {
							title: 'Vendors',
							root: true,
							page: '/admin/vendors',
							icon: 'flaticon-map'
						}
					]
				}
			},
			aside: {
				self: {},
				client: {
					self: {},
					items: [
						{section: "Vendor Accounts"},
						{
							title: "Pepco",
							root: true,
							page: '/vendor/Pepco'
						},
						{
							title: "Washington Gas",
							root: true,
							page: '/vendor/Washington Gas'
						},
						{
							title: "DC Water",
							root: true,
							page: '/vendor/DC Water'
						},
						
						{
							title: "MyCheckFree",
							root: true,
							page: '/vendor/MyCheckFree'
						},
						{
							title: "Comcast Business",
							root: true,
							page: '/vendor/BusinessComcast'
						}
					],
				},
				admin: {
					self: {},
					items: [
						{
							title: 'Dashboard',
							root: true,
							page: '/admin',
						}, {
							title: 'Invoices',
							root: true,
							page: '/admin/invoice',
							icon: 'flaticon-coins'
						}, {
							title: 'Setting',
							root: true,
							page: '/admin/setting',
							icon: 'flaticon-settings'
						}, {
							title: 'Users',
							root: true,
							page: '/admin/users',
							icon: 'flaticon-users'
						}, {
							title: 'Vendors',
							root: true,
							page: '/admin/vendors',
							icon: 'flaticon-users'
						}
					]
				}
			}
		}
	}
}
