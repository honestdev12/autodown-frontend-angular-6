import { LayoutModule } from '../layout/layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { PartialsModule } from '../partials/partials.module';
import { WidgetChartsModule } from '../partials/content/widgets/charts/widget-charts.module';
import { ActionComponent } from './header/action/action.component';
import { ProfileComponent } from './header/profile/profile.component';
import { CoreModule } from '../../core/core.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { InnerComponent } from './components/inner/inner.component';

import { SpiderComponent } from './spider/spider.component';
import { DropboxComponent } from './dropbox/dropbox.component';
import { SpiderLogComponent } from './spider-log/spider-log.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { PaymentSetupComponent } from './payment-setup/payment-setup.component';

import { SpiderDialog } from './spider/spider-dialog.component';
import { InvoiceDialog } from './invoice/invoice-dialog.component';
import { PaymentDialog } from './invoice/payment-dialog.component';
import { DropboxDialog } from './dropbox/dropbox-dialog.component';
import { UploadDialog } from './spider/upload-dialog.component';
import { ConfirmDialog } from '../../content/partials/content/general/confirm-dialog/confirm-dialog.component';
import { InfoDialog } from '../../content/partials/content/general/info-dialog/info-dialog.component';

import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { NgxPayPalModule } from 'ngx-paypal';
import { NgxStripeModule } from 'ngx-stripe';
import { MatFileUploadModule } from 'angular-material-fileupload';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	// suppressScrollX: true
};

const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY MM',
    dateA11yLabel: 'YYYY-MM-DD',
    monthYearA11yLabel: 'YYYY MM',
  },
};

import {
	MatButtonModule,
	MatFormFieldModule,
	MatInputModule,
	MatCheckboxModule,
	MatDialogModule,
	MatIconModule,
	MatSortModule,
    MatProgressSpinnerModule,
	MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatProgressBarModule,
    MatTooltipModule,
	MatDatepickerModule,
	MatExpansionModule
} from '@angular/material';


@NgModule({
	declarations: [
		PagesComponent,
		ActionComponent,
		ProfileComponent,
		ErrorPageComponent,
		InnerComponent,
		SpiderComponent,
		PaymentSetupComponent,
		DropboxComponent,
		SpiderLogComponent,
		InvoiceComponent,

		SpiderDialog,
		InvoiceDialog,
		PaymentDialog,
		DropboxDialog,
		UploadDialog
	],
	imports: [
		CommonModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		PagesRoutingModule,
		CoreModule,
		LayoutModule,
		PartialsModule,
		AngularEditorModule,
		
		MatButtonModule,
		MatInputModule,
		MatFormFieldModule,
		MatCheckboxModule,
		MatDialogModule,
		MatTableModule,
		MatIconModule,
		MatSortModule,
	    MatProgressSpinnerModule,
		MatTableModule,
	    MatPaginatorModule,
	    MatSelectModule,
	    MatProgressBarModule,
	    MatTooltipModule,
		MatDatepickerModule,
		MatExpansionModule,

		MatMomentDateModule,
		MatFileUploadModule,

		PerfectScrollbarModule,
		WidgetChartsModule,

		NgxPayPalModule,
		NgxStripeModule.forRoot()
	],
	entryComponents: [
		SpiderDialog,
		ConfirmDialog,
		InfoDialog,
		InvoiceDialog,
		PaymentDialog,
		DropboxDialog,
		UploadDialog,
	],
	providers: [
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		}, {
			provide: MAT_DATE_FORMATS,
			useValue: MY_FORMATS
		}
	]
})
export class PagesModule {
}
