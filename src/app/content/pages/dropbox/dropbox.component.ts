import {
	Component,
	OnInit,
	ViewChild,
	ChangeDetectionStrategy, 
	ChangeDetectorRef
} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
// RXJS
import { tap } from 'rxjs/operators';
import { merge } from 'rxjs';
// Models
import { QueryParamsModel } from '../../../core/models/query-params.model';
// Services
import { DropboxService } from '../../../core/services/dropbox.service';
import { CurrentUser } from '../../../core/services/current-user.service';
import { DropboxModel } from '../../../core/models/dropbox.model';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DropboxDialog } from './dropbox-dialog.component';
import { ConfirmDialog } from '../../../content/partials/content/general/confirm-dialog/confirm-dialog.component';
import { LayoutConfigService } from '../../../core/services/layout-config.service';
import * as objectPath from 'object-path';


@Component({
	selector: 'm-vendor',
	templateUrl: './dropbox.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropboxComponent implements OnInit {
	dropboxes: DropboxModel[] = [];
	displayedColumns = ['name', 'token', 'actions'];

	message: string = null;
	messageType: string = 'success';

	new_dropbox: DropboxModel =  new DropboxModel();

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	selection = new SelectionModel<DropboxModel>(true, []);

	constructor(
		private dropboxService: DropboxService,
		private currentUser: CurrentUser,
		private dialog: MatDialog,
		private ref: ChangeDetectorRef,
		private layoutConfigService: LayoutConfigService
		) {
	}

	ngOnInit() {
		const newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.aside.left.display', false);
		this.layoutConfigService.setModel(newLayoutOption, true);
		
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadItems();
				})
			)
			.subscribe();

		// First load
		this.currentUser.userUpdated$.subscribe(user => {
			this.new_dropbox['user'] = user.id;
		});
		this.loadItems(true);
  	}

  	loadItems(first_time: boolean = false) {
		const queryParams = new QueryParamsModel(
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			first_time ? 10: this.paginator.pageSize 
		);

		this.dropboxService.get(queryParams).subscribe(res => {

			this.dropboxes = res.results;
			this.new_dropbox = new DropboxModel();
			this.ref.detectChanges();
		});
		this.selection.clear();
  	}

	delete(dropbox: DropboxModel): void {
		const dialogRef = this.dialog.open(ConfirmDialog, {
	      	width: '40vw',
	      	data: ''
	    });

	    dialogRef.afterClosed().subscribe(result => {
	    	if (result)
				this.dropboxService.delete(dropbox.id).subscribe(res => this.loadItems()
		)
	    });
		
	}

	openDialog(dropbox: DropboxModel = null): void {
		let data: object = {};
		if (dropbox) {
			data = {'dropbox': dropbox, new: false};
		}
		else data = {'dropbox': this.new_dropbox, new: true};
		const dialogRef = this.dialog.open(DropboxDialog, {
	      	width: '60vw',
	      	'data': data
	    });

	    dialogRef.afterClosed().subscribe(result => {
		    if (result) this.loadItems();
	    });
	}
}
