import {
	Component,
	Inject
} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DropboxModel } from '../../../core/models/dropbox.model';
import { DropboxService } from '../../../core/services/dropbox.service';


@Component({
  selector: 'dropbox-dialog',
  templateUrl: './dropbox-dialog.component.html',
})
export class DropboxDialog
{

	message: string = null;
	messageType: string = 'success';

	constructor(
		public dialogRef: MatDialogRef<DropboxDialog>,
		@Inject(MAT_DIALOG_DATA) public data: { dropbox: DropboxModel, new: boolean},
		private dropboxService: DropboxService
	) {}

	cancel(): void {
		this.dialogRef.close(false);
	}

	save(): void {
		this.dropboxService.create(this.data.dropbox
			).subscribe(
				dropbox => this.successHandler(dropbox), err => this.errorHandler(err)
			);
	}

	update(): void {
		this.dropboxService.update(
			this.data.dropbox.id, 
			this.data.dropbox
			).subscribe(
				dropbox => this.successHandler(dropbox), err => this.errorHandler(err)
			);
	}

	errorHandler(err): void {
		this.message = JSON.stringify(err.error);
		this.messageType = 'error';
	}

	successHandler(dropbox: DropboxModel): void {
		this.messageType = 'success';
		this.message = this.data.new ? 'Added Successfully.' : 'Updated Successfully';
		setTimeout(() => {
			this.message = null;
			this.dialogRef.close(true);
		}, 1000);
	}

}