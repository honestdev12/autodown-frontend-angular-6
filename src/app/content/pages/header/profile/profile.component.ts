import { Component, OnInit, ChangeDetectorRef} from '@angular/core';
import { CurrentUser } from '../../../../core/services/current-user.service';
import { UserService } from '../../../../core/services/user.service';
import { UserModel } from '../../../../core/models/user.model';
import { UserDetailModel } from '../../../../core/models/user-detail.model';
import { SpinnerButtonOptions } from '../../../partials/content/general/spinner-button/button-options.interface';


@Component({
  selector: 'm-profile',
	templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  user: UserModel = new UserModel();

  spinner: SpinnerButtonOptions = {
    active: false,
    spinnerSize: 18,
    raised: true,
    buttonColor: 'primary',
    spinnerColor: 'accent',
    fullWidth: false
  };

  message: string = '';
  messageType: string = 'success';

  constructor(
    private currentUser: CurrentUser,
    private userService: UserService,
    private ref: ChangeDetectorRef
    ) {
  }

  ngOnInit() {
  	this.currentUser.userUpdated$.subscribe(
  		user => this.user = user,
      err => this.errorHandler(err)
    )
  }

  save() {
    this.spinner.active = false;
    this.userService.update(this.user.id, this.user).subscribe(
      user => this.successHandler(user),
      err => this.errorHandler(err)
    );
  }

  successHandler (user: UserModel): void {
    this.messageType = 'success';
    this.message = 'Updated Successfully!';
    this.user = user;
    this.ref.detectChanges();
    this.spinner.active = true;
  }

  errorHandler (err): void {
    this.message = JSON.stringify(err.error);
    this.messageType = 'error';
    this.spinner.active = true;
  }

}
