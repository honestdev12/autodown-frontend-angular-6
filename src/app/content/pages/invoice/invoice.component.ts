import {
	Component,
	OnInit,
	ElementRef,
	ViewChild,
	ChangeDetectionStrategy,
	ChangeDetectorRef
} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
// RXJS
import { tap } from 'rxjs/operators';
import { fromEvent, merge, forkJoin, Observable, of } from 'rxjs';
import * as objectPath from 'object-path';
// Models
import { QueryParamsModel } from '../../../core/models/query-params.model';
import { InvoiceModel } from '../../../core/models/invoice.model';

import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { InvoiceDialog } from './invoice-dialog.component';
import { PaymentDialog } from './payment-dialog.component';
import { ConfirmDialog } from '../../../content/partials/content/general/confirm-dialog/confirm-dialog.component';
import { InfoDialog } from '../../../content/partials/content/general/info-dialog/info-dialog.component';

import { LayoutConfigService } from '../../../core/services/layout-config.service';

import { InvoiceService } from '../../../core/services/invoice.service';

@Component({
	selector: 'm-invoice',
	templateUrl: './invoice.component.html'
})
export class InvoiceComponent implements OnInit {
	invoices: InvoiceModel[] = [];
	is_empty: boolean = true;
	len: number = 0;

	displayedColumns = ['select', 'start_date', 'end_date', 'bill_date', 'amount', 'payment_type', 'payment_id', 'paid', 'actions'];

	new_invoice: InvoiceModel = new InvoiceModel();
	loading$: Observable<boolean> = of(true);

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	selection = new SelectionModel<InvoiceModel>(true, []);

	constructor(
		private route: ActivatedRoute,
		private dialog: MatDialog,
		private layoutConfigService: LayoutConfigService,
		private invoiceService: InvoiceService,
		private ref: ChangeDetectorRef
		) {}

	ngOnInit() {
		// change page config, refer to config/layout.ts
		const newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.aside.left.display', false);
		// const newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.menu.header.display', false);
		this.layoutConfigService.setModel(newLayoutOption, true);

		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.is_empty = false;
					this.loadItems();
				})
			)
			.subscribe();
		this.loadItems(true);
  	}

  	loadItems(first_time: boolean = false) {
  		this.new_invoice = new InvoiceModel();
		const queryParams = new QueryParamsModel(
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			first_time ? 10: this.paginator.pageSize
		);
		this.loading$ = of(true);
		this.invoiceService.get(queryParams).subscribe(res => {
			this.invoices = res.results;
			this.loading$ = of(false);
			this.is_empty = this.invoices.length == 0;
			this.ref.detectChanges();
		});
  	}

	openPaymentDialog(invoice: InvoiceModel=null) : void {
		let invoices: InvoiceModel[];

		invoices = invoice ? [invoice] : this.selection.selected.filter(invoice => !invoice.paid);

		if (invoices.length == 0) {
			const dialogRef = this.dialog.open(InfoDialog, {
				width: '600px',
				data: 'Please select the unpaid invoice!'
			});

		}
		else {
			
			const dialogRef = this.dialog.open(PaymentDialog, {
		      	width: '600px',
		      	data: invoices
		    });

		    dialogRef.afterClosed().subscribe(result => {
			    if (result) this.loadItems();
		    });
		}
	}

	delete(invoice: InvoiceModel): void {
		const dialogRef = this.dialog.open(ConfirmDialog, {
	      	width: '40vw',
	      	data: ''
	    });

	    dialogRef.afterClosed().subscribe(result => {
	    	if (result)
				this.invoiceService.delete(invoice.id).subscribe(res => this.loadItems());
	    });
	}

	getItemStatusString(status: boolean = false): string {
		switch (status) {
			case true:
				return 'Paid';
			case false:
				return 'Unpaid';
		}
		return '';
	}

	getItemCssClassByStatus(status: boolean = false): string {
		switch (status) {
			case true:
				return 'success';
			case false:
				return 'metal';
		}
		return '';
	}

	isAllSelected() {
	  const numSelected = this.selection.selected.length;
	  const numRows = this.invoices.length;
	  return numSelected == numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
	  this.isAllSelected() ?
	      this.selection.clear() :
	      this.invoices.forEach(row => this.selection.select(row));
	}
}
