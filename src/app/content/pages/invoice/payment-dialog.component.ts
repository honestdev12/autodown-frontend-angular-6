import {
	Component,
	Inject,
	OnInit,
	ViewChild,
	ElementRef
} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InvoiceModel } from '../../../core/models/invoice.model';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';
import { PayPalService } from '../../../core/services/paypal.service';
import { Observable, of } from 'rxjs';
import { StripeService, StripeCardComponent, ElementOptions, ElementsOptions } from "ngx-stripe";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'payment-dialog',
  templateUrl: './payment-dialog.component.html',
})
export class PaymentDialog implements OnInit
{
	@ViewChild(StripeCardComponent) card: StripeCardComponent;
 
	cardOptions: ElementOptions = {
		style: {
		  base: {
		    iconColor: '#666EE8',
		    color: '#31325F',
		    lineHeight: '40px',
		    fontWeight: 300,
		    fontSize: '18px',
		    '::placeholder': {
		      color: '#CFD7E0'
		    }
		  }
		}
	};

	elementsOptions: ElementsOptions = {
		locale: 'es'
	};

	stripeTest: FormGroup;

	public payPalConfig?: PayPalConfig;

	displayedColumns = ['no', 'period', 'price'];

	price: number = 0;

	constructor(
		public dialogRef: MatDialogRef<PaymentDialog>,
		@Inject(MAT_DIALOG_DATA) public data: InvoiceModel[],
		public payPalService: PayPalService,
		private fb: FormBuilder,
    	private stripeService: StripeService
	) {}
	
	ngOnInit() {
		this.price = this.data.map(
			invoice => invoice.price_amount
		).reduce((prev, cur) => prev + cur);

		this.payPalConfig = new PayPalConfig(
			PayPalIntegrationType.ClientSideREST, 
			PayPalEnvironment.Sandbox,
			{
		        commit: false,
		        button: {
		          label: 'pay',
		          size: 'small',
		          shape: 'rect',
		          color: 'blue',
		          tagline: false
		        },
		        client: {
		        	sandbox: 'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R'
		        },
		        onPaymentComplete: (data, actions) => {
		          
		        },
		        onCancel: (data, actions) => {
		          console.log('OnCancel');
		        },
		        onError: (err) => {
		          console.log('OnError');
		        },
		        transactions: [{
		        	amount: {
		        		currency: 'USD',
		        		total: this.price
		        	}
		        }]
		    }
	    );

	    this.stripeTest = this.fb.group({
	    	name: ['', [Validators.required]]
	    });

	    this.stripeService.setKey('environment_key');
  	}

	cancel(): void {
		this.dialogRef.close(false);
	}

	pay(): void {
		const name = this.stripeTest.get('name').value;
	    this.stripeService
	      .createToken(this.card.getCard(), { name })
	      .subscribe(result => {
	        if (result.token) {
	          // Use the token to create a charge or a customer
	          // https://stripe.com/docs/charges
	          console.log(result.token.id);
	        } else if (result.error) {
	          
	          console.log(result.error.message);
	        }
	      });
	}
}