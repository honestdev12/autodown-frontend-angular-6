import {
	Component,
	Inject,
	ChangeDetectionStrategy
} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InvoiceModel } from '../../../core/models/invoice.model';
import { InvoiceService } from '../../../core/services/invoice.service';


@Component({
  selector: 'invoice-dialog',
  templateUrl: './invoice-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvoiceDialog
{

	message: string = null;
	messageType: string = 'success';

	constructor(
		public dialogRef: MatDialogRef<InvoiceDialog>,
		@Inject(MAT_DIALOG_DATA) public data: { invoice: InvoiceModel, new: boolean },
		private invoiceService: InvoiceService
	) {}

	cancel(): void {
		this.dialogRef.close(false);
	}

	save(): void {
		this.invoiceService.create(this.data.invoice).subscribe(
				invoice => this.successHandler(invoice), err => this.errorHandler(err)
			);
	}

	update(): void {
		this.invoiceService.update(
			this.data.invoice.id,
			this.data.invoice
		).subscribe(
			invoice => this.successHandler(invoice), err => this.errorHandler(err)
		);
	}

	errorHandler(err): void {
		this.message = JSON.stringify(err.error);
		this.messageType = 'error';
	}

	successHandler(invoice: InvoiceModel): void {
		this.messageType = 'success';
		this.message = this.data.new ? 'Added Successfully.' : 'Updated Successfully';
		setTimeout(() => {
			this.message = null;
			this.dialogRef.close(true);
		}, 1000);
	}

}