import {
	Component,
	OnInit,
	ChangeDetectionStrategy
} from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';

import { PayPalService } from '../../../core/services/paypal.service';

import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { LayoutConfigService } from '../../../core/services/layout-config.service';
import * as objectPath from 'object-path';

@Component({
	selector: 'm-payment-setup',
	templateUrl: './payment-setup.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentSetupComponent implements OnInit {

	message: string = null;
	messageType: string = 'success';

	public payPalConfig?: PayPalConfig;

	constructor(
		private payPalService: PayPalService,
		private layoutConfigService: LayoutConfigService,
		) {}

	ngOnInit() {
		// change page config, refer to config/layout.ts
		const newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.aside.left.display', false);
		this.layoutConfigService.setModel(newLayoutOption, true);

		this.payPalConfig = new PayPalConfig(
			PayPalIntegrationType.ServerSideREST, 
			PayPalEnvironment.Sandbox,
			{
		        commit: false,
		        button: {
		          label: 'pay',
		          size: 'large',
		          shape: 'rect',
		          color: 'blue',
		          tagline: false
		        },
		        onPaymentComplete: (data, actions) => {
		          
		        },
		        onCancel: (data, actions) => {
		          console.log('OnCancel');
		        },
		        onError: (err) => {
		          console.log('OnError');
		        },
		        onAuthorize: (data, actions) => {
		        	return this.payPalService.excute({
	                    paymentID: data.paymentID,
	                    payerID: data.payerID
	                }).pipe(res=> of(console.log(res)));
		        }
		    }
	    );
  	}
}
