import {
	Component,
	OnInit,
	ElementRef,
	ViewChild,
	ChangeDetectionStrategy,
	Input,
	ChangeDetectorRef
} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
// RXJS
import { tap } from 'rxjs/operators';
import { merge, Observable, of } from 'rxjs';
// Models
import { QueryParamsModel } from '../../../core/models/query-params.model';
import { SpiderJobModel } from '../../../core/models/spider-job.model';
// Services
import { SpiderLogService } from '../../../core/services/spider-log.service';

import { SpiderLogModel } from '../../../core/models/spider-log.model';
import { VendorSiteModel } from '../../../core/models/vendor-site.model'

import { animate, state, style, transition, trigger } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FormControl } from '@angular/forms';


@Component({
	selector: 'm-spider-log',
	templateUrl: './spider-log.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpiderLogComponent implements OnInit {
	spider_logs : SpiderLogModel[] = [];

	is_empty: boolean = true;
	len: number = 0;

	displayedColumns = ['file_name', 'bill_date', 'invoice_id', 'account_number', 'download_time', 'actions'];

	loading$: Observable<boolean> = of(true);

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	vendor: string;

	@Input('vendor') set setVendor(value: string) {
		this.vendor = value;
		this.loadItems();
	}

	filter: object = {
		file_name: '',
		bill_date: '',
		invoice_id: '',
		account_number: ''
	}

	constructor(
		private spiderLogService: SpiderLogService,
		private route: ActivatedRoute,
		private dialog: MatDialog,
		private ref: ChangeDetectorRef
		) {}

	ngOnInit() {

		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.is_empty = false;
					this.loadItems();
				})
			)
			.subscribe();

		this.loadItems(true);
  	}

  	loadItems(first_time: boolean = false) {
		const queryParams = new QueryParamsModel(
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			first_time ? 10: this.paginator.pageSize,
			this.filter
		);
		if (this.vendor) {
			this.loading$ = of(true);
			this.spiderLogService.get(queryParams, this.vendor).subscribe(res => {
				this.spider_logs = res.results;
				this.is_empty = this.spider_logs.length == 0;
				this.len = this.spider_logs.length;
				this.loading$ = of(false);
				this.ref.detectChanges();
			});
		}
  	}
}
