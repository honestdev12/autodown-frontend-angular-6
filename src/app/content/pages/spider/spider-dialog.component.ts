import {
	Component,
	Inject
} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { SpiderJobModel } from '../../../core/models/spider-job.model';
import { ScheduleModel } from '../../../core/models/schedule.model';
import { DropboxModel } from '../../../core/models/dropbox.model';
import { SpiderJobService } from '../../../core/services/spider-job.service';


@Component({
  selector: 'spider-dialog',
  templateUrl: './spider-dialog.component.html',
})
export class SpiderDialog
{

	message: string = null;
	messageType: string = 'success';

	constructor(
		public dialogRef: MatDialogRef<SpiderDialog>,
		@Inject(MAT_DIALOG_DATA) public data: { spider: SpiderJobModel, vendor: string, dropboxes: DropboxModel[], new: boolean},
		private spiderJobService: SpiderJobService
	) {}

	cancel(): void {
		this.dialogRef.close(false);
	}

	save(): void {
		this.spiderJobService.create(this.data.vendor, this.data.spider
			).subscribe(
				spider => this.successHandler(spider), err => this.errorHandler(err)
			);
	}

	update(): void {
		this.spiderJobService.update(
			this.data.vendor,
			this.data.spider.id, 
			this.data.spider
			).subscribe(
				spider => this.successHandler(spider), err => this.errorHandler(err)
			);
	}

	errorHandler(err): void {
		this.message = JSON.stringify(err.error);
		this.messageType = 'error';
	}

	successHandler(spider: SpiderJobModel): void {
		this.messageType = 'success';
		this.message = this.data.new ? 'Added Successfully.' : 'Updated Successfully';
		setTimeout(() => {
			this.message = null;
			this.dialogRef.close(true);
		}, 1000);
	}

}