import {
	Component,
	Inject
} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { SpiderJobModel } from '../../../core/models/spider-job.model';

@Component({
  selector: 'upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialog
{

	uploadLink:string = '';
	constructor(
		public dialogRef: MatDialogRef<UploadDialog>,
		@Inject(MAT_DIALOG_DATA) public data: SpiderJobModel
	) {
		this.uploadLink = 'http://localhost:4200/api/v1/' + data.id + '/upload/';
	}

}