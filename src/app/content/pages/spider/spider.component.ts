import {
	Component,
	OnInit,
	ElementRef,
	ViewChild,
	ChangeDetectionStrategy
} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
// RXJS
import { tap } from 'rxjs/operators';
import { merge, Observable, of } from 'rxjs';
// Models
import { QueryParamsModel } from '../../../core/models/query-params.model';
import { SpiderJobModel } from '../../../core/models/spider-job.model';
// Services
import { SpiderJobService } from '../../../core/services/spider-job.service';
import { ScheduleService } from '../../../core/services/schedule.service';

import { DropboxService } from '../../../core/services/dropbox.service';
import { DropboxModel } from '../../../core/models/dropbox.model';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { SpiderDialog } from './spider-dialog.component';
import { UploadDialog } from './upload-dialog.component';
import { ConfirmDialog } from '../../../content/partials/content/general/confirm-dialog/confirm-dialog.component';


@Component({
	selector: 'm-spider',
	templateUrl: './spider.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpiderComponent implements OnInit {
	dropboxes : DropboxModel[] = [];
	spider: SpiderJobModel = new SpiderJobModel();
	new: boolean = true;
	
	message: string = '';
	messageType: string= 'success';

	vendor:string;

	constructor(
		private spiderJobService: SpiderJobService,
		private dropboxService: DropboxService,
		private scheduleService: ScheduleService,
		private route: ActivatedRoute,
		private dialog: MatDialog
		) {}

	ngOnInit() {
		this.getSpider();
		this.route.params.subscribe(params => {
			this.vendor = params['vendor'];
			this.getSpider();
		});
		this.dropboxService.get().subscribe(res => this.dropboxes = res.results);

  	}

  	getSpider() {
  		this.spiderJobService.get(this.vendor).subscribe(res => {
  			this.new = res.results.length == 0
			if (!this.new)
				this.spider = res.results[0];
			else this.spider = new SpiderJobModel();
		});
  	}

  	save(): void {
		this.spiderJobService.create(this.vendor, this.spider
			).subscribe(
				spider => this.successHandler(spider), err => this.errorHandler(err)
			);
	}

	update(): void {
		this.spiderJobService.update(
			this.vendor,
			this.spider.id, 
			this.spider
			).subscribe(
				spider => this.successHandler(spider), err => this.errorHandler(err)
			);
	}

	upload(spider: SpiderJobModel): void {
		const dialogRef = this.dialog.open(UploadDialog, {
	      	width: '60vw',
	      	data: spider
	    });

	    dialogRef.afterClosed().subscribe(result => {
	    });
	}

	errorHandler(err): void {
		this.message = JSON.stringify(err.error);
		this.messageType = 'error';
	}

	successHandler(spider: SpiderJobModel): void {
		this.messageType = 'success';
		this.message = this.new ? 'Created Successfully.' : 'Updated Successfully';
		this.spider = spider;
		setTimeout(() => {
			this.message = null;
		}, 1000);
	}
}
