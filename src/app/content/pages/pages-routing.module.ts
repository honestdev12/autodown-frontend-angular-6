import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { SpiderComponent } from './spider/spider.component';
import { DropboxComponent } from './dropbox/dropbox.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ProfileComponent } from './header/profile/profile.component';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { InnerComponent } from "./components/inner/inner.component";
import { AuthenticationGuard } from '../../core/auth/authentication-guard.service';
import { SpiderLogComponent } from './spider-log/spider-log.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { PaymentSetupComponent } from './payment-setup/payment-setup.component';

const routes: Routes = [
	{
		path: '',
		component: PagesComponent,
		canActivate: [NgxPermissionsGuard],
		data: {
			permissions: {
				only: ['ADMIN', 'USER'],
				redirectTo: '/login'
			}
		},
		children: [
			{
				path: '',
				loadChildren: './components/dashboard/dashboard.module#DashboardModule'
			},
			{
				path: 'admin',
				loadChildren: './components/admin/admin.module#AdminModule',
				data: {
					permissions: {
						only: ['ADMIN'],
						redirectTo: ''
					}
				},
			},
			{
				path: 'builder',
				loadChildren: './builder/builder.module#BuilderModule'
			},
			{
				path: 'vendor/:vendor',
				component: SpiderComponent
			},
			{
				path: 'spider-log/:spider_job_id',
				component: SpiderLogComponent
			},
			{
				path: 'Dropbox',
				component: DropboxComponent
			},
			{
				path: 'profile',
				component: ProfileComponent
			},
			{
				path: 'invoice',
				component: InvoiceComponent
			},
			{
				path: 'payment-setup',
				component: PaymentSetupComponent
			},
		]
	},
	{
		path: 'login',
		canActivate: [NgxPermissionsGuard],
		loadChildren: './auth/auth.module#AuthModule',
		data: {
			permissions: {
				except: 'ADMIN'
			}
		},
	},
	{
		path: '404',
		component: ErrorPageComponent
	},
	{
		path: 'error/:type',
		component: ErrorPageComponent
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
