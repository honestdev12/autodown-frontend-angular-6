import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layout/layout.module';
import { PartialsModule } from '../../../partials/partials.module';
import { ListTimelineModule } from '../../../partials/layout/quick-sidebar/list-timeline/list-timeline.module';
import { WidgetChartsModule } from '../../../partials/content/widgets/charts/widget-charts.module';

import { AdminComponent } from './admin.component';
import { AdminInvoiceComponent } from './admin-invoice/admin-invoice.component';
import { AdminInvoiceDialog } from './admin-invoice/admin-invoice-dialog.component';
import { SettingComponent } from './setting/setting.component';
import { UserComponent } from './users/user.component';
import { AdminVendorComponent } from './admin-vendor/admin-vendor.component';
import { AdminVendorDialog } from './admin-vendor/admin-vendor-dialog.component';


import { FormsModule } from '@angular/forms';
import {
	MatButtonModule,
	MatInputModule,
	MatCheckboxModule,
	MatDialogModule,
	MatIconModule,
	MatSortModule,
    MatProgressSpinnerModule,
	MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatProgressBarModule,
    MatTooltipModule,
	MatDatepickerModule
} from '@angular/material';

@NgModule({
	imports: [
		CommonModule,
		LayoutModule,
		PartialsModule,
		ListTimelineModule,
		WidgetChartsModule,
		
		MatButtonModule,
		MatInputModule,
		MatCheckboxModule,
		MatDialogModule,
		MatIconModule,
		MatSortModule,
	    MatProgressSpinnerModule,
		MatTableModule,
	    MatPaginatorModule,
	    MatSelectModule,
	    MatProgressBarModule,
	    MatTooltipModule,
		MatDatepickerModule,

		FormsModule,
		RouterModule.forChild([
			{
				path: '',
				component: AdminComponent
			}, {
				path: 'invoice',
				component: AdminInvoiceComponent
			}, {
				path: ':userId/:paid/invoice',
				component: AdminInvoiceComponent
			}, {
				path: 'setting',
				component: SettingComponent
			}, {
				path: 'users',
				component: UserComponent
			}, {
				path: 'vendors',
				component: AdminVendorComponent
			}
		])
	],
	providers: [],
	declarations: [
		AdminComponent,
		AdminInvoiceComponent,
		AdminInvoiceDialog,
		AdminVendorDialog,
		SettingComponent,
		UserComponent,
		AdminVendorComponent
		
	],
	entryComponents: [
		AdminInvoiceDialog,
		AdminVendorDialog
	]
})
export class AdminModule {}
