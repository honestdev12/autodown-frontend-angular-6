import {
	Component,
	OnInit,
	ElementRef,
	ViewChild,
	ChangeDetectionStrategy,
	ChangeDetectorRef
} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { 
	MatPaginator, 
	MatSort,
	MatDialog, 
	MatDialogRef
} from '@angular/material';
// RXJS
import { tap } from 'rxjs/operators';
import { fromEvent, merge, forkJoin, Observable, of } from 'rxjs';
// Models
import { QueryParamsModel } from '../../../../../core/models/query-params.model';
import { UserModel } from '../../../../../core/models/user.model';

import { ActivatedRoute } from '@angular/router';

import { ConfirmDialog } from '../../../../../content/partials/content/general/confirm-dialog/confirm-dialog.component';

import { UserService } from '../../../../../core/services/user.service';

@Component({
	selector: 'm-user',
	templateUrl: './user.component.html'
})
export class UserComponent implements OnInit {
	users: UserModel[] = [];
	is_empty: boolean = true;
	len: number = 0;

	displayedColumns = ['username', 'user', 'email', 'unpaid_invoice', 'active', 'roles', 'actions', ];

	loading$: Observable<boolean> = of(true);

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	selection = new SelectionModel<UserModel>(true, []);

	constructor(
		private dialog: MatDialog,
		private userService: UserService,
		private ref: ChangeDetectorRef
		) {}

	ngOnInit() {

		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.is_empty = false;
					this.loadItems();
				})
			)
			.subscribe();
		this.loadItems(true);
  	}

  	loadItems(first_time: boolean = false) {
  		
		const queryParams = new QueryParamsModel(
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			first_time ? 10: this.paginator.pageSize
		);
		this.loading$ = of(true);
		this.userService.get(queryParams).subscribe(res => {
			this.users = res.results;
			this.loading$ = of(false);
			this.is_empty = this.users.length == 0;
			this.ref.detectChanges();
		});
  	}


	delete(user: UserModel): void {
		const dialogRef = this.dialog.open(ConfirmDialog, {
	      	width: '40vw',
	      	data: ''
	    });

	    dialogRef.afterClosed().subscribe(result => {
	    	if (result)
				this.userService.delete(user.id).subscribe(res => this.loadItems());
	    });
	}

	getItemStatusString(status: boolean = false): string {
		switch (status) {
			case true:
				return 'Enabled';
			case false:
				return 'Disabled';
		}
		return '';
	}

	getItemCssClassByStatus(status: boolean = false): string {
		switch (status) {
			case true:
				return 'success';
			case false:
				return 'metal';
		}
		return '';
	}

	isAllSelected() {
	  const numSelected = this.selection.selected.length;
	  const numRows = this.users.length;
	  return numSelected == numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
	  this.isAllSelected() ?
	      this.selection.clear() :
	      this.users.forEach(row => this.selection.select(row));
	}
}
