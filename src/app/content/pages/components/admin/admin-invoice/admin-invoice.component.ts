import {
	Component,
	OnInit,
	ElementRef,
	ViewChild,
	ChangeDetectionStrategy,
	ChangeDetectorRef
} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { 
	MatPaginator, 
	MatSort,
	MatDialog, 
	MatDialogRef
} from '@angular/material';
// RXJS
import { tap } from 'rxjs/operators';
import { fromEvent, merge, forkJoin, Observable, of } from 'rxjs';
// Models
import { QueryParamsModel } from '../../../../../core/models/query-params.model';
import { UserModel } from '../../../../../core/models/user.model';
import { InvoiceModel } from '../../../../../core/models/invoice.model';

import { ActivatedRoute } from '@angular/router';

import { AdminInvoiceDialog } from './admin-invoice-dialog.component';
import { ConfirmDialog } from '../../../../../content/partials/content/general/confirm-dialog/confirm-dialog.component';

import { InvoiceService } from '../../../../../core/services/invoice.service';
import { UserService } from '../../../../../core/services/user.service';

@Component({
	selector: 'm-admin-invoice',
	templateUrl: './admin-invoice.component.html'
})
export class AdminInvoiceComponent implements OnInit {
	invoices: InvoiceModel[] = [];
	is_empty: boolean = true;
	len: number = 0;

	displayedColumns = ['user', 'email', 'start_date', 'end_date', 'payment_type', 'payment_id', 'paid', 'count', 'amount', 'actions'];

	new_invoice: InvoiceModel = new InvoiceModel();
	loading$: Observable<boolean> = of(true);

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	selection = new SelectionModel<InvoiceModel>(true, []);

	userId: string = '';
	paid: string = 'unpaid';

	users: UserModel[] = [];

	constructor(
		private route: ActivatedRoute,
		private dialog: MatDialog,
		private invoiceService: InvoiceService,
		private userService: UserService,
		private ref: ChangeDetectorRef
		) {}

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.userId = 'userId' in params ? params['userId'] : '';
			this.paid = 'paid' in params ? params['paid'] : 'unpaid';
		});

		this.userService.get().subscribe(res=>this.users = res.results);

		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.is_empty = false;
					this.loadItems();
				})
			)
			.subscribe();
		this.loadItems(true);
  	}

  	loadItems(first_time: boolean = false) {
  		this.new_invoice = new InvoiceModel();
		const queryParams = new QueryParamsModel(
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			first_time ? 10: this.paginator.pageSize
		);
		this.loading$ = of(true);
		this.invoiceService.get(queryParams, this.paid, true, this.userId).subscribe(res => {
			this.invoices = res.results;
			this.loading$ = of(false);
			this.is_empty = this.invoices.length == 0;
			this.ref.detectChanges();
		});
  	}

	createInvoiceDialog(): void {
		const dialogRef = this.dialog.open(AdminInvoiceDialog, {
	      	width: '60vw',
	      	data: {
	      		'invoice': this.new_invoice, 
	      		new: true, 
	      		'users': this.users
	      	}
	    });

	    dialogRef.afterClosed().subscribe(result => {
	    	if (result) this.loadItems();
	    });
	}

	editInvoiceDialog(invoice: InvoiceModel): void {
		const dialogRef = this.dialog.open(AdminInvoiceDialog, {
	      	width: '60vw',
	      	data: {'invoice': invoice, new: false, 'users': this.users}
	    });

	    dialogRef.afterClosed().subscribe(result => {
	    	if (result) this.loadItems();
	    });
	}

	delete(invoice: InvoiceModel): void {
		const dialogRef = this.dialog.open(ConfirmDialog, {
	      	width: '40vw',
	      	data: ''
	    });

	    dialogRef.afterClosed().subscribe(result => {
	    	if (result)
				this.invoiceService.delete(invoice.id).subscribe(res => this.loadItems());
	    });
	}

	getItemStatusString(status: boolean = false): string {
		switch (status) {
			case true:
				return 'Paid';
			case false:
				return 'Unpaid';
		}
		return '';
	}

	getItemCssClassByStatus(status: boolean = false): string {
		switch (status) {
			case true:
				return 'success';
			case false:
				return 'metal';
		}
		return '';
	}

	isAllSelected() {
	  const numSelected = this.selection.selected.length;
	  const numRows = this.invoices.length;
	  return numSelected == numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
	  this.isAllSelected() ?
	      this.selection.clear() :
	      this.invoices.forEach(row => this.selection.select(row));
	}
}
