import {
	Component,
	Inject
} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VendorSiteModel } from '../../../../../core/models/vendor-site.model';
import { VendorSiteService } from '../../../../../core/services/vendor-site.service';



@Component({
  selector: 'admin-vendor-dialog',
  templateUrl: './admin-vendor-dialog.component.html',
})
export class AdminVendorDialog
{

	message: string = null;
	messageType: string = 'success';

	constructor(
		public dialogRef: MatDialogRef<AdminVendorDialog>,
		@Inject(MAT_DIALOG_DATA) public data: { vendor: VendorSiteModel, new: boolean },
		private vendorSiteService: VendorSiteService
	) {}

	cancel(): void {
		this.dialogRef.close(false);
	}

	save(): void {
		this.vendorSiteService.create(this.data.vendor).subscribe(
				vendor => this.successHandler(vendor), err => this.errorHandler(err)
			);
	}

	update(): void {
		this.vendorSiteService.update(
			this.data.vendor.id,
			this.data.vendor
		).subscribe(
			vendor => this.successHandler(vendor), err => this.errorHandler(err)
		);
	}

	errorHandler(err): void {
		this.message = JSON.stringify(err.error);
		this.messageType = 'error';
	}

	successHandler(vendor: VendorSiteModel): void {
		this.messageType = 'success';
		this.message = this.data.new ? 'Added Successfully.' : 'Updated Successfully';
		setTimeout(() => {
			this.message = null;
			this.dialogRef.close(true);
		}, 1000);
	}

}