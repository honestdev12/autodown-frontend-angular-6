import {
	Component,
	OnInit,
	ElementRef,
	ViewChild,
	ChangeDetectionStrategy,
	ChangeDetectorRef
} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { 
	MatPaginator, 
	MatSort,
	MatDialog, 
	MatDialogRef
} from '@angular/material';
// RXJS
import { tap } from 'rxjs/operators';
import { fromEvent, merge, forkJoin, Observable, of } from 'rxjs';
// Models
import { QueryParamsModel } from '../../../../../core/models/query-params.model';
import { UserModel } from '../../../../../core/models/user.model';
import { VendorSiteModel } from '../../../../../core/models/vendor-site.model';

import { ActivatedRoute } from '@angular/router';

import { AdminVendorDialog } from './admin-vendor-dialog.component';
import { ConfirmDialog } from '../../../../../content/partials/content/general/confirm-dialog/confirm-dialog.component';

import { VendorSiteService } from '../../../../../core/services/vendor-site.service';
import { UserService } from '../../../../../core/services/user.service';

@Component({
	selector: 'm-admin-vendor',
	templateUrl: './admin-vendor.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminVendorComponent implements OnInit {
	vendors: VendorSiteModel[] = [];
	is_empty: boolean = true;
	len: number = 0;

	displayedColumns = ['name', 'type', 'brand_img', 'actions'];

	new_vendor: VendorSiteModel = new VendorSiteModel();

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	selection = new SelectionModel<VendorSiteModel>(true, []);

	constructor(
		private route: ActivatedRoute,
		private dialog: MatDialog,
		private vendorSiteService: VendorSiteService,
		private userService: UserService,
		private ref: ChangeDetectorRef
		) {
		this.vendorSiteService.dataUpdated$.subscribe(vendors => {
			this.vendors = vendors
			this.len = this.vendors.length;
			this.is_empty = this.len == 0;
			this.ref.detectChanges();
		});
	}

	ngOnInit() {
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.is_empty = false;
					this.loadItems();
				})
			)
			.subscribe();
		this.loadItems(true);
  	}

  	loadItems(first_time: boolean = false) {
  		this.new_vendor = new VendorSiteModel();
		const queryParams = new QueryParamsModel(
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			first_time ? 10: this.paginator.pageSize
		);
		this.vendorSiteService.get(queryParams, true, true);
  	}

	createVendorDialog(): void {
		const dialogRef = this.dialog.open(AdminVendorDialog, {
	      	width: '60vw',
	      	data: {
	      		'vendor': this.new_vendor, 
	      		new: true
	      	}
	    });

	    dialogRef.afterClosed().subscribe(result => {
	    	if (result) this.loadItems();
	    });
	}

	editVendorDialog(vendor: VendorSiteModel): void {
		const dialogRef = this.dialog.open(AdminVendorDialog, {
	      	width: '60vw',
	      	data: {'vendor': vendor, new: false}
	    });

	    dialogRef.afterClosed().subscribe(result => {
	    	if (result) this.loadItems();
	    });
	}

	delete(vendor: VendorSiteModel): void {
		const dialogRef = this.dialog.open(ConfirmDialog, {
	      	width: '40vw',
	      	data: ''
	    });

	    dialogRef.afterClosed().subscribe(result => {
	    	if (result)
				this.vendorSiteService.delete(vendor.id).subscribe(res => this.loadItems());
	    });
	}

	getTypeString(type:string): string {
		switch (type) {
			case "website":
				return "Vendor";
				
			case "saas":
				return "SAAS";
		}
	}

	getItemCssClassByType(type: string): string {
		switch (type) {
			case "website":
				return "success";
				
			case "saas":
				return "metal";
		}
	}

	isAllSelected() {
	  const numSelected = this.selection.selected.length;
	  const numRows = this.vendors.length;
	  return numSelected == numRows;
	}

	/** Selects all rows if 																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																													they are not all selected; otherwise clear selection. */
	masterToggle() {
	  this.isAllSelected() ?
	      this.selection.clear() :
	      this.vendors.forEach(row => this.selection.select(row));
	}
}
