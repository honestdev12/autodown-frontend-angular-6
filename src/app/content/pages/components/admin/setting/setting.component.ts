import { Component, OnInit, ChangeDetectorRef} from '@angular/core';
import { SettingService } from '../../../../../core/services/setting.service';
import { SettingModel } from '../../../../../core/models/setting.model';
import { SpinnerButtonOptions } from '../../../../partials/content/general/spinner-button/button-options.interface';


@Component({
  selector: 'm-setting',
	templateUrl: './setting.component.html'
})
export class SettingComponent implements OnInit {

  spinner: SpinnerButtonOptions = {
    active: false,
    spinnerSize: 18,
    raised: true,
    buttonColor: 'primary',
    spinnerColor: 'accent',
    fullWidth: false
  };

  message: string = null;
  messageType: string = 'success';

  setting: SettingModel = new SettingModel();

  constructor(
    private settingService: SettingService,
    private ref: ChangeDetectorRef
    ) {
  }

  ngOnInit() {
    this.settingService.settingUpdated$.subscribe(
      setting => {
        this.setting = setting;
        this.ref.detectChanges();
      },
      err => console.log(err)
    )
  }

  save() {
    this.spinner.active = false;
    this.settingService.update(this.setting);
  }

  successHandler (setting: SettingModel): void {
    this.setting = setting;
    this.messageType = 'success';
    this.message = 'Updated Successfully!';
    this.ref.detectChanges();
    this.spinner.active = false;
  }

  errorHandler (err): void {
    this.message = JSON.stringify(err.error);
    this.messageType = 'error';
    this.spinner.active = false;
  }
}
