import { ChangeDetectionStrategy, Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutConfigService } from '../../../../../core/services/layout-config.service';
import { SubheaderService } from '../../../../../core/services/layout/subheader.service';
import { ChartParamsModel } from '../../../../../core/models/chart-params.model';
import { VendorSiteModel } from '../../../../../core/models/vendor-site.model';
import { ChartDataService } from '../../../../../core/services/chart-data.service';
import { VendorSiteService } from '../../../../../core/services/vendor-site.service';
import * as _moment from 'moment';


import * as objectPath from 'object-path';

@Component({
	selector: 'm-activities',
	templateUrl: './activities.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActivitiesComponent implements OnInit {

	public config: any;

	@Input('vendor-id') vendorId: string = '-1';
	periods: string[] = ['week', 'month'];
	period: string = 'week';


	fromDate: any;
	endDate: any;

	showVendorFilter: boolean = false;
	vendors: VendorSiteModel[] = [];

	constructor(
		private router: Router,
		private layoutConfigService: LayoutConfigService,
		private subheaderService: SubheaderService,
		private chartDataService: ChartDataService,
		private vendorSiteService: VendorSiteService
	) {
		this.showVendorFilter = this.vendorId == '-1'
		this.filter();
		this.vendorSiteService.get();
		this.vendorSiteService.dataUpdated$.subscribe(
			vendors => this.vendors = vendors
		);
	}

	ngOnInit(): void {
		
		// change page config, refer to config/layout.ts
		const newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.aside.left.display', false);
		// const newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.menu.header.display', false);
		this.layoutConfigService.setModel(newLayoutOption, true);
	}

	filter(): void {
		const queryParams = new ChartParamsModel(this.vendorId, this.period, this.fromDate, this.endDate);
		this.chartDataService.get(queryParams);
	}
}
