import { 
	ChangeDetectionStrategy, 
	Component, 
	OnInit,
	ChangeDetectorRef
} from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { LayoutConfigService } from '../../../../../core/services/layout-config.service';
import { VendorSiteService } from '../../../../../core/services/vendor-site.service';
import { VendorSiteModel } from '../../../../../core/models/vendor-site.model';

import * as objectPath from 'object-path';

@Component({
	selector: 'm-vendor',
	templateUrl: './vendor.component.html',
	styleUrls: ['./vendor.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class VendorComponent implements OnInit {

	vendors: VendorSiteModel[] = [];
	myVendors: VendorSiteModel[] = [];
	my: boolean = true;

	constructor(
		private router: Router,
		private layoutConfigService: LayoutConfigService,
		private vendorSiteService: VendorSiteService,
		private ref: ChangeDetectorRef
	) {
		this.vendorSiteService.dataUpdated$.subscribe(
			vendors => { 
				if (vendors.length) {
					this.vendors = vendors;
					if (!this.my) this.ref.detectChanges();
				}
			}
		);
		this.vendorSiteService.myDataUpdated$.subscribe(
			vendors => {
				if (vendors.length) {
					this.myVendors = vendors;
					if (this.my) this.ref.detectChanges();
				}
			});
	}

	update(url): void {
		if (url == '/vendors/my') {
			this.my = true;
			this.vendorSiteService.get();
		}
		else if (url == '/') {
			this.my = false;
			this.vendorSiteService.get(null, true);
		}

	}

	ngOnInit(): void {
		// change page config, refer to config/layout.ts
		const newLayoutOption = objectPath.set(this.layoutConfigService.layoutConfig, 'config.aside.left.display', false);
		this.layoutConfigService.setModel(newLayoutOption, true);
		this.update(this.router.url);
	}

	getLink(vendor): string {
		return vendor.type == 'website' ? '/vendor/' + vendor.name : '/' + vendor.name;
	}
}
