import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { VendorComponent } from './vendors/vendor.component';

import { NgModule } from '@angular/core';

const routes: Routes = [
	{
		path: 'activities',
		component: DashboardComponent
	}, {
		path: '',
		component: VendorComponent
	}, {
		path: 'vendors/my',
		component: VendorComponent
	}
]

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DashboardRoutingModule {
}
