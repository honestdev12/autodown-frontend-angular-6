import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard.component';
import { ActivitiesComponent } from './activities/activities.component';
import { VendorComponent } from './vendors/vendor.component';

import { RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layout/layout.module';

import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { PartialsModule } from '../../../partials/partials.module';
import { ListTimelineModule } from '../../../partials/layout/quick-sidebar/list-timeline/list-timeline.module';
import { WidgetChartsModule } from '../../../partials/content/widgets/charts/widget-charts.module';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';

import { DashboardRoutingModule } from './dashboard-routing.module';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	// suppressScrollX: true
};

const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'YYYY MM',
    dateA11yLabel: 'YYYY-MM-DD',
    monthYearA11yLabel: 'YYYY MM',
  },
};


import {
	MatButtonModule,
	MatFormFieldModule,
	MatInputModule,
	MatCheckboxModule,
	MatDialogModule,
	MatIconModule,
	MatSortModule,
    MatProgressSpinnerModule,
	MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatProgressBarModule,
    MatTooltipModule,
	MatDatepickerModule,
	MatExpansionModule
} from '@angular/material';


@NgModule({
	imports: [
		CommonModule,
		LayoutModule,
		PartialsModule,
		ListTimelineModule,
		WidgetChartsModule,
		DashboardRoutingModule,

		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		MatCheckboxModule,
		MatDialogModule,
		MatIconModule,
		MatSortModule,
	    MatProgressSpinnerModule,
		MatTableModule,
	    MatPaginatorModule,
	    MatSelectModule,
	    MatProgressBarModule,
	    MatTooltipModule,
		MatDatepickerModule,
		MatExpansionModule,

		PerfectScrollbarModule,

		FormsModule,
		ReactiveFormsModule,

	],
	providers: [{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		},{
			provide: MAT_DATE_FORMATS,
			useValue: MY_FORMATS
		}
	],
	declarations: [
		DashboardComponent,
		ActivitiesComponent,
		VendorComponent
	],
	exports: [
		ActivitiesComponent
	]
})
export class DashboardModule {}
