import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ChartDataService } from '../../../../../../core/services/chart-data.service';

import { BaseChartDirective } from 'ng2-charts';

@Component({
	selector: 'm-line-chart',
	templateUrl: './line-chart.component.html',
	styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {

	public lineChartOptions: any = {
		scaleShowVerticalLines: false,
		responsive: true
	};
	public lineChartLabels: string[] = ['2018-10-3', '2018-10-4', '2018-10-5', '2018-10-6', '2018-10-7', '2018-10-8', '2018-10-9'];
	public lineChartType: string = 'line';
	public lineChartLegend: boolean = true;

	public lineChartData: any[] = [
		{data: [65, 59, 80, 81, 56, 55, 40], label: 'Spider 1'},
		{data: [28, 48, 40, 19, 86, 27, 90], label: 'Spider 2'}
	];

	@ViewChild("baseChart") chart: BaseChartDirective;

	constructor (
		private chartDataService: ChartDataService
	) { 
		this.chartDataService.chartDataUpdated$.subscribe(
			res => {
				if (res) {
					this.lineChartLabels.length = 0;
					this.lineChartLabels.push(...res.labels);
					this.lineChartData.length = 0;
					this.lineChartData.push(...res.data);
					if (this.chart !== undefined) {
						this.chart.ngOnDestroy();
						this.chart.chart = this.chart.getChartBuilder(this.chart.ctx);
					}
				}
			});
	}

	ngOnInit () {

	}
	
	chartClicked (e: any): void {														
	}

	chartHovered (e: any): void {
	}

	changePeriod(e: any): void {
	}
}
