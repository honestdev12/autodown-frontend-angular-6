import { UserModel } from '../../../../core/models/user.model';
import {
	Component,
	OnInit,
	HostBinding,
	Input,
	AfterViewInit,
	ChangeDetectionStrategy
} from '@angular/core';
import * as objectPath from 'object-path';
import { Router } from '@angular/router';

@Component({
	selector: 'm-topbar',
	templateUrl: './topbar.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopbarComponent implements OnInit, AfterViewInit {
	@HostBinding('id') id = 'm_header_nav';
	@HostBinding('class')
	classes = 'm-stack__item m-stack__item--right m-header-head';

	@Input() searchType: any;
	@Input() user: UserModel;

	showSearch: boolean = false;

	constructor(
		private router: Router
	) {}

	ngOnInit(): void {
		this.showSearch = window.innerWidth < 1025;
	}

	ngAfterViewInit(): void {}
}
