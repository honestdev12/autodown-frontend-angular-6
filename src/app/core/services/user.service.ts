import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';
import { HttpUtilsService } from './http-utils.service';
import { QueryParamsModel } from '../models/query-params.model';
import { GetResultsModel } from '../models/get-results.model';

@Injectable()
export class UserService {

	API_LINK: string = 'admin/users';

	constructor(private httpUtils: HttpUtilsService) {
	}

	get(queryParams: QueryParamsModel=null): Observable<GetResultsModel> {
		return this.httpUtils.get<any>(this.API_LINK + '/', queryParams);
	}

	update(id: number, data: UserModel): Observable<UserModel> {
		return this.httpUtils.put<UserModel>(this.API_LINK + '/', id, data)
	}

	create(data: UserModel) : Observable<UserModel> {
		return this.httpUtils.post<UserModel>(this.API_LINK + '/', data)
	}

	delete(id: number): Observable<any> {
		return this.httpUtils.delete<any>(this.API_LINK + '/', id);
	}
}
