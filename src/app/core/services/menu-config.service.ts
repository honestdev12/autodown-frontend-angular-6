import { Injectable } from '@angular/core';
import { ConfigData } from '../interfaces/config-data';
import { BehaviorSubject } from 'rxjs';
import { filter} from 'rxjs/operators';
import * as objectPath from 'object-path';
import { Router, NavigationStart, NavigationEnd} from '@angular/router';
import { MenuConfig } from '../../config/menu';
import { CurrentUser } from './current-user.service';

@Injectable()
export class MenuConfigService {
	public configModel: MenuConfig = new MenuConfig();
	public onMenuUpdated$: BehaviorSubject<MenuConfig> = new BehaviorSubject(
		this.configModel
	);
	menuHasChanged: any = false;

	constructor(private router: Router, private currentUser: CurrentUser) {
		this.configModel.basePath = this.router.url.indexOf('admin') >= 0 ? 'admin' : 'client';
		this.setModel(this.configModel);

		this.router.events
			.subscribe(event => {
				if (event instanceof NavigationStart) {
					if (this.menuHasChanged)
						this.resetModel();
					if (event.url.indexOf('admin') >= 0 && this.configModel.basePath != 'admin') {
						this.configModel.basePath = 'admin';
						this.setModel(this.configModel);
					}
					else if (event.url.indexOf('admin') < 0 && this.configModel.basePath != 'client') {
						this.configModel.basePath = 'client';
						this.setModel(this.configModel);
					}
				}			
			});
	}

	setModel(menuModel: MenuConfig) {
		this.configModel = Object.assign(this.configModel, menuModel);
		this.onMenuUpdated$.next(this.configModel);
		this.menuHasChanged = true;
	}

	resetModel() {
		this.configModel = new MenuConfig();
		this.onMenuUpdated$.next(this.configModel);
		this.menuHasChanged = false;
	}

}
