import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpUtilsService } from './http-utils.service';
import { PayPalModel } from '../models/paypal.model';


@Injectable()
export class PayPalService {

	CREATE_URL = 'paypal/create';
	EXCUTE_URL = 'paypal/excute';

	constructor(private httpUtils: HttpUtilsService) {
	}

	create(): Observable<PayPalModel> {
		return this.httpUtils.post<PayPalModel>(this.CREATE_URL, {});
	}

	excute(data: PayPalModel) {
		return this.httpUtils.post<PayPalModel>(this.EXCUTE_URL, data)
	}

}
