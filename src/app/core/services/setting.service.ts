import { Injectable } from '@angular/core';
import { HttpUtilsService } from './http-utils.service';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { SettingModel } from '../models/setting.model';


@Injectable({
	providedIn: 'root'
})
export class SettingService {

	API_LINK: string = 'admin/config';
	setting: SettingModel = new SettingModel();
	public settingUpdated$: BehaviorSubject<SettingModel> = new BehaviorSubject(
		this.setting
		)


	constructor(private httpUtils: HttpUtilsService) {
		this.get();
	}

	get() {
		this.httpUtils.get<SettingModel>(this.API_LINK + '/').subscribe(
			setting => this.set(setting));
	}

	set(setting) {
		this.setting = setting;
		this.settingUpdated$.next(this.setting);
	}

	update(data: SettingModel) {
		this.httpUtils.post<SettingModel>(this.API_LINK + '/', data).subscribe(
			setting => this.set(setting)
		);
	}

	clear() {
		this.setting = null;
		this.settingUpdated$.next(this.setting);
	}

}
