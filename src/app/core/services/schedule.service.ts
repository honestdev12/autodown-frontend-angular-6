import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ScheduleModel } from '../models/schedule.model';
import { HttpUtilsService } from './http-utils.service';
import { QueryParamsModel } from '../models/query-params.model';
import { GetResultsModel } from '../models/get-results.model';


@Injectable()
export class ScheduleService {	

	API_LINK: string = 'schedule';

	constructor(private httpUtils: HttpUtilsService) {
	}

	get(queryParams: QueryParamsModel): Observable<GetResultsModel> {
		return this.httpUtils.get<GetResultsModel>(this.API_LINK + '/', queryParams);
	}

	update(id: number, data: object): Observable<ScheduleModel> {
		return this.httpUtils.put<ScheduleModel>(this.API_LINK + '/', id, data)
	}

	create(data: object) : Observable<ScheduleModel> {
		return this.httpUtils.post<ScheduleModel>(this.API_LINK + '/', data)
	}

	delete(id: number): Observable<any> {
		return this.httpUtils.delete<any>(this.API_LINK + '/', id);
	}
}
