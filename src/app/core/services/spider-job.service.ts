import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SpiderJobModel } from '../models/spider-job.model';
import { HttpUtilsService } from './http-utils.service';
import { QueryParamsModel } from '../models/query-params.model';
import { GetResultsModel } from '../models/get-results.model';


@Injectable()
export class SpiderJobService {	

	API_LINK: string = 'spider_job';
	
	constructor(private httpUtils: HttpUtilsService) {
	}

	get(vendor: string, queryParams: QueryParamsModel=null): Observable<GetResultsModel> {
		return this.httpUtils.get<GetResultsModel>(vendor + '/' + this.API_LINK + '/', queryParams);
	}

	update(vendor: string, id: number, data: object): Observable<SpiderJobModel> {
		console.log('spider-job service', data);
		return this.httpUtils.put<SpiderJobModel>(vendor + '/' + this.API_LINK + '/', id, data)
	}

	create(vendor: string, data: object) : Observable<SpiderJobModel> {
		return this.httpUtils.post<SpiderJobModel>(vendor + '/' + this.API_LINK + '/', data)
	}

	delete(vendor: string, id: number): Observable<any> {
		return this.httpUtils.delete<any>(vendor + '/' + this.API_LINK + '/', id);
	}
}
