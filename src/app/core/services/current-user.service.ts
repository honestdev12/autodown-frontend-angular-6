import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { UserModel } from '../models/user.model';
import { UserDetailModel } from '../models/user-detail.model';
import { HttpUtilsService } from './http-utils.service'
import { map } from 'rxjs/operators';
import { TokenStorage } from '../auth/token-storage.service';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
	providedIn: 'root'
})
export class CurrentUser {
	currentUser:UserModel = null;
	public userUpdated$ : BehaviorSubject<UserModel> = new BehaviorSubject(
		this.currentUser
	)

	constructor(
		private tokenStorage: TokenStorage,
		private httpUtilsService: HttpUtilsService
		) {
		this.get();
	}

	public get() {
		this.httpUtilsService.get<UserModel>('auth/user/').subscribe(
			user => {
				if (!user.detail) {
					user.detail = new UserDetailModel();
				}
				this.set(user);
			});
	}

	public set(currentUser) : void {
		this.currentUser = currentUser;
		this.userUpdated$.next(this.currentUser);
	}

	public clear() : void {
		this.currentUser = new UserModel();
		this.userUpdated$.next(this.currentUser);
	}
}
