import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SpiderJobModel } from '../models/spider-job.model';
import { HttpUtilsService } from './http-utils.service';
import { QueryParamsModel } from '../models/query-params.model';
import { GetResultsModel } from '../models/get-results.model';


@Injectable()
export class SpiderLogService {

	API_LINK: string = 'spider_logs';

	constructor(private httpUtils: HttpUtilsService) {
	}

	get(queryParams: QueryParamsModel, vendor_name: string = null): Observable<GetResultsModel> {
		return this.httpUtils.get<GetResultsModel>(vendor_name + '/' + this.API_LINK + '/', queryParams);
	}

}
