import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { SpiderJobModel } from '../models/spider-job.model';
import { HttpUtilsService } from './http-utils.service';
import { ChartParamsModel } from '../models/chart-params.model';
import { ChartResultsModel } from '../models/chart-results.model';

@Injectable({
	providedIn: 'root'
})
export class ChartDataService {

	API_LINK: string = 'chart_data';
	data: ChartResultsModel = null;
	
	public chartDataUpdated$ : BehaviorSubject<ChartResultsModel> = new BehaviorSubject(this.data);

	constructor(private httpUtils: HttpUtilsService) {
	}

	get(queryParams: ChartParamsModel) {
		this.httpUtils.get<ChartResultsModel>(this.API_LINK + '/', queryParams).subscribe(
			res => this.chartDataUpdated$.next(res)
		);
	}
}
