import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders, HttpClient, HttpResponse} from '@angular/common/http';
import { TokenStorage } from '../auth/token-storage.service';
import { Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { AppConstants} from '../../config/api';
import { QueryParamsModel } from '../models/query-params.model';
import { ChartParamsModel } from '../models/chart-params.model';
import * as _moment from 'moment';


@Injectable()
export class HttpUtilsService {

	constructor(private tokenStorage: TokenStorage, private httpClient: HttpClient) {}

	getFindHTTPParams(queryParams: QueryParamsModel | ChartParamsModel): HttpParams {
		let params;
		if (queryParams instanceof QueryParamsModel) {
			params = new HttpParams()
				.set('sort', queryParams.sort)
				.set('page', (queryParams.page + 1).toString())
				.set('page_size', queryParams.page_size.toString())
				.set('search', JSON.stringify(queryParams.filter));
		}
		else if (queryParams instanceof ChartParamsModel) {
			params = new HttpParams()
				.set('vendor_id', queryParams.spider_id.toString())
				.set('period', queryParams.period)
				.set('from_date', queryParams.fromDate)
				.set('end_date', queryParams.endDate)
		}

		return params;
	}

	formateData(data: object) : object {
		for (let key in data) {
			let date = _moment(data[key], 'YYYY-MM-DDTHH:mm:ss.SSSSZ', true);
			if (key != 'created' && date.isValid()) {
				data[key] = date.format('YYYY-MM-DD');
			}
		}
		return data;
	}

	getHTTPHeader(secure:boolean = true): Observable<HttpHeaders> {
		if (secure) {
			return this.tokenStorage.getAccessToken().pipe(map(token => new HttpHeaders({'Authorization': 'Token ' + token})));
		}
		else {
			let headers = new HttpHeaders({});
			return of(headers);
		}
	}

	get<T>(url:string, queryParams: QueryParamsModel | ChartParamsModel = null, secure:boolean = true) : Observable<T> {
		return this.getHTTPHeader(secure).pipe(switchMap((headers: HttpHeaders) => {
			let options = {'headers': headers};
			if (queryParams) {
				options['params'] = this.getFindHTTPParams(queryParams);
			}
			return this.httpClient.get<T>(AppConstants.baseURL + url, options)
		}));
	}

	post<T>(url:string, data: object, secure:boolean = true) : Observable<T>{
		return this.getHTTPHeader(secure).pipe(switchMap((headers: HttpHeaders) => {
			headers = headers.set('Content-Type', 'application/json');
			return this.httpClient.post<T>(AppConstants.baseURL + url, JSON.stringify(this.formateData(data)),{'headers': headers});
		}));
		
	}

	put<T>(url:string, id: number, data: object, secure:boolean = true) : Observable<T>{
		return this.getHTTPHeader(secure).pipe(switchMap((headers: HttpHeaders) => {
			headers = headers.set('Content-Type', 'application/json');
			return this.httpClient.put<T>(AppConstants.baseURL + url + '/' + id.toString() + '/', JSON.stringify(this.formateData(data)), {'headers': headers});
		}));
	}

	delete<T>(url:string, id: number) : Observable<T>{
		return this.getHTTPHeader(true).pipe(switchMap((headers: HttpHeaders) => {
			return this.httpClient.delete<T>(AppConstants.baseURL + url + '/' + id.toString() + '/', {'headers': headers});
		}));
	}

}
