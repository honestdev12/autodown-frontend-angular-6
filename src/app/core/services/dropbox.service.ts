import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DropboxModel } from '../models/dropbox.model';
import { QueryParamsModel } from '../models/query-params.model';
import { GetResultsModel } from '../models/get-results.model';
import { HttpUtilsService } from './http-utils.service';

@Injectable()
export class DropboxService {	

	API_LINK: string = 'dropbox';

	constructor(private httpUtils: HttpUtilsService) {
	}

	get(queryParams: QueryParamsModel = null): Observable<GetResultsModel> {
		return this.httpUtils.get<GetResultsModel>(this.API_LINK + '/', queryParams, true);
	}

	update(id: number, data: object): Observable<DropboxModel> {
		return this.httpUtils.put<DropboxModel>(this.API_LINK, id, data)
	}

	create(data: object) : Observable<DropboxModel> {
		return this.httpUtils.post<DropboxModel>(this.API_LINK + '/', data)
	}

	delete(id: number): Observable<any> {
		return this.httpUtils.delete<any>(this.API_LINK + '/', id);
	}
}
