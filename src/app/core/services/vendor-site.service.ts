import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { VendorSiteModel } from '../models/vendor-site.model';
import { HttpUtilsService } from './http-utils.service';
import { QueryParamsModel } from '../models/query-params.model';
import { GetResultsModel } from '../models/get-results.model';

@Injectable({
	providedIn: 'root'
})
export class VendorSiteService {

	API_LINK: string = 'vendor_site';
	ADMIN_API_LINK: string = 'admin/vendor_site';
	myData: VendorSiteModel[] = [];
	myDataUpdated$: BehaviorSubject<VendorSiteModel[]> = new BehaviorSubject(this.myData);

	data: VendorSiteModel[] = [];
	dataUpdated$: BehaviorSubject<VendorSiteModel[]> = new BehaviorSubject(this.data);

	constructor(private httpUtils: HttpUtilsService) {
	}

	get(queryParams: QueryParamsModel=null, all:boolean=false, admin: boolean=false): void {
		let api_link = all ? this.API_LINK : 'my/vendor_site';
		api_link = admin ? 'admin/' +  api_link : api_link;
		this.httpUtils.get<any>(api_link + '/', queryParams).subscribe(
			res =>  {
				if (all)
					this.dataUpdated$.next(res.results);
				else this.myDataUpdated$.next(res.results);
			}
		);
	}

	update(id: number, data: VendorSiteModel): Observable<VendorSiteModel> {
		return this.httpUtils.put<VendorSiteModel>(this.ADMIN_API_LINK + '/', id, data)
	}

	create(data: VendorSiteModel) : Observable<VendorSiteModel> {
		return this.httpUtils.post<VendorSiteModel>(this.ADMIN_API_LINK + '/', data)
	}

	delete(id: number): Observable<VendorSiteModel> {
		return this.httpUtils.delete<any>(this.ADMIN_API_LINK + '/', id);
	}
}
