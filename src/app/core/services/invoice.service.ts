import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { InvoiceModel } from '../models/invoice.model';
import { HttpUtilsService } from './http-utils.service';
import { QueryParamsModel } from '../models/query-params.model';
import { GetResultsModel } from '../models/get-results.model';

let date_fields: string[] = [
	'start_date',
	'end_date',
	'bill_date'
]

@Injectable()
export class InvoiceService {

	API_LINK: string = 'invoice';
	
	constructor(private httpUtils: HttpUtilsService) {
	}

	getAPILink(admin: boolean = false, paid: string = null, userId: string = null): string {
		let api_link = this.API_LINK + '/';
		if (paid=='paid' || paid == 'unpaid')
			api_link = paid + '/' + api_link;
		if (userId) 
			api_link = userId + '/' + api_link;
		if (admin)
			api_link = 'admin/' + api_link;
		return api_link;
	}

	get(queryParams: QueryParamsModel, paid: string=null, admin: boolean=false, userId: string=null): Observable<GetResultsModel> {
		
		return this.httpUtils.get<GetResultsModel>(this.getAPILink(admin, paid, userId), queryParams);
	}

	update(id: number, data: InvoiceModel, admin: boolean = false): Observable<InvoiceModel> {
		return this.httpUtils.put<InvoiceModel>(this.getAPILink(admin), id, data);
	}

	create(data: InvoiceModel, admin: boolean = false) : Observable<InvoiceModel> {
		return this.httpUtils.post<InvoiceModel>(this.getAPILink(admin), data);
	}

	delete(id: number, admin: boolean = false): Observable<any> {
		return this.httpUtils.delete<any>(this.getAPILink(admin), id);
	}
}

