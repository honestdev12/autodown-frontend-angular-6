export class ScheduleModel {
	id?: number;
    hours: number;
    days: number;
    weeks: number;
    months: number;

    constructor () {
    	this.hours = 1;
    	this.days = 0;
    	this.weeks = 0;
    	this.months = 0;
    }
}
