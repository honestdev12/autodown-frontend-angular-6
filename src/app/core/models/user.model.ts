import {UserDetailModel} from './user-detail.model';


export class UserModel {
    id?: number;
    username: string;
    email: string;
    first_name: string;
    last_name: string;
    roles: string[];
    detail: UserDetailModel;
    unpaid_inv: number;

    constructor() {
		this.username = '';
		this.email = '';
		this.roles = [];
		this.first_name = '';
		this.last_name = '';
        this.unpaid_inv = 0;
		this.detail = new UserDetailModel();
	}
}
