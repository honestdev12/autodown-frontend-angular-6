export class QueryParamsModel {
	// fields
	sort: string;
	page: number;
	page_size: number;
	filter: object;

	// constructor overrides
	constructor(_sortOrder: string = 'asc',
		_sort: string = '',
		_page: number = 0,
		_pageSize: number = 10,
		_filter:object={}) {
		if (_sortOrder == 'desc')
			this.sort = '-' + _sort;
		else
			this.sort = _sort;
		this.page = _page;
		this.page_size = _pageSize;
		this.filter = _filter;
	}
}
