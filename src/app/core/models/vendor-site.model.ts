export class VendorSiteModel {
    id?: number;
    name: string;
    brand_img: string;
    type: string;
    spider_name: string;
}
