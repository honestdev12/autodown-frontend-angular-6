export class UserDetailModel {
    avatar : string;
    linkedin : string;
    facebook : string;
    twitter : string;
    instagram : string;
    address : string;
    city : string;
    phone_number : string;
    company_name : string;
    occupation : string;
    post_code: string;
    state: string;

    constructor() {
        this.avatar = '';
        this.linkedin = '';
        this.facebook = '';
        this.twitter = '';
        this.instagram = '';
        this.address = '';
        this.city = '';
        this.phone_number = '';
        this.company_name = '';
        this.occupation = '';
        this.post_code = '';
        this.state = '';
    }
}
