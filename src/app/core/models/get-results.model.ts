export class GetResultsModel {
	// fields
	results: any[];
	count: number;
	next: string;
	previous: string;
}
