import {ScheduleModel} from './schedule.model';

export class DropboxModel {
	id?: number;
	name: string;
    token: string;
    spider_job?: number;
    user?: number;

    constructor() {
    	this.name = '';
    	this.token = '';
    }
}
