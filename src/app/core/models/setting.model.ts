export class SettingModel {
    strip_api_key:  string;
    paypal_api_key: string;
    rate: number;
    dropbox_token: string;
}
