import { UserModel } from './user.model';

export class InvoiceModel {
	id?: number;
	total_count: number;
	price_amount: number;
    start_date: string;
    end_date: string;
    bill_date: string;
    paid: boolean;
    payment_type: string;
    payment_id: string;
    user: number;
    rate: number;
    full_name: string;
    email: string;

    constructor(
        _id:number = null, 
        _total_count: number = 0,
         _price_amount:number = 0,
         _start_date: string = null,
         _end_date: string = null,
         _bill_date: string = null,
         _paid: boolean = false,
         _payment_type: string = 'PayPal',
         _payment_id: string = null,
         _rate: number = 0, 
         _full_name: string = null,
         _email: string = null) {
        this.id = _id;
        this.total_count = _total_count;
        this.price_amount = _price_amount;
        this.start_date = _start_date
        this.end_date = _end_date
        this.bill_date = _bill_date;
        this.paid = _paid;
        this.payment_type = _payment_type;
        this.payment_id = _payment_id;
        this.rate = _rate;
        this.full_name = _full_name;
        this.email = _email
    }

}
