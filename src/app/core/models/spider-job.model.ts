import { ScheduleModel } from './schedule.model';
import { DropboxModel } from './dropbox.model';
import * as _moment from 'moment';

export class SpiderJobModel {
	id?: number;
	dropbox?: number;
    username: string;
    password: string;
    status: boolean;
    user?: number;
    vendor_site?: number;
    name?: string;
    from_date: string;

    constructor() {
        this.username = '';
        this.password = '';
        this.status = true;
        this.name = '';
        this.dropbox = 0;
        this.from_date = _moment().format('YYYY-MM-DD');
    }
}
