export class SpiderLogModel {
	id?: number;
    spider_job?:number;
    bill_date?:string;
    vendor_site_name: string;
    invoice_id: string;
    account_number: string;
    file_name: string;
    file_link: string;
    download_time: string;
}
