import * as _moment from 'moment';


export class ChartParamsModel {
	// fields
	spider_id: number | string;
	period: string;
	fromDate: string;
	endDate: string;

	// constructor overrides
	constructor(
		_spider_id: number | string = -1, 
		_period:string = 'week',
		_fromDate: any,
		_endDate: any
	) {
		this.spider_id = _spider_id;
		this.period = _period;
		this.fromDate = _fromDate ? _fromDate.format('YYYY-MM-DD') : '';
		this.endDate = _endDate ? _endDate.format('YYYY-MM-DD') : '';
	}
}
